<?php
    $query="SELECT v.DataVenda, v.Factura, v.valor, v.desconto, v.valorLiquido, v.pago, v.diferenca, p.nome as utilizador,c.idCaixa, cli.nome as cliente, mp.Modo_Pagamento from venda as v inner join pessoal as p on v.Pessoal_idPessoal =p.idPessoal inner join modo_pagamento as mp on v.Modo_Pagamento_idModo_Pagamento=mp.idModo_Pagamento inner join cliente as cli on cli.idCliente=v.Cliente_idCliente inner join caixa as c on c.idCaixa=v.Caixa_idCaixa";
    $vetor=$player->select($query,$liggar);
    
?>
<div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="dt_colVis_buttons"></div>
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Data</th>
                            <th>Utilizador</th> 
                            <th>Pagamento</th>
                            <th>Cliente</th>
                            <th>Caixa</th>   
                            <th>Factura</th>
                            <th>Valor</th>
                            <th>Desconto</th>
                            <th>Liquido</th>
                            <th>Valor Pago</th>
                            <th>Diferença</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=0;
                                while($linhas=$vetor->fetch_assoc()){
                                	$i++;
                            ?>
                        <tr>
                            <td><?php echo $i ?></td> 
                             <td><?php echo $dataVenda=$linhas['DataVenda'] ?></td>
                             <td><?php echo $linhas['utilizador'];?></td>
                             <td><?php  echo $linhas['Modo_Pagamento'];?></td>
                             <td><?php echo $cli=$linhas['cliente']?></td>
                             <td><?php echo $linhas['idCaixa'];?></td>
                             <td><?php echo $fact=$linhas['Factura']?></td>
                             <td><?php echo number_format($linhas['valor'],2,',','.')?></td>
                             <td><?php echo $linhas['desconto'].' %'?></td>
                             <td><?php echo number_format($linhas['valorLiquido'],2,',','.')?></td>
                             <td style="text-align: right;"><?php echo number_format($linhas['pago'],2,',','.')?></td>
                             <td style="text-align: right;">
                                <?php echo number_format($linhas['diferenca'],2,',','.')?>
                            </td>
                            <td>
                                 <a title="Detalhes da venda" data-uk-modal="{target:'#modal_full'}">
                                     <i class="material-icons">info</i>
                                </a>
                                <?php 
                                    require_once('modalVendas.php');
                                ?> 
                         </td>

                        </tr>
                        <?php
                        } 
                        ?>
                      </tbody>
                    </table>
                </div>
            </div>
