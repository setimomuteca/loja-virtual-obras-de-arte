<?php
	require('invoice.php');
	session_start();
	
   $data=$dataF = '';
   $hora =$horaF= '';
   $vector = array();
   $utilizador='';
   $dataMov= array();
   $movimento=array();
   $valorMov=array();
   $obs=array();
   $totalEntradas=$totalSaidas=$saldoFinal=$saldoInformado=$quebraCaixa=0;

	//busca empresa
		require('../iuda_Shop.php');
		$ligar_BD = new conexao();
    	$liggar=$ligar_BD->conectar();
    	$player= new Operacao();
		$buscaEmpresa="Select*from empresa";
		$ObjQuery=$player->select($buscaEmpresa,$liggar);  

		while ($linhas=$ObjQuery->fetch_assoc()) {
			$empresa='';//$linhas['nomeComercial'];
			$end=$linhas['endereco'];
			$telefone=$linhas['telefone'];
			$NIF=$linhas['NContribuinte'];

		}
		$idCaixa=$_GET['cx'];
   		 $query="SELECT*from caixa INNER JOIN transacaocaixa on caixa.idCaixa= transacaocaixa.Caixa_idCaixa INNER Join pessoal on pessoal.idPessoal=caixa.Pessoal_idPessoal where Caixa_idCaixa=$idCaixa";
   		
		 $queryObj=$player->select($query,$liggar);
		 $i=0;
		while($registos=$queryObj->fetch_assoc()){
			$abertura=$registos['Abertura'];
			$response = explode(" ", $abertura);
			$data = $response[0];
			$hora = $response[1];
			$Fecho=$registos['Fecho'];
			$dataMov[$i]= $registos['dataTransacao'];
		    $movimento[$i]=$registos['tipo'];
		    $valorMov[$i]=$registos['valor'];
		    $obs[$i]=$registos['obs'];
			if ($Fecho!='') {
				$responseFecho=explode(" ", $Fecho);
				$dataF=$responseFecho[0];
				$horaF=$responseFecho[1];
			}
			$inicial=$registos['Valor'];
			$entradas=$registos['Adicionado'];
			//die($entradas);
			$saidas=$registos['Retirado'];
			$saldoInfor=$registos['saldoFinalInformado'];
			$saldoFinal=$registos['SaldoFinal'];
			$utilizador=$registos['nome'];

		$i++;	
		}

	

	//busca empresa
	$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
	$pdf->AddPage();
	$pdf->AliasNbPages();
	$pdf->SetAutoPageBreak(true);
	$pdf->addSociete($empresa,
                  $end."\n" .
                  $telefone."\n" .
                  "NIF : ".$NIF."");
		$pdf->SetFont('Arial','B',13);
		$pdf->setXY(20,53);
		$pdf->SetLineWidth(0.3);
		$pdf->Line( 10, 50, 200, 50);
		$pdf->SetLineWidth(0);
		$titulo="Extrato de Caixa ";
		$width=$pdf->GetStringWidth($titulo);
		$pdf->Cell($width,10,utf8_decode($titulo),0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Setxy(145,40);
		$pdf->SetFont('Arial','',9);
		$y=60;
		$pdf->setY($y);
		$altura=5;
			$pdf->SetDrawColor(226,223,223);
			$pdf->SetFont('Arial','',8);
			$pdf->SetFillColor(174,195,238);
			$pdf->Cell(90,5,'',0,1,'C');
			$pdf->Cell(15,5,utf8_decode('Utilizador'),1,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(75,5,utf8_decode($utilizador),1,1,'C');
			$pdf->Cell(90,5,'',0,1,'C');
			$pdf->Cell(45,5,utf8_decode('Abertura'),1,0,'C');
			$pdf->Cell(45,5,utf8_decode('Fecho'),1,1,'C');
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(22.5,5,$data,1,0,'C');
			$pdf->Cell(22.5,5,$hora,1,0,'C');
			$pdf->Cell(22.5,5,$dataF,1,0,'C');
			$pdf->Cell(22.5,5,$horaF,1,1,'C');
			$pdf->Cell(90,5,'',0,1,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(29,5,utf8_decode('Data'),1,0,'C',true);
			$pdf->Cell(19,5,utf8_decode('Hora'),1,0,'C',true);
			$pdf->Cell(30,5,utf8_decode('Movimento'),1,0,'C',true);
			$pdf->Cell(29,5,utf8_decode('Valor'),1,0,'C',true);
			$pdf->Cell(84,5,utf8_decode('observações'),1,1,'C',true);
			$totalGeral=0;
			$i=0;
			$inicial=0;

			$pdf->SetFont('Arial','',8);
			for ($i=0; $i <count($dataMov) ; $i++) { 

				$resposta=explode(" ", $dataMov[$i]);
				$dataMovimento=$resposta[0];
				$horaMovimento=$resposta[1];
				
				$pdf->Cell(29,5,$dataMovimento,1,0,'C');
				$pdf->Cell(19,5,$horaMovimento,1,0,'C');
				$pdf->Cell(30,5,utf8_decode($movimento[$i]),1,0,'C');
				$pdf->Cell(29,5,number_format($valorMov[$i],2,',',' '),1,0,'R');
				$pdf->Cell(84,5,utf8_decode($obs[$i]),1,1,'C');
				if ($movimento[$i]=='A') {
					$inicial=$valorMov[$i];
				}
				if ($pdf->PageNo()==1 and $i==31) {
					$pdf->AddPage();
				}

			
		 }
		  
		  $pdf->Cell(191,5,'',0,1,'C');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(84,5,utf8_decode('Balanço de caixa'),1,1,'C',true);
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Valor Inicial'),1,0,'L');
		  $pdf->Cell(42,5,number_format($inicial,2,',',' '),1,1,'R');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Total de  entradas'),1,0,'L');
		  $pdf->Cell(42,5,number_format($entradas,2,',',' '),1,1,'R');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Total de  saidas'),1,0,'L');
		  $pdf->Cell(42,5,number_format($saidas*(-1),2,',',' '),1,1,'R');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Saldo'),1,0,'L');
		  $pdf->Cell(42,5,number_format($saldoFinal,2,',',' '),1,1,'R');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Valor Informado'),1,0,'L');
		  $pdf->Cell(42,5,number_format($saldoInfor,2,',',' '),1,1,'R');
		  $pdf->Cell(107,5,'',0,0,'C');
		  $pdf->Cell(42,5,utf8_decode('Quebra de caixa'),1,0,'L');
		  $pdf->Cell(42,5,number_format($saldoInfor-$saldoFinal,2,',',' '),1,1,'R');


	
	
		

		  


	$pdf->Output();

?>