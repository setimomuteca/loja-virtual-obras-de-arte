<?php
    require("fornVendas.php");
        $doc=$_GET['XX'];
        $query="SELECT*FROM linhasacertos inner join produto on linhasacertos.idProduto=produto.idProduto where NumDoc='$doc'"; 
        $usuarios=$player->select($query,$liggar);
       //die($query);
?>
<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>SEVEN-Sistema de Gestão</title>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
	<link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="JS/personalizado.js"></script>
    <script type="text/javascript" src="JS/modalVenda.js"></script>
    <!--adicionar e remover linhas-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>  
        <script src="js/bootstrap.min.js"></script>
        <!--adicionar e remover linhas-->
    <style type="text/css">
        .resultado li{
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#diferenca").maskMoney({thousands:'.', decimal:',', allowZero: true, suffix: ' akz',});

        });
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require("menu.php");
   ?>
    <!-- main sidebar end -->
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                        	<!--Cabecalho das tabs-->
	                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
	                                <li class="uk-active"><a href="#">Acerto</a></li>
	                            </ul>
                            <!--Cabecalho das tabs-->
                            <ul id="tabs_1_content" class="uk-switcher uk-margin">
                                <!--PRIMEIRA TAB-->
                                <li>
                                <form action="prodAcertos.php" method="POST" name="formVendas">	
									<table style="vertical-align: bottom; margin-top:10px;">
                                        <tr style="margin-top: 150px;">
                                            <td>
                                              <div class="uk-width-medium-1-1">
                                                <input type="text" id="pesquisa" name="pesquisa" class="md-input uk-form-width-large" autocomplete="off" placeholder="Código de barras ou nome do produto" required />
                                                <input type="hidden" name="idProduto" id="idProduto">
                                                <ul class="resultado" style="list-style: none; background: #C1C1C2; color:white; padding:3px; z-index:0; position: fixed;"> 
                                                    <li class="lista"></li>
                                                </ul> 
                                            </td>
                                            <td>
                                                <div class="md-input-wrapper md-input-filled" style="left:80px;">
                                                <label>Quantidades</label>
                                                <input type="number" min="1" name="qtd"  class="md-input uk-form-width-mini label-fixed" style="width: 100px;" onKeyUp='somente_numero(this)'/>
                                                </div> 
                                            </td>
                                        </tr>
                                    </table>
                                <button class="btn btn-large btn-success" style="float: right; left: -38px; position: relative; margin-bottom: 20px;" type="submit">Adicionar
                                    </button>
                                    <div style="width: 100%; ">
                                        <?php
                                     

                                        ?>
                                        <input type="hidden" name="fact" value="<?php echo $doc;?>">
                                      <table id="products-table" style="margin-top:40px; border-top:double #0B2462 2px; width: 880px;" class="table table-hover table-bordered">
                                     
	                                    <thead>
	                                        <tr>
	                                          <th>Produto</th>
                                              <th style="width: 75px">Qtd</th>
	                                          <th class="actions" style="width: 40px;">Ações</th>
	                                        </tr>
	                                    </thead>
                                        
                                        <tbody>
                                            <?php
                                            $valorF=0;
                                            while( $registos=$usuarios->fetch_assoc()):
                                                ?>
                                                <tr>
                                                    <td>

                                                        <?php echo $nome=$registos['Desigacao']; ?> 
                                                    </td>
                                                    <td>
                                                        <?php echo $registos['qtd']; ?> 
                                                    </td>
                                                    
                                                    
                                                    <td width="90" align="center"><a href="delete_Prod_Acerto.php?id=<?php echo $registos['idProduto']; ?>&invoice=<?php echo $registos['NumDoc']; ?>&transact=<?php echo $registos['id']?>"><i class="material-icons uk-text-danger">delete</i> </a></td>

                                                </tr>
                                            <?php
                                                endwhile;
                                            ?>
                                        </tbody>
                               
                                        </table>
                                    </div>
                                 </form>
                                    <a class="btn btn-large btn-primary" href="finalizaAcerto.php?fact=<?php echo $doc ?>">Finalizar
                                    </a>
                             
                            		<!-- Final-->

                                </div>
                                </li>
                                <!--Fim da primeira tab-->
                                <!--Segunda Tab-->
									
                               </div>
							</div>
                     	</div>
                    </div>  
             </li>              
     	</div>
	</li>
</td>
    <script type="text/javascript" src="JS/jquery.maskMoney.js"></script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
</body>
</html>