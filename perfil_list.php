<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="SELECT* FROM Perfis";
    $forneceddores=$player->select($busca,$liggar);
    $i=1;
?>
<!doctype html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Lista de Perfis</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once('header.php');
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
    <?php
        require_once('menu.php');
    ?>
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Lista de Perfis</h3>
            <div class="uk-width-medium-1-6">
                            <a href="perfil_regist.php" class="md-btn md-btn-success md-btn-wave-light" href="javascript:void(0)">Novo</a>
                </div>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="dt_colVis_buttons"></div>
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Perfil</th>
                            <th>Descrição</th>
                            <th>Ativo</th>
                            <th colspan="2"></th>                          
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nome=array();
                            $i=1;
                            while( $liga=$forneceddores->
                                        fetch_assoc()):
                            ?>
                        <tr>
                            <td><?php echo $i?></td> 
                            <td><?php echo $Perfil=$liga['Perfil']?></td>
                            <td><?php echo $Descricao=$liga['Descricao']?></td>
                            <td style="width: 100px;">
                            	<input type="checkbox" <?php if ($liga['Ativo']) echo "checked"?>/>
                            </td>
                            <td style="width: 20px;">
                                <a href="user_Perfil.html" data-uk-modal="{target:'#modal_default<?php echo $i?>'}"">
                                    <i class="material-icons">info</i>
                                    <!--Inicio do modal-->
                                <div class="uk-width-medium-1-3">
                                    <div class="uk-modal" id="modal_default<?php echo $i?>">
                                    <div class="uk-modal-dialog">		
						            <div class="md-card" style="height: 421px;">
                                    <div class="user_heading" style="height: 117px;">
                                    <div class="user_heading_menu hidden-print">
                                        <div class="uk-display-inline-block"><i class="md-icon md-icon-light material-icons" id="page_print">&#xE8ad;</i></div>
                                    </div>
                                    <div class="user_heading_avatar">
                                        <div class="thumbnail">
                                            <img src="assets/img/avatars/avatar_11.png" alt="user avatar"/>
                                        </div>
                                    </div>
                                    <div class="user_heading_content">
                                        <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?php echo $Perfil?></span><span class="sub-heading"><?php echo $Descricao?></span></h2>
                                    </div>
                                    <!--final do modal-->
                                <div class="user_content">
                                	<div class="uk-width-large-1-2" style="width: 100%;">
                                        <h4 class="heading_c uk-margin-small-bottom">Informações de contacto</h4>
                                        <table class="table" style="color: black; width: 100%;">
                                            <tr><td>Perfil</td><td><?php echo $liga['Perfil']?></td></tr>
                                            <tr>
                                                <td>Descrição</td>
                                                <td><?php echo $liga['Descricao']?></td>
                                            </tr>
                                            <tr>
                                                <td>Ativo</td>
                                                <td>
                                                <input type="checkbox" name="" <?php If ($liga['Ativo']) echo "Checked"?> disabled/> 
                                                                                              
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>                                
                            </td>                         
                        </tr>
                        <?php
                        $i++;
                        endwhile
                        ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
    
    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>
    
    <script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>