<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="SELECT*from unidades order by unidade asc ";
    $categ="SELECT*from familia order by familia asc ";
    $forneceddores=$player->select($busca,$liggar);  
    $fornecedores=$player->select($categ,$liggar); 

    if (isset($_GET['XX'])) {
        $id=$_GET['XX'];
        require_once("iuda_Shop.php");
        $ligar_BD = new conexao();
        $liggar=$ligar_BD->conectar();
        $player= new Operacao();
        $busca="select*from produto where idProduto='$id'";
        $registos=$player->select($busca,$liggar);
        while( $liga=$registos->fetch_assoc()){
            $Desigacao =$liga['Desigacao'];
           $marca=$liga['marca'];
            $Pcompra =$liga['Pcompra'];
            $Pvenda =$liga['Pvenda'];
            $stkmax =$liga['stkmax'];
            $stkmin=$liga['stkmin'];
        };
    };

?>   

<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Registo de Produtos</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript" src="JS/validar.js"></script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require("menus/menuAdmin.php");
   ?>
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
            <form action="reg_produtos.php" method="POST" class="uk-form-stacked" id="product_edit_form">
                <input type="hidden" name="editaProd">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Registo de Produtos
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                        <div class="uk-input-group">
                                        <label for="product_edit_sku_control">Código de Barras</label>
                                        <input type="text" onkeyup="somente_numero(this)" class="md-input" required name="codBarras" id="product_edit_sku_control"/>
                                    </div>
                                </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_name_control">Descrição</label>
                                            <input type="text" class="md-input" id="descricao" name="descricao" required value="<?php echo $Desigacao ?>" />
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Marca/Fabricante</label>
                                            <input type="text" class="md-input" id="marca" name="marca" required value="<?php echo $marca?>"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_memory_control" class="uk-form-label">Unidade</label>
                                            <select id="marca" class="md-input" name="unidade" data-md-selectize required >
                                                <option>Nao definido</option>
                                                <?php 
                                                    while( $liga=$forneceddores->fetch_assoc()){
                                                        echo "<option value=".$liga['idunidades'].">".$liga['unidade']."</option>";
                                                        
                                                     }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_editt_memory_control" class="uk-form-label">Categoria</label>
                                            <select id="product_editt_memory_control" class="md-input" name="categoria" data-md-selectize required>
                                                <option></option>
                                                <?php 
                                                    while( $liga=$fornecedores->fetch_assoc()){
                                                        echo "<option value=".$liga['idfamilia'].">".$liga['familia']."</option>";
                                                       }
                                                ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                            <label for="product_edit_description_control">Nota</label><br><br>
                                            <textarea class="md-input" name="product_edit_description_control" id="product_edit_description_control" cols="30" rows="4" placeholder="Area de Descrição do Produto"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                   Valores e Quantidades
                                </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-usd"></i>
                                        </span>
                                        <label for="product_edit_price_control">Preço de Custo</label>
                                        <input type="text" class="md-input" onkeyup="somente_numero(this)" name="pCusto" id="product_edit_price_control"  value="<?php echo $Pcompra?>" />
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">%</span>
                                        <label for="product_edit_tax_control">Preço de venda</label>
                                        <input type="text" onkeyup="somente_numero(this)" class="md-input" name="pVenda" id="product_edit_tax_control" value="<?php $Pvenda?>"/>
                                    </div>
                                </div>

                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-cubes"></i>
                                        </span>
                                        <label for="product_edit_quantity_control">Quantidade em stock</label>
                                        <input type="text" onkeyup="somente_numero(this)" class="md-input" name="quantidade" id="product_edit_quantity_control"  />
                                    </div>
                                </div>

                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-cubes"></i>
                                        </span>
                                        <label for="product_edit_quantity_control">Quantidade em stock</label>
                                        <input type="text" onkeyup="somente_numero(this)" class="md-input" name="quantidade" id="product_edit_quantity_control"  />
                                    </div>
                                </div>

                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-cubes"></i>
                                        </span>
                                        <label for="product_edit_quantity_control">Quantidade em stock</label>
                                        <input type="text" onkeyup="somente_numero(this)" class="md-input" name="quantidade" id="product_edit_quantity_control"  />
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="uk-icon-cubes"></i>
                                        </span>
                                        <label for="product_edit_quantity_control">Taxa de ipc</label>

                                        <select class="md-input" name="taxaIpc">
                                               <?php 
                                                    while( $liga=$buscaIpc->fetch_assoc()){
                                                        echo "<option value=".$liga['idIpc'].">".$liga['descricao']."</option>";
                                                        
                                                     }
                                                ?>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
</body>


</html>