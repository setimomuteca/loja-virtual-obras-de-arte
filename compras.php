<?php
    require("fornVendas.php");   
?>
<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>SEVEN-Sistema de Gestão</title>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
	<link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="JS/personalizado.js"></script>
    <script type="text/javascript" src="JS/modalVenda.js"></script>
    <!--adicionar e remover linhas-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>  
        <script src="js/bootstrap.min.js"></script>
        <!--adicionar e remover linhas-->
    <style type="text/css">
        .resultado li{
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#diferenca").maskMoney({thousands:'.', decimal:',', allowZero: true, suffix: ' akz',});

        });
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        if($_SESSION['previlegio']=='Administrador')
        {
            require_once('menus\menuAdmin.php');
        }
        if($_SESSION['previlegio']=='Gvnd')
        {
            require_once('menus\menuGerenteVendas.php');
        }

        if($_SESSION['previlegio']=='Gstck')
        {
            require_once('menus\menuGerenteStock.php');
        }

   ?>
    <!-- main sidebar end -->
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                        	<!--Cabecalho das tabs-->
	                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
	                                <li class="uk-active"><a href="#">Nova</a></li>
	                                <li class="named_tab"><a href="#">Pesquisar</a></li>
	                            </ul>
                            <!--Cabecalho das tabs-->
                            <ul id="tabs_1_content" class="uk-switcher uk-margin">
                                <!--PRIMEIRA TAB-->
                                <li>
                                <form action="prodCompras.php" method="POST" name="formVendas">	
									<table style="vertical-align: bottom;">
                                        <tr style="margin-top: 50px;">
                                            <td>
                                              <div class="uk-width-medium-1-1">
                                                <input type="text" id="pesquisa" name="pesquisa" class="md-input uk-form-width-large" autocomplete="off" placeholder="Código de barras ou nome do produto" required />
                                                <input type="hidden" name="idProduto" id="idProduto">
                                                <ul class="resultado" style=" list-style: none; background: #C1C1C2; color:white; padding:3px; z-index:0; position: fixed;"> 
                                                    <li class="lista"></li>
                                                </ul> 
                                            </td>
                                            <td>
                                               <div class="md-input-wrapper md-input-filled" style="left: 50px; margin-top:10px;">
                                                    <label>Preço</label>
                                                    <input type="text" 
                                        onkeyup='somente_numero(this)' name="precUni" min="1" value="1" id="Punit" class="md-input uk-form-width-mini label-fixed" style="width: 80px;"/>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="md-input-wrapper md-input-filled" style="left:80px;">
                                                <label>Quantidades</label>
                                                <input type="text" id="qtd" min="1" value="<?php echo number_format(1)?>" name="qtd"  class="md-input uk-form-width-mini label-fixed" style="width: 100px;" onKeyUp='somente_numero(this)'/>
                                                </div> 
                                            </td>
                                            <td>
                                               <div class="md-input-wrapper md-input-filled" style="left:100px;">
                                                <label>Valor</label>
                                                <input type="text" readonly name="valor" id="totVal" style="width: 150px;" class="md-input uk-form-width-mini label-fixed"/>
                                                </div> 
                                            </td>
                                        </tr>
                                    </table>
                                <button class="btn btn-large btn-success" style="float: right; left: -38px; position: relative; margin-bottom: 20px;" type="submit">Adicionar
                                    </button>
                                    <div style="width: 100%; ">
                                        <?php
                                        $doc=$_GET['XX'];
                                            $busca="SELECT*FROM vendas_pend WHERE nFactura='$doc' and tipo='entrada'";
                                            $usuarios=$player->select($busca,$liggar);

                                        ?>
                                        <input type="hidden" name="fact" value="<?php echo $doc;?>">
                                      <table id="products-table" style="margin-top:40px; border-top:double #0B2462 2px; width: 880px;" class="table table-hover table-bordered">
                                     
	                                    <thead>
	                                        <tr>
	                                          <th>Produto</th>
                                              <th style="width: 75px">Qtd</th>
	                                          <th style="width: 75px; text-align: center;">Preço </th>
	                                          <th style="width: 75px">Valor</th>
	                                          <th class="actions" style="width: 40px;">Ações</th>
	                                        </tr>
	                                    </thead>
                                        
                                        <tbody>
                                            <?php
                                            $valorF=0;
                                            while( $registos=$usuarios->fetch_assoc()):
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $nome=$registos['DesProduto']; ?> 
                                                    </td>
                                                    <td>
                                                        <?php echo $registos['Qty']; ?> 
                                                    </td>
                                                    <td>
                                                        <?php echo number_format($registos['precUni'],2,',','.'); ?> 
                                                    </td>
                                                    
                                                    <td>
                                                        <?php echo number_format($registos['valor'],2,',','.'); 
                                                        $valorF+=$registos['valor'];
                                                        ?> 
                                                    </td>
                                                    <td width="90" align="center"><a href="delete_Prod_Compra.php?id=<?php echo $registos['idProduto']; ?>&invoice=<?php echo $registos['nFactura']; ?>&transact=<?php echo $registos['idVendas_Pend']?>"><i class="material-icons uk-text-danger">delete</i> </a></td>

                                                </tr>
                                            <?php
                                                endwhile;
                                            ?>
                                        </tbody>
                               
                                        </table>
                                    </div>
                                 </form>

                                    <span style="float: right;  left:250px; font-weight: bolder; position: relative; margin-top: 78px; border:none; font-size: 40px;">
                                            Total=
                                             <input type="text"  readonly style="border:none;"  value="<?php echo number_format($valorF,2,',','.'); ; ?>" >  
                                             <input type="hidden" name="totVenda" value="<?php echo $valorF ?>" id="totVenda">     
                                    </span>
                                    <button class="btn btn-large btn-primary" data-uk-modal="{target:'#modal_header_footer'}" id="finalizar">Finalizar
                                    </button>
                                <div class="uk-width-medium-1-3">
                                    <form action="finaliza_compra.php" method="POST">
                                        <input type="hidden" name="fact" value="<?php echo $doc;?>">
										<div class="uk-modal" id="modal_header_footer">
                                		    <div class="uk-modal-dialog">
    	                                    	<div class="uk-modal-header">
    	                                        	<h3 class="uk-modal-title" style="font-size: 22px;">
                                                     FINALIZAR COMPRA
                                                    </h3>
    	                                    	</div>
                                                <input type="hidden" name="modoPagamento" value="1"/>
                                    		  <hr class="uk-grid-divider">
                                             <div class="row" style="border:solid 2px;">
                                                    <div class="col-sm-4">
                                                       <h4>Total da conta <input type="text" name="ttvenda" id="ttvenda" readonly style="border:none;"> 
                                                        </h4> 
                                                    </div>
                                                      <div class="col-sm-4">
                                                       <label>Desconto(%)</label>
                                                    <input type="text" style="width:50px;" maxlength="3" placeholder="%" name="desconto"  id="percentagem" onkeyup="somente_numero(this);" value="0"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Total Liquido</label>
                                                        <input type="text" style="width:80px; border:none;" readonly   readonly id="vfinal" name="vFinal" />
                                                    </div>
                                            </div>

                                     		<hr class="uk-grid-divider">
                                    	  <h2 class="heading_b">Pagamento</h2>
                             				<div class="uk-grid uk-grid-divider" data-uk-grid-margin>
                                    	<div class="uk-grid">
                                       
                                        <div class="uk-width-1-1" id="dinheiro">
                                            <div class="js-pm_info pm_credit_card uk-margin-top">
                                                <div class="uk-grid"  style="width: 550px; margin-left: 10px;">
                                                    <div class="uk-width-medium-2-4">
                                                    <input type="text" name="recebido" onkeyup="somente_numero(this);" id="recebido" required autocomplete="OFF"  class="md-input" placeholder="valor pago..." />
                                                    </div>
                                                    <div class="uk-width-medium-1-4" id="grp" >
                                                        <label for="diferenca" id="lbl" style="font-weight: bolder;"></label>
                                                        <input type="text" class="md-input label-fixed" placeholder="diferença" readonly id="diferenca" name="diferenca" />

                                                    </div>
                                                    
                                                    <input type="hidden" id="numProdutos" name="numProdutos">
                                                </div>
                                            </div>
                                            <div class="uk-form-row" style="width: 550px; margin-left: 10px;">
                                                <span class="uk-form-help-block">Fornecedor</span>
                                                <select id="select_demo_2" class="md-input" data-uk-tooltip="{pos:'top'}" title="Cliente" name="fornecedores">
                                                    <option value='1'>Nao Identificado</option>
                                                    <?php
                                                        while($liga=$forneceddores->fetch_assoc()){
                                                    ?>
                                                    <option value="<?php echo $liga['idfornecedores'] ?>"> 
                                                        <?php echo $liga['companhia'];?>
                                                    </option>
                                                    
                                                    <?php
                                                       }
                                                    ?>
                                                </select>
                            
                                       </div>
                                     </div>
                            		<!--Rodape da modal-->
                            		<div class="uk-modal-footer" style="width: 595px; left: 30px; position: relative;">
                                        <div class="row">
                                            <div class="col-md-5">
                                               <span class="icheck-inline">
                                                    <input type="checkbox" name="printFact" id="checkbox_demo_inline_3" data-md-icheck checked/>
                                                    <label for="checkbox_demo_inline_3" class="inline-label">Imprimir vª fatura</label>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="submit" name="guardar" class="md-btn btn-success" value="Terminar">
                                            </div>
                                        </div>
                                     </div>

                            		</div>
                                 </form>
                                </div>
                            		<!-- Final-->

                                </div>
                                </li>
                                <!--Fim da primeira tab-->
                                <!--Segunda Tab-->
									<li>
										<?php 
											require("lista_vendas.php");
										?>
									</li>
                               </div>
							</div>
                     	</div>
                    </div>  
             </li>              
     	</div>
	</li>
</td>
    <script type="text/javascript" src="JS/jquery.maskMoney.js"></script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
</body>
</html>