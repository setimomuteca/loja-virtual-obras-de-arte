<?php
   if (isset($_GET['uknw'])) {
       $id=$_GET['uknw'];
   } else if (isset($_GET['dstv'])) {
      $aviso="<span style='color:red; font-weight: bold'>Password antiga errada!</span>";
      $id=$_GET['dstv'];

   }else if (isset($_GET['sprd'])) {
      $aviso="<span style='color:red; font-weight: bold'>As Password são diferentes!</span>";
      $id=$_GET['sprd'];
   } 
   else{ 
    header('location:index.php');
     die();
   }

?>
<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Mudança da Palavra-Passe</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript" src="JS/validar.js"></script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <div id="page_content">
        <div id="page_content_inner">
            <form action="change.php" method="POST" class="uk-form-stacked" id="product_edit_form">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="uk-alert uk-alert-warning" data-uk-alert>
                            <a href="#" class="uk-alert-close uk-close"></a>
                                <?php
                                    if (isset($aviso)) {
                                       echo $aviso;
                                    } else{
                                      echo  'Recomendamos que altere a sua password para a sua seguranca no sistema!!';  
                                    }

                                ?>

                            
                        </div>  
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Atualização da Password! 
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                        <div class="uk-form-row">
                                            <label for="product_edit_name_control">Introduza a password antiga</label>
                                            <input type="password" class="md-input" id="descricao" required name="pass"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Nova password</label>
                                            <input type="password" class="md-input" id="marca" required name="new_pass"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Repita a password</label>
                                            <input type="password" class="md-input" id="marca" required name="pass2" value=""/>
                                            <input type="hidden" name="chave" value="<?php echo $id ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-fab-wrapper">
                    <input type="submit" name="guardar" id="btn_Guardar" value="Guardar">
                    <i class="material-icons"></i>
                 </div>

            </form>
        </div>
    </div>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
</body>
</html>