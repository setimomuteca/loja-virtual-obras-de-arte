
<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Sistema de vendas</title>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
	<link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="JS/personalizado.js"></script>
    <script type="text/javascript" src="JS/modalVenda.js"></script>
    <script type="text/javascript" src="JS/pagamento.js"></script>
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript" src=JS/priceFormat.js></script>
    <!--adicionar e remover linhas-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>  
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#SaldoInformado").keyup(function() {
                    var total=$("#ttvenda").val();
                    var recebido=$("#SaldoInformado").val();
                    var diferenca=total-recebido;
                    var absolut=Math.abs(diferenca);
                    $("#diferenca").val(absolut);
                    if (diferenca>=0) {
                        $("#lbl").text("Falta");
                        $("#grp").css("color","#FF0000");
                     } else{
                      $("#lbl").text("Resta"); 
                      $("#grp").css("color","#3A9708");

                     }

                });
               
            });
        </script>   
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        if($_SESSION['previlegio']=='Administrador')
        {
            require_once('menus\menuAdmin.php');
        }
        if($_SESSION['previlegio']=='Gvnd')
        {
            require_once('menus\menuGerenteVendas.php');
        }

        if($_SESSION['previlegio']=='Gstck')
        {
            require_once('menus\menuGerenteStock.php');
        }

   ?>
    <!-- main sidebar end -->
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                        	<!--Cabecalho das tabs-->
	                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
	                                <li class="named_tab"><a href="#"> ENTRADA DE ESTOQUE </a></li>
	                                <li class="uk-active"><a href="#">TRANSAÇÃO DE ESTOQUE</a></li>
                                    <li class="named_tab"><a href="#">SAIDA DE ESTOQUE</a></li>
	                            </ul>
                            <!--Cabecalho das tabs-->
                            <ul id="tabs_1_content" class="uk-switcher uk-margin">
                                <!--PRIMEIRA TAB-->
                                <li>
                                    
                                        <div class="md-card uk-margin-medium-bottom">
	                                          <table class="table">
	                                        	
	                                        		<td>
	                                        			<button class="md-btn md-btn-primary md-btn-block md-btn-wave-light" data-uk-modal="{target:'#modal_default4'}">
	                                        				Devolução
	                                        				
	                                        			</button>
	                                        			 <div class="uk-modal" id="modal_default4">
                                                           <div class="uk-modal-dialog">
                                                            <button type="button" class="uk-modal-close uk-close"></button>
                                                            <form action="AddMoney.php" method="POST">
                                                            <h2 class="heading_a">Devolução dos produtos<br><br>
                                                                    <div class="class="uk-width-medium" style="margin-top: 23px; margin-bottom: 23px">
                                                                        <input type="text" class="md-input label-fixed" name="factura" id="valorAdicionar" placeholder="Nº da factura" autocomplete="off" required />
                                                               

                                                                    </div>
                                                                   <input type="submit" class="md-btn md-btn-primary" name="retirar" value="Buscar"> 
                                                                </form>
                                                            </div>
                                                             
                                                        </div>
	                                        		</td>
	                                        		<td>
                                                        <?php $codFact=rand(100,1000).'-'.date('s');?>
                                                        <a class="md-btn md-btn-primary md-btn-block md-btn-wave-light" href="compras.php?XX=<?php echo $codFact?>">Compra
	                                        		</a>
                                                   

                                                </td>
                                                    <?php $codFact=rand(100,1000).'-'.date('s');?>
	                                        		
	                                        		
	                                        		<td><a href="acertos.php?XX=<?php echo $codFact?>" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Acertos
                                                    </a>
                                                                                                                                                
                                                           

                                                    <!--Final do modal de Adicionar Dinheiro-->
                                                </td>
                                                <td>
                                                    <button class="md-btn md-btn-primary md-btn-block md-btn-wave-light" data-uk-modal="{target:'#modal_default4'}">Outras
                                                    </button>
                                                </td>
	                                        </tr>
	                                		</table>
                                        </div>
								</li>
                                <li>
                                <!--Caixas Anteriores-->
                                 <div class="md-card uk-margin-medium-bottom">
                                   <div class="md-card-content">
											 <div class="dt_colVis_buttons"></div>
                                                <table id="dt_colVis" class="uk-table uk-table-striped" cellspacing="0" width="100%">
													<thead>
								                        <tr>
								                            <th>#</th>
                                                            <th>Produto</th>
                                                            <th>Qty</th>
                                                            <th>P.Unit</th>
                                                            <th>Valor</th>
                                                            <th>Data</th>
                                                            <th>Tipo</th>
								                            <th>Doc</th>
                                                            <th>Nº Doc</th>
                                                            <th>Entidade</th>
								                            <th>Utilizador</th>
								                 		</tr>
                       								</thead>
                                                    <tbody>  
                       								<?php 
                                                       require_once("transacao_estoque.php");
                                                       //$i=0;
                                                       while( $liga=$forneceddores->fetch_assoc()){
                                                        $i++;
                                                    ?>
                                                    <tr>
                                                         <td><?php echo $i?></td>
                                                         <td> <?php echo $liga['Desigacao']?></td>
                                                         <td> <?php echo $liga['quant_trans']?></td>
                                                         <td><?php  echo number_format($liga['Prec_Uni'],2,',','.')?></td>
                                                         <td><?php  echo number_format($liga['Valor_trans'],2,',','.')?></td>
                                                         <td><?php  echo $liga['data_trans']?></td>
                                                         <td><?php  echo $liga['tipo']?></td>
                                                         <td><?php  echo "Factura"?></td>
                                                         <td><?php  echo $liga['nDocumento']?></td>
                                                         <td><?php  echo $liga['cliente']?></td>
                                                         <td><?php  echo $liga['utilizador'] ?></td>
                                                    </tr> 
                                                  <?php
                                                  }
                                                  ?>
                                                </tbody>

											</table>
                                        </div>
									</div>
                                    <!--Caixas Final dos Anteriores-->
                                </li>
                                <li>
                                   <table class="table">
                                       <tr>
                                           <td>
                                                <button class="md-btn md-btn-primary md-btn-block md-btn-wave-light" data-uk-modal="{target:'#modal_default4'}">Dev. a fornecedores
                                                </button>
                                            </td>
                                            <td>
                                            	<?php $codFact=rand(0,1000).'/'.date('s');?>
                                                <a  href="vendas.php?XX=<?php echo $codFact ?>" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Vendas 
                                                </a>
                                            </td>
                                            <td>
                                                <button class="md-btn md-btn-primary md-btn-block md-btn-wave-light" data-uk-modal="{target:'#modal_default4'}">Outros
                                                </button>
                                            </td>
                                       </tr>
                                   </table>
                                </li>
                            </ul>	
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
        <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
    
    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>
</body>
</html>