<?php
    require_once("iuda_Shop.php");
    $camposr_BD = new conexao();
    $liggar=$camposr_BD->conectar();
    $player= new Operacao();
    $busca="SELECT*from produto as p INNER JOIN unidades as un on p.unidades_idunidades=un.idunidades inner join familia as fam on p.familia_idfamilia=fam.idfamilia";
	
	$queryNPub="SELECT*from produto as p  inner join familia as fam on p.familia_idfamilia=fam.idfamilia WHERE Publicado=0";
    $objProdutos=$player->select($busca,$liggar);
	$objProdutosNP=$player->select($busca,$liggar);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>Lista de Produtos</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <style type="text/css">
        tr:nth-child(even) {background-color: #DDD3D3}
    </style>
</head>
    <body class="disable_transitions sidebar_main_open sidebar_main_swipe">
        <!-- main header -->
        <?php
            require("header.php");
        ?>
        <!-- main header end -->
        <!-- main sidebar -->
        <?php
            if($_SESSION['previlegio']=='Administrador')
            {
                require_once('menus\menuAdmin.php');
            }

            if($_SESSION['previlegio']=='Gvnd')
            {
                require_once('menus\menuGerenteVendas.php');
            }

            if($_SESSION['previlegio']=='Gstck')
            {
                require_once('menus\menuGerenteStock.php');
            }
        ?>
        <!-- main sidebar end -->

        <div id="page_content">
		
		
		
            <div id="page_content_inner">
                <h3 class="heading_b uk-margin-bottom">Lista de Produtos 
                    <div class="uk-width-medium-1-6">
                                <a href="Registo_Produtos.php" class="md-btn md-btn-success md-btn-wave-light" href="javascript:void(0)">Novo</a>
                    </div>
                </h3>
				
				
			  <div class="uk-width-medium">                   
                    <div class="md-card">
                        <div class="md-card-content">
                            <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
                                <li class="uk-width-1-3 uk-active"><a href="#">Todos</a></li>
                                <li class="uk-width-1-3"><a href="#">Não publicados</a></li>                                
                            </ul>
                            <ul id="tabs_4" class="uk-switcher uk-margin">
                                <li>
								  
								  <div class="md-card uk-margin-medium-bottom">
               
								<div class="md-card-content">
									<div class="dt_colVis_buttons"></div>
									<table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
										<thead>
										<tr>
											<th>Número</th>
											<th>Descrição</th>
											<th>Marca</th>
											<th>Categoria</th>
											<th>Unidades</th>
											<th>Preço de venda</th>                                 
											<th>IPC(valor)</th>
											<th>Preço Final</th>
											<th>Preço de Compra</th>
											<th>Stock Qtd</th>
											<th>Stock Valor</th>
											<th>Codigo de Barras</th>   
											<th>Acções</th>
										</tr>
										</thead>
										<tbody>
											<?php
												$i=0;
												while( $campos=$objProdutos->fetch_assoc()){
													$i+=1;
													$id=$campos['idProduto'];
													$data=$campos['data_registo'];         
											?>
											<tr>
												<td><?php echo $i ?></td>
												<td><?php echo $campos["Desigacao"] ?></td>
												<td><?php echo $campos['marca']?></td> 
												<td><?php echo $campos['familia']; ?></td>
												<td><?php echo $campos["unidade"];?></td>
												<td><?php echo number_format($campos['Pvenda'],2,',','.')?></td>
											
												<td>
													<?php 
													   $ipcVal=$campos['Pvenda']; 
													echo number_format($ipcVal,2,',','.');
													?>
													

												</td>
												<td>
													<?php 
														$valorFinal=$ipcVal+$campos['Pvenda'];
													echo number_format($valorFinal,2,',','.');
													?>
													
												</td>
												 
												   <td><?php echo number_format($campos['Pcompra'],2,',','.')?></td>
												<td><?php echo $q=$campos['quantidade'] ?></td>
												<td><?php echo number_format($q*$campos['Pcompra'],2,',','.') ?></td>
												<td><?php echo $campos["cod_Barras"] ?></td>
												<td>
													<a href="#" data-uk-modal="{target:'#modal_default<?php echo $i; ?>'}"><i class="material-icons">info</i>
													</a> 
													<!-- modal do artigo-->
														  <div class="uk-modal" id="modal_default<?php echo $i;?>">
											<div class="uk-modal-dialog">
												<a class="uk-modal-close uk-close"></a>
												<h3><?php echo $campos['Desigacao']?></h3>
													<div class="uk-width-1-1">
														<ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content<?php echo $i?>'}" id="tabs_1">
															<li class="uk-active"><a href="#">Dados Gerais</a></li>
															<li><a href="#">Stock</a></li>
															<li><a href="#">Transações</a></li>
															
														</ul>
														<ul id="tabs_1_content<?php echo $i?>" class="uk-switcher uk-margin">
															<li>
																<div class="content">
																	<table class="table" style="width: 100%;"> 
																		<tr>
																			<td><strong>Descrição</strong></td>
																			<td><?php echo $campos['Desigacao'];?></td>
																		</tr>
																		<tr>
																			<td><strong>Código de Barras</strong></td>
																			<td><?php echo $campos['cod_Barras'];?></td>
																		</tr>
																		<tr>
																			<td><strong>Marca</strong></td>
																			<td><?php echo $campos['marca'];?></td>
																		</tr>
																		<tr>
																			<td><strong>Família</strong></td>
																			<td><?php echo $campos['familia'];?></td>
																		</tr>
																		<tr>
																			<td><strong>Unidade</strong></td>
																			<td><?php echo $campos['unidade'];?></td>
																		</tr>
																		<tr>
																			<td><strong>Preço de Venda</strong></td>
																			<td><?php echo number_format($campos['Pvenda'],2,',','.') ;?></td>
																		</tr>
																		<tr>
																			<td><strong>Preço de Compra</strong></td>
																			<td><?php echo 
																			
																			number_format($campos['Pcompra'],2,',','.')
																			?></td>
																		</tr>
																	</table>
																</div>
															</li>
															  <li style="height: 300px;">
																<table class="table" style="width: 100%;">
																	<tr>
																		<td>Stock disponível</td>
																		<td><?php echo $campos['quantidade']?></td>
																	</tr>
																	 <tr>
																		<td>Stock em valor</td>
																		<td><?php 
																		$Stkvalor= $campos['quantidade']*$campos['Pvenda'];
																		echo number_format($Stkvalor,2,',','.');
																		?></td>
																	</tr>
																	<tr>
																		<td>Stock Máximo</td>
																		<td><?php echo $campos['stkmax']?></td>
																	</tr>
																	<tr>
																		<td>Stock Mínimo</td>
																		<td><?php echo $campos['stkmin']?></td>
																	</tr>
																   
																</table>
															</li>
															
															<li style="height: 300px;  overflow-y: scroll;">
															   <div class="dt_colVis_buttons"></div>
																<table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
																   <th>#</th>
																   <th>Data</th>
																   <th>Hora</th>
																   <th>Tipo</th>
																   <th>Nº Doc</th>
																   <th>Qtd.</th>
																   <th>P.Uni</th>
																   <th>Valor</th>
																<?php
																	$b="select*from transacoes_estoques where Produto_idProduto=$id";
																	$forn=$player->select($b,$liggar);
																	$a=0;
																	while( $registos=$forn->fetch_assoc()){
																		$a++;
																		$response=explode(' ', $registos['data_trans']);
																		$data=$response[0];
																		$hora=$response[1];
																		?>
																	   <tr>      
																		<td><?php echo $a?></td>
																		<td>
																			<?php echo $data ?></td>
																		 <td>
																			<?php echo $hora ?></td>
																		<td><?php echo $registos['tipo'];?></td>
																		<td><?php echo $registos['nDocumento'];?></td>
																		<td><?php echo $registos['quantidade'];?></td>
																		<td>
																			<?php echo number_format( $registos['preco'],2,',',' ')?>
																		</td> 
																		<td>
																			<?php echo number_format($registos['valor'],2,',','.') ;?>
																		</td>
																	   </tr>

																<?php
																	}
																?>
																  </table>
															</li>
														  
														</ul>
													</div>
													<div class="uk-modal-footer uk-text-right">

														<a class="md-btn md-btn-primary md-btn-wave-light" href="Edit_Produtos.php?XX=<?php echo $id;?>">Editar</a>

														<a class="md-btn md-btn-success md-btn-wave-light" href="extrArtigo.php?XX=<?php echo $id;?>">Extrato</a>
													
													</div>
											</div>

											</div>
													<!-- modal do artigo-->
											 </td>
										</tr>
										<?php
										} 
										?>
									  </tbody>
									</table>
								</div>
							</div>
								  
								  
								  
								  
								</li>
                                <li>																
									<div class="md-card-content">
										<table class="uk-table" id="dt_colVis">
												<tr>                                
													<th>#</th>
													<th>Produto</th>
													<th>Catgoria</th>													
													<th>Vendedor</th>													
													<th>Fotos</th>
													<th>Observações</th>
											   </tr>
											   <?php
												$i=0;
												while(  $c=$objProdutosNP->fetch_assoc())
												{ $i++;
												?>
													<tr>                                
													<td><?php echo $i; ?></td>
													<td><?php echo $c["Desigacao"] ?> </td>
													<td><?php echo $c["familia"] ?> </td>													
													<td><a><?php echo $c["vendedor"] ?> </a></td>
													<td>Fotos</th>
													<td><?php echo $c["obs"] ?> </td>
											   </tr>
											   <?php
												}
											   ?>
											   
											   
										</table>
									</div>
								</li>                                
                            </ul>
                        </div>
                    </div>
                </div>												               
            </div>
        </div>

        <!-- common functions -->
        <script src="assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="assets/js/uikit_custom.min.js"></script>
        <!-- altair common functions/helpers -->
        <script src="assets/js/altair_admin_common.min.js"></script>

        <!-- page specific plugins -->
        <!-- datatables -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <!-- datatables buttons-->
        <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
        <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
        <script src="bower_components/jszip/dist/jszip.min.js"></script>
        <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
        <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
        
        <!-- datatables custom integration -->
        <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

        <!--  datatables functions -->
        <script src="assets/js/pages/plugins_datatables.min.js"></script>
    </body>
</html>