$(document).ready(function() {
(function($) {
  RemoveTableRow = function(handler) {
    var tr = $(handler).closest('tr');
    tr.fadeOut(100, function(){ 
      tr.remove();
      soma(); 
    }); 
    return false;
  };
 
  AddTableRow = function() {
      var newRow = $("<tr>");
      var cols = "";
      var produto= $("#pesquisa").val(); 
      var preUni= $("#Punit").val();
      var vallor= $("#totVal").val();
      if (produto=="") {
        UIkit.modal.alert('Tem que selecionar um produto para vender!!');
      } else{
        var qtd=$("#qtd").val();
            if (qtd=="") {
              qtd=1;
             }
            cols += '<td>'+produto+'</td>';
            cols += '<td>'+preUni+'</td>';
            cols += '<td>'+qtd+'</td>';
            cols += '<td>'+vallor+'</td>';
            cols += '<td class="actions" style="width:30px; text-align:center;  font-weight:bold">';
            cols += '<a  onclick="RemoveTableRow(this);" style="font-size:22px; color:#ff0000;">x</a>';
            cols += '</td>';
            $("#pesquisa").val("");
            $("#pesquisa").focus();
            $("#Punit").val("");
            $("#qtd").val(1);
            $("#totVal").val(""); 
            newRow.append(cols);
            $("#products-table").append(newRow);
             return false;
        }
    }   
  })(jQuery);
  
});