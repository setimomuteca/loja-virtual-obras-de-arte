$(document).ready(function() {
	$('#chk').click(function(event) {
		if (this.checked) {
			$('.ArrRadio').removeAttr("disabled"); 
		}else{
			$('.ArrRadio').attr('disabled','true');
		}
	});
	$('#filtroProdutos').change(function(event) {
		var dados=$('#filtroProdutos').val();
		if (dados==-1) {
			$('#buscaFiltro').attr('disabled','true');
		}else{
			$('#buscaFiltro').removeAttr("disabled"); 
			$.post('buscaSettings.php',dados, function(retorno){
					$(".resultado").html(retorno);
				})
		}	
	});
});