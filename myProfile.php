<?php
   if (isset($_GET['uknw'])) {
       $id=$_GET['uknw'];

   } else if (isset($_GET['dstv'])) {
      $aviso="<span style='color:red; font-weight: bold'>Password antiga errada!</span>";
      $id=$_GET['dstv'];

   }else if (isset($_GET['sprd'])) {
      $aviso="<span style='color:red; font-weight: bold'>As Password são diferentes!</span>";
      $id=$_GET['sprd'];
   } 


?>
<!doctype html>
 <html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Sistema de vendas</title>

    <!-- additional styles for plugins -->
        <!-- weather icons -->
        <link rel="stylesheet" href="bower_components/weather-icons/css/weather-icons.min.css" media="all">
        <!-- metrics graphics (charts) -->
        <link rel="stylesheet" href="bower_components/metrics-graphics/dist/metricsgraphics.css">
        <!-- chartist -->
        <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">
      <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require("header.php");
    ?>
    <?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();

    $busca="select*from pessoal where idPessoal=$user";
    $registos=$player->select($busca,$liggar);
    $i=1;    
   while($linha=$registos->fetch_assoc()){
        $nome=$linha['nome'];
        $telefone=$linha['telefone'];
        $email=$linha['email'];
        $previl=$linha['Previlegio'];
        $senha=$linha['senha'];
    }
?>
    <!-- main header end -->
    <!-- main sidebar -->
    <?php 
        require("menus/menuAdmin.php");
    ?>
    
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
             <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                               
                            </div>
                            <div class="user_heading_avatar">
                                <div class="thumbnail">
                                    <img src="assets/img/avatars/avatar_11.png" alt="user avatar"/>
                                </div>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?php echo $nome;?></span><span class="sub-heading"><?php $previl; ?></span></h2>
                               
                            </div>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Dados Gerais</a></li>
                                <li><a href="#">Alterar a Senha</a></li>
                            </ul>
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $email ?></span>
                                                        <span class="uk-text-small uk-text-muted">
                                                            <?php echo "Email"?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $telefone ?></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                       <i class="md-list-addon-icon material-icons">
                                                        settings_input_component
                                                        </i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $previl ?></span>
                                                        <span class="uk-text-small uk-text-muted">Categoria</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                  <div class="box" style="width: 400px; margin:5% auto;">
                                    <form action="changePass.php" method="POST">
                                      <div class="box-header">
                                          <h3 style="background: #f3f3f3; padding: 4px;">Alterar a Senha</h3>
                                      </div>
                                      <div class="box-body">
                                        <div class="uk-form-row">
                                            <input type="hidden" name="chave" readonly value="<?php echo $user ?>">
                                            <input type="hidden" name="senhaActual" readonly value="<?php echo $senha ?>">
                                            <label>Senha Actual</label>
                                            <input type="password" name="senhaAntiga" class="md-input">
                                        </div>
                                         <div class="uk-form-row">
                                            <label>Nova Senha</label>
                                            <input type="password" id="senha" name="new_pass" class="md-input">
                                        </div>
                                         <div class="uk-form-row">
                                            <label>Repita a Senha</label>
                                            <input type="password" name="pass2" class="md-input">
                                        </div>
                                         <div class="uk-form-row">
                                            
                                            <button type="submit" id="salvar" name="salvar" class="md-btn md-btn-success md-btn-wave-light" >Salvar</button>    
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </li>
                            </ul>
                        </div>
                    </div>
        </div>
    </div>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
        <!-- d3 -->
        <script src="bower_components/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
        <script src="bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <!-- maplace (google maps) -->
        <script src="http://maps.google.com/maps/api/js"></script>
        <script src="bower_components/maplace-js/dist/maplace.min.js"></script>
        <!-- peity (small charts) -->
        <script src="bower_components/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <script src="bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <!-- countUp -->
        <script src="bower_components/countUp.js/dist/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="bower_components/handlebars/handlebars.min.js"></script>
        <script src="assets/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="bower_components/clndr/clndr.min.js"></script>

        <!--  dashbord functions -->

</body>
</html>