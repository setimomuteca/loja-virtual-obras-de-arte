<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
<script type="text/javascript">
	function calc(a,b,c){
		var a= a.innerHTML;
		var quant=b.value;
		c.innerHTML=(a*quant);
	}
	function soma(){
          var tble=document.getElementById('table');
          var soma=0;
          var coluna=0;
          for (var i =1; i < tble.rows.length; i++){
        	 	coluna=coluna + parseInt(tble.rows[i].cells[2].innerHTML);
                 
                 }
                document.getElementById('somaTotal').value=coluna;



      }

           
           $(document).ready(function(){
           		$(".campoValor").change(function(){
           			$("#somaTotal").focus();
           			$("#somaTotal").maskMoney({thousands:'.', decimal:',', allowZero: true, suffix: ' akz',});


           		});
           		$(".valorv").maskMoney({thousands:'.', decimal:',', allowZero: true, suffix: ' akz'});
           		$("#zerar").click(function() {
           		$(".valorv").text("0");

           		});
           	
           });
          

	</script>
	<form>
	<table class="table" id="table">
		<thead> 
			<tr>
				<th>Nota/Moeda</th>
				<th>Quantidade</th>
				<th>Valor</th>
			</tr>
		</thead>
			<tbody>
				<tr>
					<td><span id="cinco">5</span> AKZ</td>
					<td>
						<input type="number" class="campoValor"  name="valor" min="0" onchange="calc(cinco,this,five);soma()">
					</td>
					<td id="five" class="valorv">
						0
					</td>

				</tr>
				<tr>

					<td><span id="dez">10</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(dez,this,ten);soma()">
					</td>
					<td id="ten" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="vinte">20</span> AKZ</td>
					<td>
						<input type="number"  name="valor" class="campoValor" min="0" id="vinteT" onchange="calc(vinte,this,twenty); soma()">
					</td>
					<td id="twenty" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="cinquenta">50</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(cinquenta,this,fifty);soma()">
					</td>
					<td id="fifty" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="cem">100</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(cem,this,under);soma()">
					</td>
					<td id="under" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="dduz">200</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(dduz,this,duz);soma()">
					</td>
					<td id="duz" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="quin">500</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(quin,this,quinh);soma()">
					</td>
					<td id="quinh" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="mil">1000</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(mil,this,mill);soma()">
					</td>
					<td id="mill" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="ddmil">2000</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(ddmil,this,ddmill);soma()">
					</td>
					<td id="ddmill" class="valorv">
						0
					</td>
				</tr>
				<tr>
					<td><span id="cmil">5000</span> AKZ</td>
					<td>
						<input type="number" name="valor" class="campoValor" min="0" onchange="calc(cmil,this,cmiil);soma()">
					</td>
					<td id="cmiil" class="valorv">
						0
					</td>
				</tr>
			</tbody>
</table>
	 <div class="input-group">
    	<span class="input-group-addon">Total Geral</span>
    	<input type="text" class="form-control" name="msg" placeholder="Total Geral" 
    	style="text-align: right; font-weight: bolder" readonly id="somaTotal">
  	</div><br>
<input type="reset" name="reinicio" value="zerar Contador" id="zerar" class="md-btn md-btn-primary" >
	</form>



