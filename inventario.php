 <?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="select distinct(familia_idfamilia), familia from produto inner join familia on familia_idfamilia=idfamilia";
    $forneceddores=$player->select($busca,$liggar);
    $i=1;    
?>




<!doctype html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Inventários||Sistema de Vendas</title>

    <!-- additional styles for plugins -->
        <!-- weather icons -->
        <link rel="stylesheet" href="bower_components/weather-icons/css/weather-icons.min.css" media="all">
        <!-- metrics graphics (charts) -->
        <link rel="stylesheet" href="bower_components/metrics-graphics/dist/metricsgraphics.css">
        <!-- chartist -->
        <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
    <?php 
        require("menus/menuAdmin.php");
    ?>
    
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
           <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                     <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                                <i class="md-icon material-icons">&#xE5D5;</i>
                                <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <i class="md-icon material-icons">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="facturas/inventarioGeralPDF.php">Exportar PDF</a></li>
                                            <li><a href="#">Imprimir</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Relatório-Inventário
                            </h3>
                        </div>
                </div>
                <div class="md-card-content">
                  
                       <?php
                       $grandTotal=0;
                        while( $linha=$forneceddores->fetch_assoc()){
                           
                             $fam= $linha['familia_idfamilia'].'<br>';
                           echo "<h5 style='background:#F3F3F3; padding:3px;'>".$linha['familia']."</h5>";
                            $query="select*from produto where familia_idfamilia='$fam'";
                            $objQuery=$player->select($query,$liggar);
                            echo  "<table class='uk-table' style='border-top:solid 1px black'>";
                            echo "<tr><th>Produto</th><th>qty</th><th>preço</th><th style='text-align:right;'>valor akz</th></tr>";
                            $total=0;
                            while( $registo=$objQuery->fetch_assoc()){
                                echo "<tr><td style='width:300px;'>".$registo['Desigacao']."</td><td style='width:150px;'>".$registo['quantidade']."</td>";
                                echo "<td style='width:150px;'>".number_format($registo['Pvenda'],2,',',' ')."</td>";
                                echo "<td style='width:150px; text-align:right;'>".number_format($valor=$registo['Pvenda']*$registo['quantidade'],2,',',' ')."</td></tr>";
                                $total+=$valor;
                            };
                            echo "<tr><td>total ".$linha['familia']."</td>";
                            echo"<td colspan='3' style='text-align:right; border-button:solid 2px black'>".number_format($total,2,',',' ')."</td></tr>";
                            echo "</table>"; 
                           $grandTotal+=$total;
                            
                        }
                         echo "<table class='uk-table' style='width:100%;'><tr><th>TOTAL</th><th style='text-align:right;'>".number_format($grandTotal,2,',',' ')."</th></tr></table>";

                         
                       ?>
                                 

                </div>
           </div>
        </div>
    </div>


  

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
        <!-- d3 -->
        <script src="bower_components/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
        <script src="bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <!-- chartist (charts) -->
        <!-- maplace (google maps) -->
        <script src="bower_components/maplace-js/dist/maplace.min.js"></script>
        <!-- peity (small charts) -->
        <script src="bower_components/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <!-- countUp -->
        <script src="bower_components/countUp.js/dist/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="bower_components/handlebars/handlebars.min.js"></script>
        <script src="assets/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="bower_components/clndr/clndr.min.js"></script>

   
</body>
</html>