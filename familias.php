<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="SELECT*from familia order by familia asc ";
    $forneceddores=$player->select($busca,$liggar);
    $i=1;
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>FAMILIAS</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript">
        function confirma(id){
            window.location.href="delete_fam.php?id="+id;
        }
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        if($_SESSION['previlegio']=='Administrador')
        {
            require_once('menus\menuAdmin.php');
        }
        if($_SESSION['previlegio']=='Gvnd')
        {
            require_once('menus\menuGerenteVendas.php');
        }

        if($_SESSION['previlegio']=='Gstck')
        {
            require_once('menus\menuGerenteStock.php');
        }
   ?>
    <!-- main sidebar end -->
    <div id="page_content">
        <div id="page_content_inner">
                <div class="uk-grid uk-grid-medium" >
                    <form action="operacoes.php" method="POST" class="uk-form-stacked" id="product_edit_form">
                            <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                                <div class="md-card" style="width: 200px;">
                                    <div class="md-card-toolbar">
                                        <h3 class="md-card-toolbar-heading-text">
                                          Registar nova familia
                                        </h3>
                                    </div>
                                    <div class="md-card-content">
                                        <div class="uk-form-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="uk-icon-sliders uk-icon-medium"></i>
                                                </span>
                                                <label for="product_edit_price_control">Familia</label>
                                                <input type="text" required class="md-input" name="product_edit_price_control" id="product_edit_price_control"/>
                                                <input type="hidden" name="cat_reg" value="familia">
                                            </div>
                                        </div>
                                        <div class="uk-form-row">
                                              <button class="md-btn md-btn-primary" name="guardarFamilia" type="submit">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>
          
       

                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                   Familias Registadas
                                </h3>
                            </div>
                        <div class="md-card-content large-padding">
                               
                        <div class="md-card uk-margin-medium-bottom">
                         <div class="md-card-content">
                        <div class="uk-overflow-container">
                        <table class="uk-table uk-text-nowrap">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Familia</th>
                                    <th colspan="2">Acções</th>
                                </tr>
                            </thead>
                            <?php
                                if (isset($_GET['info'])) {
                                echo "<tfoot>
                                        <tr>
                                            <td colspan='3' style='color:#D04545; font-weight: bolder;'>
                                                Não pode ser eliminado,  existem produtos nessa familia!
                                            </td>
                                        </tr>
                                    </tfoot>";
                                }
                            ?>
                            <tbody>
                                <?php
                                    $ff=array();
                                    $i=0;
                                    while( $liga=$forneceddores->
                                        fetch_assoc()):
                                    ?>
                            <tr>
                                <td><?php echo $i?></td>
                                <td><?php echo $liga["familia"];?></td>
                                <td>
                                <button class="md-btn" style="border:none; background: none;"  data-uk-modal="{target:'#modal_default<?php echo $i ?>'}">editar
                                </button>
                                <div class="uk-modal" id="modal_default<?php echo $i ?>">
                                <div class="uk-modal-dialog">
                                    <button type="button" class="uk-modal-close uk-close"></button>
                                    <div class="md-card-content">
                                    <form action="editIPC.php" method="POST">
                                         <div class="uk-form-row">
                                            <div class="uk-input-group" style="width: 100%">
                                                <label for="product_edit_price_control">Família</label>
                                                <input type="text" readonly="readonly" min="0" max="100" required class="md-input" name="familia" value="<?php echo $liga["idfamilia"] ?>" id="product_edit_price_control"/><br><br><br>
                                                <label for="product_edit_price_control">Descrição</label>
                                                <input type="text" required class="md-input" name="Editdescricao" value="<?php echo $liga["familia"] ?>" id="product_edit_price_control"/>
                                                <input type="hidden" name="idIpc" value="<?php echo $liga['familia'];?>">
                                            </div>
                                        </div>
                                        <div class="uk-form-row">
                                            <button type="submit" class="md-btn md-btn-primary" name="EditFamilia">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                                </td>
                                <td>
              
                 
       <a href="#" style="width: 30px;" type="button" class="md-btn  md-btn-mini md-btn-wave-light" onclick="UIkit.modal.confirm('Tem certeza que pretende eliminar esse registo?', function(){
                        confirma('<?php echo $liga['idfamilia'] ?>');
             });">
            <span style="color:red; font-weight: bolder; font-size: 18px; padding: 4px; left: -9px; position: relative;">X</span>
        </a>
    </td>
        </tr>
            <?php
            $i+=1; 
                endwhile;
            ?>
            </tbody>
        </table>
                    </div>
                </div>
            </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
    
    <script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>