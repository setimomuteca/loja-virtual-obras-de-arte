<?php
   $idCaixa=$_GET['cx'];
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $operacao= new Operacao();
    $query="SELECT*from caixa INNER JOIN transacaocaixa on transacaocaixa.Caixa_idCaixa=caixa.idCaixa  where Caixa_idCaixa=$idCaixa";
    $objQuery=$operacao->select($query,$liggar);
    $i=0;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>Movimentos do Caixa</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <style type="text/css">
        tr:nth-child(even) {background-color: #DDD3D3}
    </style>
</head>
    <body class="disable_transitions sidebar_main_open sidebar_main_swipe">
        <!-- main header -->
        <?php
            require("header.php");
        ?>
        <!-- main header end -->
        <!-- main sidebar -->
        <?php
            require("menus/menuAdmin.php");
        ?>
        <!-- main sidebar end -->

        <div id="page_content">
            <div id="page_content_inner">
                <h3 class="heading_b uk-margin-bottom">Movimento de caixa</h3>
                <div class="md-card uk-margin-medium-bottom">
               
                    <div class="md-card-content">
                        <div class="box" style="margin-bottom: 20px;">
                           <div></div>
                        </div >
                        <div class="dt_colVis_buttons"></div>
                        <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Data/Hora</th>
                                <th>Movimento</th>
                                <th>Valor</th>
                                <th>Observação</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i=0;
                                    while ($linhas=$objQuery->fetch_assoc()) {
                                        $i+=1;
                                        $id=$linhas['idCaixa'];
                                        //$data=$linhas['data_registo'];         
                                ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $linhas["dataTransacao"] ?></td>
                                    <td><?php echo $linhas['tipo']?></td> 
                                    <td><?php echo number_format($linhas['valor'],2,',',' '); ?></td>
                                    <td><?php echo $linhas['obs'] ?></td>
                                </tr>
                                <?php
                                } 
                                ?>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- common functions -->
        <script src="assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="assets/js/uikit_custom.min.js"></script>
        <!-- altair common functions/helpers -->
        <script src="assets/js/altair_admin_common.min.js"></script>

        <!-- page specific plugins -->
        <!-- datatables -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <!-- datatables buttons-->
        <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
        <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
        <script src="bower_components/jszip/dist/jszip.min.js"></script>
        <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
        <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
        
        <!-- datatables custom integration -->
        <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

        <!--  datatables functions -->
        <script src="assets/js/pages/plugins_datatables.min.js"></script>
    </body>
</html>