<?php 
    if (isset($_GET['XX'])) {
        $idF=$_GET['XX'];
        require_once("iuda_Shop.php");
        $ligar_BD = new conexao();
        $liggar=$ligar_BD->conectar();
        $player= new Operacao();
        $busca="select*from fornecedores where idfornecedores='$idF'";
        $registos=$player->select($busca,$liggar);
        while( $liga=$registos->fetch_assoc()){
            $nome=$liga['nome'];
            $endereco=$liga['endereco'];
            $identidade=$liga['identidade'];
            $numeroContrib=$liga['numContrib'];
            $telefone=$liga['telefone'];
            $email=$liga['email'];
            $desconto=$liga['desconto'];
            $credito=$liga['credito'];
        };
    };
?>
<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>Registo de Novos Clientes</title>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript">
    	function testes(campo){
    		if (campo.value>100) {
    			campo.value=100;
    		}
    	}
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require("menu.php");
   ?>
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
            <form action="client_update.php" method="POST" class="uk-form-stacked" id="product_edit_form">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text" style="font-weight: bold">
                                    Editar a ficha dos clientes
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                        <div class="uk-form-row">
                                            <label for="product_edit_name_control">Nome do cliente</label>
                                            <input type="text" class="md-input" required id="nome" value="<?php echo $nome;?>" name="userName"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Endereço</label>
                                            <input type="text" class="md-input" required id="marca" name="endereco" value="<?php echo $endereco ?>"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Nº Identidade</label>
                                            <input type="text" class="md-input" required id="contr" name="identidade" value="<?php echo $numeroContrib  ?>"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Nº Contribuinte</label>
                                            <input type="text" class="md-input" required id="contr" name="Contribuinte" value="<?php echo $numeroContrib  ?>"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Telefone</label>
                                            <input type="text" class="md-input" required id="marca" name="telefone" value="<?php echo $telefone ?>"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Email</label>
                                            <input type="text" class="md-input" id="marca" name="email" value="<?php echo $email ?>" required/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Desconto(%)</label>
                                         <input type="text" class="md-input" id="desconto" min="0" max="100" name="desconto" <?php echo $desconto ?>/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Limite de Crédito(akz)</label>
                                         <input type="text" class="md-input" id="desconto" min="0" max="100" name="limiteCred" <?php echo $credito  ?> />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="md-fab-wrapper">
                    <input type="submit" name="guardar" id="btn_Guardar" value="Guardar">
                    <i class="material-icons"></i>
                 </div>
            </form>

        </div>
    </div> 
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
    
    <script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

<!-- Mirrored from altair_html.tzdthemes.com/ecommerce_product_edit.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Sep 2017 11:23:05 GMT -->
</html>