-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 21-Set-2020 às 11:06
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `seven`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa`
--

CREATE TABLE `caixa` (
  `idCaixa` int(10) UNSIGNED NOT NULL,
  `Pessoal_idPessoal` int(10) UNSIGNED NOT NULL,
  `Valor` int(10) UNSIGNED DEFAULT NULL,
  `Abertura` datetime DEFAULT NULL,
  `Fecho` datetime DEFAULT NULL,
  `status_Caixa` char(1) COLLATE latin1_general_cs DEFAULT NULL,
  `SaldoFinal` int(10) UNSIGNED DEFAULT NULL,
  `Adicionado` float DEFAULT NULL,
  `Retirado` float DEFAULT NULL,
  `saldoFinalInformado` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `caixa`
--

INSERT INTO `caixa` (`idCaixa`, `Pessoal_idPessoal`, `Valor`, `Abertura`, `Fecho`, `status_Caixa`, `SaldoFinal`, `Adicionado`, `Retirado`, `saldoFinalInformado`) VALUES
(24, 2, 500, '2018-07-30 13:24:28', '2018-07-30 14:46:58', 'F', 500, 0, 0, 500),
(25, 2, 6300, '2018-07-31 08:45:24', '2018-08-01 12:23:05', 'F', 13188, 50, 3000, 13188),
(26, 2, 6000, '2018-08-01 13:32:43', '2018-08-01 14:02:11', 'F', 6150, 300, 150, 6400),
(27, 2, 0, '2018-08-01 14:10:27', '2018-08-06 17:02:24', 'F', 2599008, 10000, 89000, 49899),
(28, 2, 0, '2018-08-07 10:48:11', '2019-06-23 19:03:30', 'F', 2516342, 0, 0, 10000),
(29, 4, 0, '2019-06-24 20:14:37', '2020-06-16 14:07:10', 'F', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho`
--

CREATE TABLE `carrinho` (
  `session_id` varchar(60) NOT NULL,
  `produto` int(10) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL DEFAULT 'A',
  `DocId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `carrinho`
--

INSERT INTO `carrinho` (`session_id`, `produto`, `quantidade`, `preco`, `estado`, `DocId`) VALUES
('3s27bnj62vieknjbctlimlfpo9', 44, 5, 200, 'A', 0),
('3s27bnj62vieknjbctlimlfpo9', 34, 3, 33, 'A', 0),
('5bhg4e2jv3qn3ecljvr4qqtr75', 30, 8, 12000, 'A', 0),
('5bhg4e2jv3qn3ecljvr4qqtr75', 29, 8, 12300, 'A', 0),
('drksvjcgc3e1h5r2rocb9qf063', 29, 3, 12300, 'A', 7),
('drksvjcgc3e1h5r2rocb9qf063', 34, 30, 33, 'A', 7),
('2stj7icpv6p3697qbr6kq9m4n7', 29, 1, 12300, 'A', 8),
('lp0g047sivs4n4ik9gkbdcb9ak', 44, 5, 200, 'A', 9),
('lp0g047sivs4n4ik9gkbdcb9ak', 29, 3, 12300, 'A', 9),
('lp0g047sivs4n4ik9gkbdcb9ak', 34, 100, 33, 'A', 9),
('lp0g047sivs4n4ik9gkbdcb9ak', 33, 13, 300, 'A', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `endereco` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `endereco2` varchar(400) COLLATE latin1_general_cs DEFAULT NULL,
  `pais` varchar(5) COLLATE latin1_general_cs DEFAULT NULL,
  `cidade` varchar(30) COLLATE latin1_general_cs DEFAULT NULL,
  `identidade` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `telefone` varchar(20) COLLATE latin1_general_cs DEFAULT NULL,
  `email` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `desconto` float DEFAULT NULL,
  `credito` float DEFAULT NULL,
  `status_Clientes` char(1) COLLATE latin1_general_cs DEFAULT NULL,
  `limiteCred` float DEFAULT NULL,
  `numContrib` varchar(40) COLLATE latin1_general_cs DEFAULT NULL,
  `codPostal` varchar(20) COLLATE latin1_general_cs DEFAULT NULL,
  `infoAdicional` varchar(50) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nome`, `endereco`, `endereco2`, `pais`, `cidade`, `identidade`, `telefone`, `email`, `desconto`, `credito`, `status_Clientes`, `limiteCred`, `numContrib`, `codPostal`, `infoAdicional`) VALUES
(1, 'Virgem Maria', 'Cazenga ', '', NULL, NULL, 'AO923445454', '756756765', 's@gmail.com', 10, 0, 'A', 100, '123131', NULL, NULL),
(6, 'Francis Cris', 'Viana Cazenga', '', NULL, NULL, '54645654', '2434343434', 'Figueiredo@gmail.com', 16, 0, 'A', 100, '5467675BE123', NULL, NULL),
(9, 'Cliente indiferenciado', 'indiferenciado', '', NULL, NULL, '21123234234242', '92356576', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(12, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(13, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(14, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(15, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(16, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(17, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(18, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(19, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(20, 'Testes Stestes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '676756756', 'utututyu@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ''),
(21, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(22, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(23, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(24, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(25, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(26, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(27, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(28, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(29, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(30, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(31, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(32, 'Testes Ultimo Nome', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', 'Isso é somente um teste'),
(33, 'Testes Testes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ' Isso é somente um teste'),
(34, 'Testes Testes', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, 'erwerwe', 'werwrwer@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', '       Isso é somente um teste'),
(35, 'António Muteca', 'ututu', 'ututyutyu', 'AO', 'Luanda', NULL, '923425453', 'setimo1918@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', '     Testes'),
(36, 'Jonh Jonh', 'Rua', 'Rua', 'AF', 'Luanda', NULL, '9234545454', 'tests@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', '   Testes agora '),
(37, 'António Muteca', 'Rua Inicial', 'Rua Final ', 'AO', 'Luanda', NULL, '923425453', 'setimomuteca@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ' testes'),
(38, 'Jonh  Alison', 'Rua Inicial', 'Rua Final ', 'DZ', 'Luanda', NULL, '923425453', 'setimo1918@gmail.com', NULL, NULL, NULL, NULL, NULL, 'tetertergdgdfgdf', ' Isso é somente mais um teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contasbancarias`
--

CREATE TABLE `contasbancarias` (
  `id` int(11) NOT NULL,
  `numConta` varchar(30) DEFAULT NULL,
  `banco` varchar(10) DEFAULT NULL,
  `titular` varchar(60) DEFAULT NULL,
  `iban` varchar(40) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `contasbancarias`
--

INSERT INTO `contasbancarias` (`id`, `numConta`, `banco`, `titular`, `iban`, `activo`) VALUES
(1, '123434234234234', 'BPC', 'Loja Obra de Artes', 'AO000TRT343434546456', b'1'),
(2, '000567456676', 'Banco Sol', 'Loja Obra de Artes', '432423423234234234234', b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `divida`
--

CREATE TABLE `divida` (
  `iddivida` int(10) UNSIGNED NOT NULL,
  `Venda_Cliente_idCliente` int(10) UNSIGNED NOT NULL,
  `Venda_Modo_Pagamento_idModo_Pagamento` int(10) UNSIGNED NOT NULL,
  `Venda_idVenda` int(10) UNSIGNED NOT NULL,
  `Cliente_idCliente` int(10) UNSIGNED NOT NULL,
  `valor` int(10) UNSIGNED DEFAULT NULL,
  `dataRegisto` date DEFAULT NULL,
  `dataPagamento` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `idEmpresa` int(11) NOT NULL,
  `nomeComercial` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `nomeFiscal` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `endereco` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `telefone` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `NContribuinte` varchar(250) COLLATE latin1_general_cs NOT NULL,
  `logotipo` varchar(500) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`idEmpresa`, `nomeComercial`, `nomeFiscal`, `endereco`, `telefone`, `email`, `NContribuinte`, `logotipo`) VALUES
(4, 'SEVEN Tecnology', 'Grupo SÃ©timo Lda', 'Luanda Viana Zango3', '+244 923 456 677', 'testes@gmail.com', 'AO 1122547785', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `encomendas`
--

CREATE TABLE `encomendas` (
  `id` int(11) NOT NULL,
  `cliente` int(10) UNSIGNED NOT NULL,
  `pagamento` varchar(40) NOT NULL,
  `dataregisto` datetime DEFAULT current_timestamp(),
  `total` double DEFAULT NULL,
  `estado` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `encomendas`
--

INSERT INTO `encomendas` (`id`, `cliente`, `pagamento`, `dataregisto`, `total`, `estado`) VALUES
(1, 36, 'transferencia', '0000-00-00 00:00:00', 0, 'PEN'),
(2, 36, 'transferencia', '2020-07-08 20:44:03', 0, 'PEN'),
(3, 36, 'transferencia', '2020-07-08 21:01:25', 0, 'PEN'),
(4, 36, 'transferencia', '2020-07-08 21:01:42', 0, 'PEN'),
(5, 36, 'transferencia', '2020-07-08 21:02:20', 0, 'PEN'),
(6, 36, 'transferencia', '2020-07-08 21:05:19', 0, 'PEN'),
(7, 36, 'transferencia', '2020-07-08 21:05:43', 0, 'PEN'),
(8, 37, 'entrega', '2020-07-12 08:16:24', 0, 'PEN'),
(9, 38, 'transferencia', '2020-07-26 20:05:42', 0, 'PEN');

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada`
--

CREATE TABLE `entrada` (
  `idEntrada` int(10) UNSIGNED NOT NULL,
  `Produto_idProduto` int(10) UNSIGNED NOT NULL,
  `Produto_familia_idfamilia` int(10) UNSIGNED NOT NULL,
  `Modo_Pagamento_idModo_Pagamento` int(10) UNSIGNED NOT NULL,
  `Produto_unidades_idunidades` int(10) UNSIGNED NOT NULL,
  `DataEntrada` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Estrutura da tabela `familia`
--

CREATE TABLE `familia` (
  `idfamilia` int(10) UNSIGNED NOT NULL,
  `familia` varchar(45) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `familia`
--

INSERT INTO `familia` (`idfamilia`, `familia`) VALUES
(2, 'Frutas'),
(5, 'Bebidas Frias'),
(4, 'Eletrônicos'),
(6, 'AntibiÃ³ticos'),
(19, 'Quadros'),
(9, 'cosmÃ©sticos'),
(10, 'SmartPhones'),
(18, 'Acessóriosssss');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedores`
--

CREATE TABLE `fornecedores` (
  `idfornecedores` int(10) UNSIGNED NOT NULL,
  `companhia` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `correspondente` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `endereco` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `telefone` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `email` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `Ncontr` varchar(34) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `fornecedores`
--

INSERT INTO `fornecedores` (`idfornecedores`, `companhia`, `correspondente`, `endereco`, `telefone`, `email`, `Ncontr`) VALUES
(2, 'Cubo Vicio', 'Arlindo Matias', 'Luanda Sambizanga', '92555639874', 'testes@gmail.com', 'AO-112-334-223'),
(3, 'Do Carmo Ventura', 'Ventura', 'Calemba 2 ', '998930548', 'nelsonventur@gmail.com', '00667829LA772');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ipc`
--

CREATE TABLE `ipc` (
  `idIpc` int(11) NOT NULL,
  `descricao` varchar(500) COLLATE latin1_general_cs NOT NULL,
  `valor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `ipc`
--

INSERT INTO `ipc` (`idIpc`, `descricao`, `valor`) VALUES
(1, 'IVA', 14),
(5, 'Reduzida', 2),
(2, 'Isento', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ipcvalor`
--

CREATE TABLE `ipcvalor` (
  `idipcValor` int(11) NOT NULL,
  `dataIpc` date NOT NULL,
  `tipoDoc` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `valorDoc` float NOT NULL,
  `numDoc` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `valorIpc` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `ipcvalor`
--

INSERT INTO `ipcvalor` (`idipcValor`, `dataIpc`, `tipoDoc`, `valorDoc`, `numDoc`, `valorIpc`) VALUES
(10, '2018-07-28', 'FA', 21840, '24.8285714', 873.6),
(11, '2018-07-28', 'FA', 23712, '16.5555555', 948.48),
(12, '2018-07-28', 'FA', 509600, '10.8163265', 20384),
(13, '2018-07-28', 'FA', 250.7, '10.8163265', 22.563),
(14, '2018-07-28', 'FA', 250.7, '530/49', 22.563),
(15, '2018-07-28', 'FA', 250.7, '530/49', 22.563),
(16, '2018-07-29', 'FA', 169156, '607/21', 6800.02),
(17, '2018-07-29', 'FA', 16848, '548/54', 673.92),
(18, '2018-07-29', 'FA', 76.3, '924/11', 6.867),
(19, '2018-07-29', 'FA', 54500, '579/05', 4905),
(20, '2018-07-31', 'FA', 654, '138/24', 58.86),
(21, '2018-07-31', 'FA', 654, '138/24', 58.86),
(22, '2018-07-31', 'FA', 654, '138/24', 58.86),
(23, '2018-07-31', 'FA', 654, '138/24', 58.86),
(24, '2018-07-31', 'FA', 654, '138/24', 58.86),
(25, '2018-07-31', 'FA', 654, '138/24', 58.86),
(26, '2018-07-31', 'FA', 654, '569/00', 58.86),
(27, '2018-07-31', 'FA', 1716, '979/09', 68.64),
(28, '2018-07-31', 'FA', 130.8, '29/48', 11.772),
(29, '2018-07-31', 'FA', 1716, '29/48', 68.64),
(30, '2018-08-01', 'FA', 1716, '505/13', 68.64),
(31, '2018-08-01', 'FA', 132121, '823/27', 11858.6),
(32, '2018-08-01', 'FA', 57200, '853/52', 2288),
(33, '2018-08-01', 'FA', 57200, '853/52', 2288),
(34, '2018-08-01', 'FA', 57200, '853/52', 2288),
(35, '2018-08-01', 'FA', 675.8, '853/52', 60.822),
(36, '2018-08-01', 'FA', 6125.8, '853/59', 551.322),
(37, '2018-08-01', 'FA', 16848, '853/59', 673.92),
(38, '2018-08-01', 'FA', 16848, '853/80', 673.92),
(39, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(40, '2018-08-01', 'FA', 0, '853/52', 0),
(41, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(42, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(43, '2018-08-01', 'FA', 0, '853/52', 0),
(44, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(45, '2018-08-01', 'FA', 21840, '853/52', 873.6),
(46, '2018-08-01', 'FA', 13080, '853/52', 1177.2),
(47, '2018-08-01', 'FA', 13080, '853/52', 1177.2),
(48, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(49, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(50, '2018-08-01', 'FA', 250.7, '853/52', 22.563),
(51, '2018-08-01', 'FA', 250.7, '853/52', 22.563),
(52, '2018-08-01', 'FA', 250.7, '853/52', 22.563),
(53, '2018-08-01', 'FA', 1716, '853/52', 68.64),
(54, '2018-08-01', 'FA', 0, '853/52', 0),
(55, '2018-08-01', 'FA', 1716, '295/54', 68.64),
(56, '2018-08-01', 'FA', 1716, '295/54', 68.64),
(57, '2018-08-01', 'FA', 1716, '295/54', 68.64),
(58, '2018-08-01', 'FA', 4054.8, '295/54', 364.932),
(59, '2018-08-01', 'FA', 14796, '295/54', 1245.84),
(60, '2018-08-01', 'FA', 55378.7, '295/54', 4843.68),
(61, '2018-08-01', 'FA', 323040, '295/54', 18153.6),
(62, '2018-08-06', 'FA', 1856400, '699/20', 74256),
(63, '2018-08-07', 'FA', 0, '255/11', 0),
(64, '2018-08-07', 'FA', 0, '255/11', 0),
(65, '2018-08-07', 'FA', 0, '255/11', 0),
(66, '2018-09-17', 'FA', 67548, '144/51', 2701.92),
(67, '2018-09-17', 'FA', 72492, '89/41', 2797.68),
(68, '2018-09-17', 'FA', 1716, '89/41', 68.64),
(69, '2018-09-17', 'FA', 16848, '825/34', 673.92),
(70, '2018-09-17', 'FA', 16848, '825/34', 673.92),
(71, '2018-09-17', 'FA', 16848, '825/34', 673.92),
(72, '2019-05-01', 'FA', 624, '339/52', 24.96);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_vendas`
--

CREATE TABLE `itens_vendas` (
  `iditens_vendas` int(10) UNSIGNED NOT NULL,
  `Venda_Cliente_idCliente` int(10) UNSIGNED NOT NULL,
  `Venda_Modo_Pagamento_idModo_Pagamento` int(10) UNSIGNED NOT NULL,
  `Venda_idVenda` int(10) UNSIGNED NOT NULL,
  `Produto_idProduto` int(10) UNSIGNED NOT NULL,
  `Produto_familia_idfamilia` int(10) UNSIGNED NOT NULL,
  `Produto_unidades_idunidades` int(10) UNSIGNED NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `precUni` float DEFAULT NULL,
  `dataVenda` int(10) UNSIGNED DEFAULT NULL,
  `Factura` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `desconto` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Estrutura da tabela `linhasacertos`
--

CREATE TABLE `linhasacertos` (
  `id` int(11) NOT NULL,
  `NumDoc` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `qtd` int(11) NOT NULL,
  `dataTrans` datetime NOT NULL DEFAULT current_timestamp(),
  `idProduto` int(11) NOT NULL,
  `status` char(1) COLLATE latin1_general_cs NOT NULL DEFAULT 'P'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `linhasacertos`
--

INSERT INTO `linhasacertos` (`id`, `NumDoc`, `qtd`, `dataTrans`, `idProduto`, `status`) VALUES
(12, '726-37', 25, '2018-07-24 14:42:39', 5, 'F'),
(13, '622-18', 100, '2018-08-08 11:41:29', 5, 'F'),
(14, '795-29', 100, '2018-08-08 11:44:55', 28, 'F'),
(15, '679-18', 50, '2018-08-08 11:47:43', 28, 'F');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modo_pagamento`
--

CREATE TABLE `modo_pagamento` (
  `idModo_Pagamento` int(10) UNSIGNED NOT NULL,
  `Modo_Pagamento` varchar(255) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `modo_pagamento`
--

INSERT INTO `modo_pagamento` (`idModo_Pagamento`, `Modo_Pagamento`) VALUES
(1, 'Multicaixa'),
(2, 'dinheiro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfis`
--

CREATE TABLE `perfis` (
  `Perfil` char(10) NOT NULL,
  `Descricao` char(100) DEFAULT NULL,
  `Ativo` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `perfis`
--

INSERT INTO `perfis` (`Perfil`, `Descricao`, `Ativo`) VALUES
('Adm', 'Administrador', b'1'),
('Vend', 'Vendedor', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfis_utilizadores`
--

CREATE TABLE `perfis_utilizadores` (
  `Perfil` char(50) NOT NULL,
  `Pessoal` int(10) NOT NULL,
  `DataRegisto` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoal`
--

CREATE TABLE `pessoal` (
  `idPessoal` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `Previlegio` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `username` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `senha` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `telefone` varchar(60) COLLATE latin1_general_cs DEFAULT NULL,
  `statusFunc` char(1) COLLATE latin1_general_cs DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `pessoal`
--

INSERT INTO `pessoal` (`idPessoal`, `nome`, `Previlegio`, `username`, `senha`, `telefone`, `statusFunc`, `email`) VALUES
(2, 'Ã‚ngelo Boss', 'Vendedor', 'administrador', 'e10adc3949ba59abbe56e057f20f883e', '5555444', 'A', 'testes@gmail.com'),
(3, 'Ã‚ngelo Boss', 'Vendedor', 'Ã‚ngelo', 'e10adc3949ba59abbe56e057f20f883e', '5555444', 'A', 'testes@gmail.com'),
(4, 'Miguel Sebastião', 'Administrador', 'Admin', 'e10adc3949ba59abbe56e057f20f883e', '92569874', 'A', 'testes@gmail.com'),
(6, 'an', 'Gerente', 'Gerente', 'e10adc3949ba59abbe56e057f20f883e', '9234345', 'A', 'Gerente@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `idProduto` int(10) UNSIGNED NOT NULL,
  `familia_idfamilia` int(10) UNSIGNED NOT NULL,
  `unidades_idunidades` int(10) UNSIGNED NOT NULL,
  `Desigacao` varchar(27) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `cod_Barras` varchar(45) COLLATE latin1_general_cs DEFAULT NULL,
  `data_registo` date DEFAULT NULL,
  `marca` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `Pvenda` float DEFAULT NULL,
  `Pcompra` float DEFAULT NULL,
  `quantidade` int(10) UNSIGNED DEFAULT NULL,
  `stkmax` int(10) UNSIGNED DEFAULT NULL,
  `stkmin` int(10) UNSIGNED DEFAULT NULL,
  `ipc` int(11) NOT NULL,
  `obs` varchar(1000) COLLATE latin1_general_cs DEFAULT NULL,
  `Publicado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idProduto`, `familia_idfamilia`, `unidades_idunidades`, `Desigacao`, `cod_Barras`, `data_registo`, `marca`, `Pvenda`, `Pcompra`, `quantidade`, `stkmax`, `stkmin`, `ipc`, `obs`, `Publicado`) VALUES
(45, 18, 2, '', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, 'Testes ', 1),
(44, 19, 2, 'testes2', '434343434', '2020-06-17', 'cxcxcxcxc', 200, 67676, 95, 6767, 50, 2, 'TESTES', 1),
(42, 19, 2, 'Quadro do Pepetela', '7777777777777', '2020-06-17', 'Pepetela', 200, 67676, 1, 6767, 50, 1, '', 1),
(43, 5, 2, 'Testes', '7777777777777', '2020-06-17', 'Pepetela', 200, 67676, 1, 6767, 50, 5, 'TESTES', 1),
(33, 4, 2, 'Bloco de tomadas', '4234234234', '2018-07-27', 'Wintech', 300, 120, 10, 10, 0, 5, '', 1),
(29, 4, 2, 'Cabo HDMI 3M', '12332323', '2018-07-26', 'Mahatam', 12300, 100, 38, 100, 1, 1, '', 1),
(30, 4, 2, 'Cadeira Gammer Vermelha', '4234234234', '2018-07-26', 'Razor', 12000, 100, 5, 100, 10, 5, '', 1),
(34, 18, 2, 'Cabo VGA 4M', '53453453', '2018-07-27', 'Xiaomi', 33, 10, 117, 100, 0, 1, '', 1),
(46, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, 'Testes ', 1),
(47, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, 'Testes										', 0),
(48, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '				Testes						', 0),
(49, 5, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(50, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(51, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(52, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(53, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(54, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(55, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(56, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(57, 18, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '										Tesrse', 0),
(58, 2, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '		Testes								', 0),
(59, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(60, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(61, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(62, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(63, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(64, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(65, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(66, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(67, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(68, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(69, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(70, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(71, 6, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(72, 19, 2, 'Estatua de Buda', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '				Testes						', 0),
(73, 19, 2, 'Estatua do pensador', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Tesetes									', 0),
(74, 18, 2, 'Teste Fornecedor', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '		Testes								', 0),
(75, 5, 2, 'Caçadeira Lendaria', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '		Testes								', 0),
(76, 6, 2, 'Teste Fornecedor', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '		Testes								', 0),
(77, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(78, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(79, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(80, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(81, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(82, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(83, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(84, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(85, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(86, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(87, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(88, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(89, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(90, 19, 2, 'Vasos de Cristais ', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(91, 19, 2, 'Samanhonga', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '		Testes								', 0),
(92, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(93, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(94, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(95, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(96, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(97, 19, 2, 'Comboio em Miniatura', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0),
(98, 2, 2, 'Estatua do pensador', NULL, '0000-00-00', NULL, NULL, NULL, 1, NULL, NULL, 0, '	Testes									', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto_imagens`
--

CREATE TABLE `produto_imagens` (
  `produto` int(10) NOT NULL,
  `imagem` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `descricao` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `thumb` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `produto_imagens`
--

INSERT INTO `produto_imagens` (`produto`, `imagem`, `descricao`, `thumb`) VALUES
(98, 'product_image/16005267713.png', 'Captura de Ecrã (84)', 0),
(98, 'product_image/16005267712.png', 'Captura de Ecrã (81)', 0),
(98, 'product_image/16005267711.png', 'Captura de Ecrã (78)', 0),
(98, 'product_image/16005267710.png', 'Captura de Ecrã (75)', 1),
(97, 'product_image/16005029662.PNG', '5', 0),
(97, 'product_image/16005029660.png', '3', 1),
(97, 'product_image/16005029661.PNG', '4', 0),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (61)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (64)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (67)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (70)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (74)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (77)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (80)', NULL),
(75, 'product_image/1600113332.png', 'Captura de Ecrã (83)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (69)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (73)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (76)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (79)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (82)', NULL),
(76, 'product_image/1600113545.png', 'Captura de Ecrã (85)', NULL),
(90, 'product_image/16001142550.png', 'Captura de Ecrã (67)', NULL),
(90, 'product_image/16001142551.png', 'Captura de Ecrã (70)', NULL),
(90, 'product_image/16001142552.png', 'Captura de Ecrã (74)', NULL),
(90, 'product_image/16001142553.png', 'Captura de Ecrã (77)', NULL),
(90, 'product_image/16001142554.png', 'Captura de Ecrã (80)', NULL),
(90, 'product_image/16001142555.png', 'Captura de Ecrã (83)', NULL),
(91, 'product_image/16005026430.png', '3', 0),
(91, 'product_image/16005026431.PNG', '4', 0),
(91, 'product_image/16005026432.PNG', '5', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `setings`
--

CREATE TABLE `setings` (
  `arrPreco` int(11) NOT NULL,
  `TipoArredondamento` int(11) NOT NULL,
  `layoutFactura` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacaocaixa`
--

CREATE TABLE `transacaocaixa` (
  `idtransacaoCaixa` int(10) UNSIGNED NOT NULL,
  `Caixa_idCaixa` int(10) UNSIGNED NOT NULL,
  `dataTransacao` datetime DEFAULT current_timestamp(),
  `valor` double NOT NULL,
  `tipo` varchar(20) COLLATE latin1_general_cs DEFAULT NULL,
  `obs` varchar(600) COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `transacaocaixa`
--

INSERT INTO `transacaocaixa` (`idtransacaoCaixa`, `Caixa_idCaixa`, `dataTransacao`, `valor`, `tipo`, `obs`) VALUES
(18, 24, '2018-07-30 00:00:00', 500, 'A', 'Abertura de caixa'),
(19, 25, '2018-07-31 08:45:24', 6300, 'A', 'Abertura de caixa'),
(20, 25, '2018-07-31 08:56:50', 654, 'E', 'Venda'),
(21, 25, '2018-07-31 08:58:39', 654, 'E', 'Venda'),
(22, 25, '2018-07-31 08:58:56', 654, 'E', 'Venda'),
(23, 25, '2018-07-31 09:10:33', 654, 'E', 'Venda'),
(24, 25, '2018-07-31 09:11:06', 654, 'E', 'Venda'),
(25, 25, '2018-07-31 09:13:16', 654, 'E', 'Venda'),
(26, 25, '2018-07-31 09:14:17', 654, 'E', 'Venda'),
(27, 25, '2018-07-31 09:15:28', 1716, 'E', 'Venda'),
(28, 25, '2018-07-31 09:16:21', 130.8, 'E', 'Venda'),
(29, 25, '2018-07-31 09:54:11', 1716, 'E', 'Venda'),
(30, 25, '2018-08-01 07:45:25', 1716, 'E', 'Venda'),
(31, 25, '2018-08-01 08:43:56', 3000, 'S', 'pagar o transporte'),
(32, 25, '2018-08-01 12:22:54', 50, 'E', 'Pagar as contas'),
(33, 26, '2018-08-01 13:32:43', 6000, 'A', 'Abertura de caixa'),
(34, 26, '2018-08-01 13:55:55', 300, 'E', 'Recebimento de Clientes'),
(35, 26, '2018-08-01 14:00:54', 150, 'S', 'comprar pÃ£o'),
(36, 27, '2018-08-01 14:10:27', 0, 'A', 'Abertura de caixa'),
(37, 27, '2018-08-01 14:11:52', 132120.6, 'E', 'Venda'),
(38, 27, '2018-08-01 14:17:01', 57200, 'E', 'Venda'),
(39, 27, '2018-08-01 14:17:24', 57200, 'E', 'Venda'),
(40, 27, '2018-08-01 14:29:33', 57200, 'E', 'Venda'),
(41, 27, '2018-08-01 14:31:20', 675.8, 'E', 'Venda'),
(42, 27, '2018-08-01 14:33:09', 6125.8, 'E', 'Venda'),
(43, 27, '2018-08-01 14:36:40', 16848, 'E', 'Venda'),
(44, 27, '2018-08-01 14:38:50', 16848, 'E', 'Venda'),
(45, 27, '2018-08-01 14:46:07', 1716, 'E', 'Venda'),
(46, 27, '2018-08-01 14:47:53', 0, 'E', 'Venda'),
(47, 27, '2018-08-01 14:59:57', 1716, 'E', 'Venda'),
(48, 27, '2018-08-01 15:07:14', 1716, 'E', 'Venda'),
(49, 27, '2018-08-01 15:13:24', 0, 'E', 'Venda'),
(50, 27, '2018-08-01 15:14:15', 1716, 'E', 'Venda'),
(51, 27, '2018-08-01 15:53:49', 21840, 'E', 'Venda'),
(52, 27, '2018-08-01 15:57:04', 13080, 'E', 'Venda'),
(53, 27, '2018-08-01 15:58:02', 13080, 'E', 'Venda'),
(54, 27, '2018-08-01 15:58:40', 1716, 'E', 'Venda'),
(55, 27, '2018-08-01 15:59:17', 1716, 'E', 'Venda'),
(56, 27, '2018-08-01 16:00:16', 250.7, 'E', 'Venda'),
(57, 27, '2018-08-01 16:02:35', 250.7, 'E', 'Venda'),
(58, 27, '2018-08-01 16:38:54', 250.7, 'E', 'Venda'),
(59, 27, '2018-08-01 16:39:22', 1716, 'E', 'Venda'),
(60, 27, '2018-08-01 16:39:36', 0, 'E', 'Venda'),
(61, 27, '2018-08-01 16:45:42', 1716, 'E', 'Venda'),
(62, 27, '2018-08-01 16:48:13', 1716, 'E', 'Venda'),
(63, 27, '2018-08-01 16:50:13', 1716, 'E', 'Venda'),
(64, 27, '2018-08-01 16:51:01', 4054.8, 'E', 'Venda'),
(65, 27, '2018-08-01 17:00:36', 14796, 'E', 'Venda'),
(66, 27, '2018-08-01 17:03:57', 55378.7, 'E', 'Venda'),
(67, 27, '2018-08-01 17:05:46', 323040, 'E', 'Venda'),
(68, 27, '2018-08-06 16:06:52', 1856400, 'E', 'Venda'),
(69, 27, '2018-08-06 16:59:49', 89000, 'S', ''),
(70, 27, '2018-08-06 17:01:32', 10000, 'E', 'testes'),
(71, 28, '2018-08-07 10:48:11', 0, 'A', 'Abertura de caixa'),
(72, 28, '2018-08-07 10:52:37', 0, 'E', 'Venda'),
(73, 28, '2018-08-07 10:52:52', 0, 'E', 'Venda'),
(74, 28, '2018-08-07 10:55:04', 0, 'E', 'Venda'),
(75, 28, '2018-09-17 13:20:36', 67548, 'E', 'Venda'),
(76, 28, '2018-09-17 15:52:23', 72492, 'E', 'Venda'),
(77, 28, '2018-09-17 15:53:01', 1716, 'E', 'Venda'),
(78, 28, '2018-09-17 16:08:21', 16848, 'E', 'Venda'),
(79, 28, '2018-09-17 16:11:06', 16848, 'E', 'Venda'),
(80, 28, '2018-09-17 16:12:05', 16848, 'E', 'Venda'),
(81, 28, '2019-05-01 14:22:40', 624, 'E', 'Venda'),
(82, 29, '2019-06-24 20:14:37', 0, 'A', 'Abertura de caixa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacoes_estoques`
--

CREATE TABLE `transacoes_estoques` (
  `idtransacoes_estoques` int(10) UNSIGNED NOT NULL,
  `Produto_idProduto` int(10) UNSIGNED NOT NULL,
  `Pessoal_idPessoal` int(10) UNSIGNED NOT NULL,
  `data_trans` datetime DEFAULT NULL,
  `tipo` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `clienteFornecedor` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `quantidade` int(10) UNSIGNED DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `nDocumento` varchar(45) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `transacoes_estoques`
--

INSERT INTO `transacoes_estoques` (`idtransacoes_estoques`, `Produto_idProduto`, `Pessoal_idPessoal`, `data_trans`, `tipo`, `clienteFornecedor`, `quantidade`, `valor`, `preco`, `nDocumento`) VALUES
(1, 1, 3, '2018-03-19 20:28:30', 'Venda', '1', 3, 48600, 16200, '317-01'),
(2, 1, 3, '2018-03-19 20:29:24', 'Venda', '1', 3, 48600, 16200, '317-01'),
(3, 1, 3, '2018-03-19 20:30:16', 'Venda', '1', 1, 16200, 16200, '568-54'),
(4, 1, 3, '2018-03-23 05:49:17', 'Venda', '1', 3, 48600, 16200, '173-40'),
(5, 1, 3, '2018-03-23 05:50:47', 'Venda', '1', 3, 48600, 16200, '173-40'),
(6, 1, 3, '2018-03-23 05:50:47', 'Venda', '1', 1, 16200, 16200, '173-40'),
(7, 1, 2, '2018-03-23 20:25:26', 'Venda', '1', 1, 16200, 16200, '673-03'),
(8, 1, 2, '2018-03-23 20:33:21', 'Venda', '1', 1, 16200, 16200, '673-03'),
(9, 1, 2, '2018-03-23 20:33:46', 'Venda', '1', 1, 16200, 16200, '673-03'),
(10, 1, 2, '2018-03-24 17:23:16', 'Venda', '1', 3, 48600, 16200, '490-40'),
(11, 1, 2, '2018-03-24 17:26:38', 'Venda', 'Nao Identificado', 3, 48600, 16200, '490-40'),
(12, 1, 2, '2018-03-24 17:26:38', 'Venda', 'Nao Identificado', 1, 16200, 16200, '490-40'),
(13, 1, 2, '2018-03-24 17:29:03', 'Venda', '1', 3, 48600, 16200, '935-37'),
(14, 1, 2, '2018-03-24 17:31:29', 'Venda', 'Nao Identificado', 3, 48600, 16200, '935-37'),
(15, 1, 2, '2018-03-24 17:31:29', 'Venda', 'Nao Identificado', 1, 16200, 16200, '935-37'),
(16, 4, 2, '2018-03-24 17:33:05', 'Venda', '1', 1, 2700, 2700, '493-29'),
(17, 4, 2, '2018-03-24 17:34:04', 'Venda', '1', 1, 2700, 2700, '358/05'),
(18, 4, 2, '2018-03-24 17:39:31', 'Venda', '1', 1, 2700, 2700, '358/05'),
(19, 1, 2, '2018-03-24 17:40:31', 'Venda', '1', 1, 16200, 16200, '386-58'),
(20, 1, 2, '2018-03-24 17:43:46', 'Venda', '1', 1, 16200, 16200, '868/31'),
(21, 1, 2, '2018-03-24 17:46:14', 'Venda', '1', 1, 16200, 16200, '406/46'),
(22, 1, 2, '2018-03-24 17:51:26', 'Venda', '1', 1, 16200, 16200, '774/14'),
(23, 1, 2, '2018-03-24 17:52:18', 'Venda', '1', 1, 16200, 16200, '774/14'),
(24, 1, 2, '2018-03-24 17:53:05', 'Venda', '1', 1, 16200, 16200, '774/14'),
(25, 1, 2, '2018-03-24 17:53:45', 'Venda', '1', 1, 16200, 16200, '470/05'),
(26, 1, 2, '2018-03-24 17:54:27', 'Venda', '1', 1, 16200, 16200, '470/05'),
(27, 1, 2, '2018-03-24 17:56:27', 'Venda', '1', 1, 16200, 16200, '985/27'),
(28, 1, 2, '2018-03-24 18:05:36', 'Venda', 'Nao Identificado', 1, 16200, 16200, '416/27'),
(29, 1, 2, '2018-03-24 18:10:21', 'Venda', 'Nao Identificado', 1, 16200, 16200, '416/27'),
(30, 1, 2, '2018-03-24 18:14:01', 'Venda', '1', 1, 16200, 16200, '41/21'),
(31, 1, 2, '2018-03-24 18:14:37', 'Venda', '1', 1, 16200, 16200, '41/21'),
(32, 1, 2, '2018-03-24 18:15:00', 'Venda', '1', 1, 16200, 16200, '41/21'),
(33, 1, 2, '2018-03-24 18:15:00', 'Venda', '1', 1, 16200, 16200, '41/21'),
(34, 1, 2, '2018-03-25 23:02:16', 'Compra', '1', 10, 162000, 16200, '990-10'),
(35, 1, 2, '2018-04-04 15:16:35', 'Venda', '1', 3, 48600, 16200, '941/06'),
(36, 4, 2, '2018-04-04 15:16:35', 'Venda', '1', 3, 8100, 2700, '941/06'),
(37, 1, 2, '2018-04-04 15:19:26', 'Venda', '6', 1, 16200, 16200, '671/19'),
(38, 1, 2, '2018-04-04 15:19:26', 'Venda', '6', 1, 16200, 16200, '671/19'),
(39, 4, 2, '2018-04-04 15:19:26', 'Venda', '6', 1, 2700, 2700, '671/19'),
(40, 4, 2, '2018-04-04 15:19:26', 'Venda', '6', 1, 2700, 2700, '671/19'),
(41, 4, 2, '2018-04-04 15:19:26', 'Venda', '6', 1, 2700, 2700, '671/19'),
(42, 1, 2, '2018-04-04 15:30:30', 'Compra', 'Nao Identificado', 10, 162000, 16200, '467-12'),
(43, 1, 2, '2018-04-16 22:24:37', 'Venda', '1', 1, 16200, 16200, '432/38'),
(44, 1, 2, '2018-04-16 22:25:27', 'Venda', '1', 1, 16200, 16200, '432/38'),
(45, 1, 2, '2018-04-16 22:26:21', 'Venda', '1', 1, 16200, 16200, '432/38'),
(46, 1, 2, '2018-04-16 22:27:46', 'Venda', '1', 1, 16200, 16200, '432/38'),
(47, 1, 2, '2018-04-16 22:28:11', 'Venda', '1', 1, 16200, 16200, '432/38'),
(48, 1, 2, '2018-04-16 22:30:31', 'Venda', 'Nao Identificado', 1, 16200, 16200, '432/38'),
(49, 1, 2, '2018-04-16 22:30:31', 'Venda', 'Nao Identificado', 1, 16200, 16200, '432/38'),
(50, 1, 2, '2018-04-16 22:35:18', 'Venda', '6', 1, 16200, 16200, '432/38'),
(51, 1, 2, '2018-04-16 22:35:18', 'Venda', '6', 1, 16200, 16200, '432/38'),
(52, 1, 2, '2018-04-16 22:35:18', 'Venda', '6', 1, 16200, 16200, '432/38'),
(53, 1, 2, '2018-04-19 19:38:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(54, 1, 2, '2018-04-19 19:38:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(55, 1, 2, '2018-04-19 19:38:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(56, 1, 2, '2018-04-19 19:40:13', 'Venda', '1', 1, 16200, 16200, '432/38'),
(57, 1, 2, '2018-04-19 19:40:13', 'Venda', '1', 1, 16200, 16200, '432/38'),
(58, 1, 2, '2018-04-19 19:40:13', 'Venda', '1', 1, 16200, 16200, '432/38'),
(59, 1, 2, '2018-04-19 19:40:13', 'Venda', '1', 1, 16200, 16200, '432/38'),
(60, 1, 2, '2018-04-19 19:43:55', 'Venda', '1', 1, 16200, 16200, '432/38'),
(61, 1, 2, '2018-04-19 19:43:55', 'Venda', '1', 1, 16200, 16200, '432/38'),
(62, 1, 2, '2018-04-19 19:43:55', 'Venda', '1', 1, 16200, 16200, '432/38'),
(63, 1, 2, '2018-04-19 19:43:55', 'Venda', '1', 1, 16200, 16200, '432/38'),
(64, 1, 2, '2018-04-19 19:46:27', 'Venda', '6', 1, 16200, 16200, '432/38'),
(65, 1, 2, '2018-04-19 19:46:27', 'Venda', '6', 1, 16200, 16200, '432/38'),
(66, 1, 2, '2018-04-19 19:46:27', 'Venda', '6', 1, 16200, 16200, '432/38'),
(67, 1, 2, '2018-04-19 19:46:27', 'Venda', '6', 1, 16200, 16200, '432/38'),
(68, 1, 2, '2018-04-19 19:46:27', 'Venda', '6', 1, 16200, 16200, '432/38'),
(69, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(70, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(71, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(72, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(73, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(74, 1, 2, '2018-04-19 19:47:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(75, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(76, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(77, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(78, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(79, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(80, 1, 2, '2018-04-19 19:47:45', 'Venda', '6', 1, 16200, 16200, '432/38'),
(81, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(82, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(83, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(84, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(85, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(86, 1, 2, '2018-04-19 19:48:15', 'Venda', '6', 1, 16200, 16200, '432/38'),
(87, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(88, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(89, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(90, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(91, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(92, 1, 2, '2018-04-19 19:48:49', 'Venda', '6', 1, 16200, 16200, '432/38'),
(93, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(94, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(95, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(96, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(97, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(98, 1, 2, '2018-04-19 19:49:22', 'Venda', '6', 1, 16200, 16200, '432/38'),
(99, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(100, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(101, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(102, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(103, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(104, 1, 2, '2018-04-19 19:50:03', 'Venda', '6', 1, 16200, 16200, '432/38'),
(105, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(106, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(107, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(108, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(109, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(110, 1, 2, '2018-04-19 19:50:42', 'Venda', '6', 1, 16200, 16200, '432/38'),
(111, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(112, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(113, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(114, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(115, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(116, 1, 2, '2018-04-19 19:52:43', 'Venda', '6', 1, 16200, 16200, '432/38'),
(117, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(118, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(119, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(120, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(121, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(122, 1, 2, '2018-04-19 19:54:52', 'Venda', '6', 1, 16200, 16200, '432/38'),
(123, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(124, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(125, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(126, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(127, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(128, 1, 2, '2018-04-19 19:56:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(129, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(130, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(131, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(132, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(133, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(134, 1, 2, '2018-04-19 19:57:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(135, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(136, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(137, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(138, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(139, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(140, 1, 2, '2018-04-19 19:57:50', 'Venda', '6', 1, 16200, 16200, '432/38'),
(141, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(142, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(143, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(144, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(145, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(146, 1, 2, '2018-04-19 19:58:07', 'Venda', '6', 1, 16200, 16200, '432/38'),
(147, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(148, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(149, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(150, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(151, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(152, 1, 2, '2018-04-19 19:59:29', 'Venda', '6', 1, 16200, 16200, '432/38'),
(153, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(154, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(155, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(156, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(157, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(158, 1, 2, '2018-04-19 20:00:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(159, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(160, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(161, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(162, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(163, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(164, 1, 2, '2018-04-19 20:00:55', 'Venda', '6', 1, 16200, 16200, '432/38'),
(165, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(166, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(167, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(168, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(169, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(170, 1, 2, '2018-04-19 20:01:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(171, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(172, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(173, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(174, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(175, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(176, 1, 2, '2018-04-19 20:02:21', 'Venda', '6', 1, 16200, 16200, '432/38'),
(177, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(178, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(179, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(180, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(181, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(182, 1, 2, '2018-04-19 20:02:33', 'Venda', '6', 1, 16200, 16200, '432/38'),
(183, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(184, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(185, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(186, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(187, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(188, 1, 2, '2018-04-19 20:03:35', 'Venda', '6', 1, 16200, 16200, '432/38'),
(189, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(190, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(191, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(192, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(193, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(194, 1, 2, '2018-04-19 20:04:24', 'Venda', '6', 1, 16200, 16200, '432/38'),
(195, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(196, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(197, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(198, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(199, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(200, 1, 2, '2018-04-19 20:05:37', 'Venda', '6', 1, 16200, 16200, '432/38'),
(201, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(202, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(203, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(204, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(205, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(206, 1, 2, '2018-04-19 20:06:58', 'Venda', '6', 1, 16200, 16200, '432/38'),
(207, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(208, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(209, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(210, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(211, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(212, 1, 2, '2018-04-19 20:07:17', 'Venda', '6', 1, 16200, 16200, '432/38'),
(213, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(214, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(215, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(216, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(217, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(218, 1, 2, '2018-04-19 20:07:28', 'Venda', '6', 1, 16200, 16200, '432/38'),
(219, 4, 2, '2018-04-19 20:10:23', 'Venda', '1', 1, 2700, 2700, '700/29'),
(220, 4, 2, '2018-04-19 21:18:56', 'Venda', '1', 1, 2700, 2700, '608/14'),
(221, 4, 2, '2018-04-19 21:19:10', 'Venda', '1', 1, 2700, 2700, '608/14'),
(222, 4, 2, '2018-04-19 21:20:01', 'Venda', '1', 1, 2700, 2700, '608/14'),
(223, 4, 2, '2018-04-19 21:20:27', 'Venda', '1', 1, 2700, 2700, '608/14'),
(224, 4, 2, '2018-04-19 21:20:46', 'Venda', '1', 1, 2700, 2700, '608/14'),
(225, 4, 2, '2018-04-19 21:35:21', 'Venda', '1', 1, 2700, 2700, '608/14'),
(226, 1, 2, '2018-04-19 21:35:21', 'Venda', '1', 10, 162000, 16200, '608/14'),
(227, 4, 2, '2018-04-19 21:36:53', 'Venda', '1', 1, 2700, 2700, '608/14'),
(228, 1, 2, '2018-04-19 21:36:53', 'Venda', '1', 10, 162000, 16200, '608/14'),
(229, 4, 2, '2018-04-19 21:37:26', 'Venda', '1', 1, 2700, 2700, '608/14'),
(230, 1, 2, '2018-04-19 21:37:26', 'Venda', '1', 10, 162000, 16200, '608/14'),
(231, 4, 2, '2018-04-19 21:38:13', 'Venda', '6', 1, 2700, 2700, '608/14'),
(232, 1, 2, '2018-04-19 21:38:13', 'Venda', '6', 10, 162000, 16200, '608/14'),
(233, 1, 2, '2018-04-19 21:38:13', 'Venda', '6', 17, 275400, 16200, '608/14'),
(234, 4, 2, '2018-04-19 21:38:58', 'Venda', '6', 1, 2700, 2700, '608/14'),
(235, 1, 2, '2018-04-19 21:38:58', 'Venda', '6', 10, 162000, 16200, '608/14'),
(236, 1, 2, '2018-04-19 21:38:58', 'Venda', '6', 17, 275400, 16200, '608/14'),
(237, 4, 2, '2018-04-19 21:40:07', 'Venda', '6', 1, 2700, 2700, '608/14'),
(238, 1, 2, '2018-04-19 21:40:07', 'Venda', '6', 10, 162000, 16200, '608/14'),
(239, 1, 2, '2018-04-19 21:40:07', 'Venda', '6', 17, 275400, 16200, '608/14'),
(240, 4, 2, '2018-04-19 21:40:38', 'Venda', 'Nao Identificado', 1, 2700, 2700, '608/14'),
(241, 1, 2, '2018-04-19 21:40:38', 'Venda', 'Nao Identificado', 10, 162000, 16200, '608/14'),
(242, 1, 2, '2018-04-19 21:40:38', 'Venda', 'Nao Identificado', 17, 275400, 16200, '608/14'),
(243, 1, 2, '2018-04-19 21:40:38', 'Venda', 'Nao Identificado', 2, 32400, 16200, '608/14'),
(244, 4, 2, '2018-04-19 21:44:28', 'Venda', '6', 1, 2700, 2700, '608/14'),
(245, 1, 2, '2018-04-19 21:44:28', 'Venda', '6', 10, 162000, 16200, '608/14'),
(246, 1, 2, '2018-04-19 21:44:28', 'Venda', '6', 17, 275400, 16200, '608/14'),
(247, 1, 2, '2018-04-19 21:44:28', 'Venda', '6', 2, 32400, 16200, '608/14'),
(248, 4, 2, '2018-04-19 21:44:28', 'Venda', '6', 10, 27000, 2700, '608/14'),
(249, 4, 2, '2018-04-19 21:44:45', 'Venda', '6', 1, 2700, 2700, '608/14'),
(250, 1, 2, '2018-04-19 21:44:45', 'Venda', '6', 10, 162000, 16200, '608/14'),
(251, 1, 2, '2018-04-19 21:44:45', 'Venda', '6', 17, 275400, 16200, '608/14'),
(252, 1, 2, '2018-04-19 21:44:45', 'Venda', '6', 2, 32400, 16200, '608/14'),
(253, 4, 2, '2018-04-19 21:44:45', 'Venda', '6', 10, 27000, 2700, '608/14'),
(254, 4, 2, '2018-04-19 21:59:36', 'Venda', '6', 1, 2700, 2700, '608/14'),
(255, 1, 2, '2018-04-19 21:59:36', 'Venda', '6', 10, 162000, 16200, '608/14'),
(256, 1, 2, '2018-04-19 21:59:36', 'Venda', '6', 17, 275400, 16200, '608/14'),
(257, 1, 2, '2018-04-19 21:59:36', 'Venda', '6', 2, 32400, 16200, '608/14'),
(258, 4, 2, '2018-04-19 21:59:36', 'Venda', '6', 10, 27000, 2700, '608/14'),
(259, 4, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 1, 2700, 2700, '608/14'),
(260, 1, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 10, 162000, 16200, '608/14'),
(261, 1, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 17, 275400, 16200, '608/14'),
(262, 1, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 2, 32400, 16200, '608/14'),
(263, 4, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 10, 27000, 2700, '608/14'),
(264, 4, 2, '2018-04-19 22:00:05', 'Venda', 'Nao Identificado', 4, 10800, 2700, '608/14'),
(265, 4, 2, '2018-04-19 22:01:14', 'Venda', '6', 1, 2700, 2700, '608/14'),
(266, 1, 2, '2018-04-19 22:01:14', 'Venda', '6', 10, 162000, 16200, '608/14'),
(267, 1, 2, '2018-04-19 22:01:14', 'Venda', '6', 17, 275400, 16200, '608/14'),
(268, 1, 2, '2018-04-19 22:01:14', 'Venda', '6', 2, 32400, 16200, '608/14'),
(269, 4, 2, '2018-04-19 22:01:14', 'Venda', '6', 10, 27000, 2700, '608/14'),
(270, 4, 2, '2018-04-19 22:01:14', 'Venda', '6', 4, 10800, 2700, '608/14'),
(271, 4, 2, '2018-04-19 22:01:14', 'Venda', '6', 9, 24300, 2700, '608/14'),
(272, 1, 2, '2018-04-21 15:10:05', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(273, 1, 2, '2018-04-21 15:11:49', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(274, 1, 2, '2018-04-21 15:12:27', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(275, 1, 2, '2018-04-21 15:13:48', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(276, 1, 2, '2018-04-21 15:14:23', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(277, 1, 2, '2018-04-21 15:14:38', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(278, 1, 2, '2018-04-21 15:15:41', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(279, 1, 2, '2018-04-21 15:16:01', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(280, 1, 2, '2018-04-21 15:16:30', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(281, 1, 2, '2018-04-21 15:16:53', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(282, 1, 2, '2018-04-21 15:17:20', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(283, 1, 2, '2018-04-21 15:17:42', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(284, 1, 2, '2018-04-21 15:17:55', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(285, 1, 2, '2018-04-21 15:18:14', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(286, 1, 2, '2018-04-21 15:18:27', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(287, 1, 2, '2018-04-21 15:18:55', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(288, 1, 2, '2018-04-21 15:19:31', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(289, 1, 2, '2018-04-21 15:20:16', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(290, 1, 2, '2018-04-21 15:20:56', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(291, 1, 2, '2018-04-21 15:21:11', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(292, 1, 2, '2018-04-21 15:21:23', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(293, 1, 2, '2018-04-21 15:23:13', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(294, 1, 2, '2018-04-21 15:23:41', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(295, 1, 2, '2018-04-21 15:23:57', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(296, 1, 2, '2018-04-21 15:25:43', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(297, 1, 2, '2018-04-21 15:25:57', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(298, 1, 2, '2018-04-21 15:26:17', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(299, 1, 2, '2018-04-21 15:26:35', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(300, 1, 2, '2018-04-21 15:26:57', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(301, 1, 2, '2018-04-21 15:27:33', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(302, 1, 2, '2018-04-21 15:28:01', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(303, 1, 2, '2018-04-21 15:28:13', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(304, 1, 2, '2018-04-21 15:31:52', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(305, 1, 2, '2018-04-21 15:33:14', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(306, 1, 2, '2018-04-22 19:01:53', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(307, 1, 2, '2018-04-22 19:02:16', 'Compra', 'Nao Identificado', 1, 16200, 16200, '370-22'),
(308, 1, 2, '2018-04-22 19:03:19', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(309, 1, 2, '2018-04-22 19:04:10', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(310, 1, 2, '2018-04-22 19:04:33', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(311, 1, 2, '2018-04-22 19:05:06', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(312, 1, 2, '2018-04-22 19:06:10', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(313, 1, 2, '2018-04-22 19:13:31', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(314, 1, 2, '2018-04-22 19:24:52', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(315, 1, 2, '2018-04-22 19:25:06', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(316, 1, 2, '2018-04-22 19:26:44', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(317, 1, 2, '2018-04-22 19:29:06', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(318, 1, 2, '2018-04-22 19:34:09', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(319, 1, 2, '2018-04-22 19:36:39', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(320, 4, 2, '2018-04-22 19:36:39', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(321, 1, 2, '2018-04-22 19:36:39', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(322, 4, 2, '2018-04-22 19:36:39', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(323, 1, 2, '2018-04-22 22:26:55', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(324, 4, 2, '2018-04-22 22:26:55', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(325, 1, 2, '2018-04-22 22:26:55', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(326, 4, 2, '2018-04-22 22:26:55', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(327, 1, 2, '2018-04-22 22:27:16', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(328, 4, 2, '2018-04-22 22:27:16', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(329, 1, 2, '2018-04-22 22:27:16', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(330, 4, 2, '2018-04-22 22:27:16', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(331, 1, 2, '2018-04-22 22:27:38', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(332, 4, 2, '2018-04-22 22:27:38', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(333, 1, 2, '2018-04-22 22:27:38', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(334, 4, 2, '2018-04-22 22:27:38', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(335, 1, 2, '2018-04-22 22:28:05', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(336, 4, 2, '2018-04-22 22:28:05', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(337, 1, 2, '2018-04-22 22:28:05', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(338, 4, 2, '2018-04-22 22:28:05', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(339, 1, 2, '2018-04-22 22:28:19', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(340, 4, 2, '2018-04-22 22:28:19', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(341, 1, 2, '2018-04-22 22:28:19', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(342, 4, 2, '2018-04-22 22:28:19', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(343, 1, 2, '2018-04-22 22:28:44', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(344, 4, 2, '2018-04-22 22:28:44', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(345, 1, 2, '2018-04-22 22:28:44', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(346, 4, 2, '2018-04-22 22:28:44', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(347, 1, 2, '2018-04-22 22:30:11', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(348, 4, 2, '2018-04-22 22:30:11', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(349, 1, 2, '2018-04-22 22:30:11', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(350, 4, 2, '2018-04-22 22:30:11', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(351, 1, 2, '2018-04-22 22:30:26', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(352, 4, 2, '2018-04-22 22:30:26', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(353, 1, 2, '2018-04-22 22:30:26', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(354, 4, 2, '2018-04-22 22:30:26', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(355, 1, 2, '2018-04-22 22:30:37', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(356, 4, 2, '2018-04-22 22:30:37', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(357, 1, 2, '2018-04-22 22:30:37', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(358, 4, 2, '2018-04-22 22:30:37', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(359, 1, 2, '2018-04-22 22:30:53', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(360, 4, 2, '2018-04-22 22:30:53', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(361, 1, 2, '2018-04-22 22:30:53', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(362, 4, 2, '2018-04-22 22:30:53', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(363, 1, 2, '2018-04-22 22:31:17', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(364, 4, 2, '2018-04-22 22:31:17', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(365, 1, 2, '2018-04-22 22:31:17', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(366, 4, 2, '2018-04-22 22:31:17', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(367, 1, 2, '2018-04-22 22:31:49', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(368, 4, 2, '2018-04-22 22:31:49', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(369, 1, 2, '2018-04-22 22:31:49', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(370, 4, 2, '2018-04-22 22:31:49', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(371, 1, 2, '2018-04-22 22:32:10', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(372, 4, 2, '2018-04-22 22:32:10', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(373, 1, 2, '2018-04-22 22:32:10', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(374, 4, 2, '2018-04-22 22:32:10', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(375, 1, 2, '2018-04-22 22:33:28', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(376, 4, 2, '2018-04-22 22:33:28', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(377, 1, 2, '2018-04-22 22:33:28', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(378, 4, 2, '2018-04-22 22:33:28', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(379, 1, 2, '2018-04-22 22:33:55', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(380, 4, 2, '2018-04-22 22:33:55', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(381, 1, 2, '2018-04-22 22:33:55', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(382, 4, 2, '2018-04-22 22:33:55', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(383, 1, 2, '2018-04-22 22:34:19', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(384, 4, 2, '2018-04-22 22:34:19', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(385, 1, 2, '2018-04-22 22:34:19', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(386, 4, 2, '2018-04-22 22:34:19', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(387, 1, 2, '2018-04-22 22:35:46', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(388, 4, 2, '2018-04-22 22:35:46', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(389, 1, 2, '2018-04-22 22:35:46', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(390, 4, 2, '2018-04-22 22:35:46', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(391, 1, 2, '2018-04-22 22:36:16', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(392, 4, 2, '2018-04-22 22:36:16', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(393, 1, 2, '2018-04-22 22:36:16', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(394, 4, 2, '2018-04-22 22:36:16', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(395, 1, 2, '2018-04-22 22:36:43', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(396, 4, 2, '2018-04-22 22:36:43', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(397, 1, 2, '2018-04-22 22:36:43', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(398, 4, 2, '2018-04-22 22:36:43', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(399, 1, 2, '2018-04-22 22:37:01', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(400, 4, 2, '2018-04-22 22:37:01', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(401, 1, 2, '2018-04-22 22:37:01', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(402, 4, 2, '2018-04-22 22:37:01', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(403, 1, 2, '2018-04-22 22:37:12', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(404, 4, 2, '2018-04-22 22:37:12', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(405, 1, 2, '2018-04-22 22:37:12', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(406, 4, 2, '2018-04-22 22:37:12', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(407, 1, 2, '2018-04-22 22:37:42', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(408, 4, 2, '2018-04-22 22:37:42', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(409, 1, 2, '2018-04-22 22:37:42', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(410, 4, 2, '2018-04-22 22:37:42', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(411, 1, 2, '2018-04-22 22:38:11', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(412, 4, 2, '2018-04-22 22:38:11', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(413, 1, 2, '2018-04-22 22:38:11', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(414, 4, 2, '2018-04-22 22:38:11', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(415, 1, 2, '2018-04-22 22:38:23', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(416, 4, 2, '2018-04-22 22:38:23', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(417, 1, 2, '2018-04-22 22:38:23', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(418, 4, 2, '2018-04-22 22:38:23', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(419, 1, 2, '2018-04-22 22:38:39', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(420, 4, 2, '2018-04-22 22:38:39', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(421, 1, 2, '2018-04-22 22:38:39', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(422, 4, 2, '2018-04-22 22:38:39', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(423, 1, 2, '2018-04-22 22:38:55', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(424, 4, 2, '2018-04-22 22:38:55', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(425, 1, 2, '2018-04-22 22:38:55', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(426, 4, 2, '2018-04-22 22:38:55', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(427, 1, 2, '2018-04-22 22:41:12', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(428, 4, 2, '2018-04-22 22:41:12', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(429, 1, 2, '2018-04-22 22:41:12', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(430, 4, 2, '2018-04-22 22:41:12', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(431, 1, 2, '2018-04-22 22:45:39', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(432, 4, 2, '2018-04-22 22:45:39', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(433, 1, 2, '2018-04-22 22:45:39', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(434, 4, 2, '2018-04-22 22:45:39', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(435, 1, 2, '2018-04-22 22:46:04', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(436, 4, 2, '2018-04-22 22:46:04', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(437, 1, 2, '2018-04-22 22:46:04', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(438, 4, 2, '2018-04-22 22:46:04', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(439, 1, 2, '2018-04-22 22:48:19', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(440, 4, 2, '2018-04-22 22:48:19', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(441, 1, 2, '2018-04-22 22:48:19', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(442, 4, 2, '2018-04-22 22:48:19', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(443, 1, 2, '2018-04-22 22:48:50', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(444, 4, 2, '2018-04-22 22:48:50', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(445, 1, 2, '2018-04-22 22:48:50', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(446, 4, 2, '2018-04-22 22:48:50', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(447, 1, 2, '2018-04-22 22:49:20', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(448, 4, 2, '2018-04-22 22:49:20', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(449, 1, 2, '2018-04-22 22:49:20', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(450, 4, 2, '2018-04-22 22:49:20', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(451, 1, 2, '2018-04-22 22:51:31', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(452, 4, 2, '2018-04-22 22:51:31', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(453, 1, 2, '2018-04-22 22:51:31', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(454, 4, 2, '2018-04-22 22:51:31', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(455, 1, 2, '2018-04-22 22:52:04', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(456, 4, 2, '2018-04-22 22:52:04', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(457, 1, 2, '2018-04-22 22:52:04', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(458, 4, 2, '2018-04-22 22:52:04', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(459, 1, 2, '2018-04-22 22:53:21', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(460, 4, 2, '2018-04-22 22:53:21', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(461, 1, 2, '2018-04-22 22:53:21', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(462, 4, 2, '2018-04-22 22:53:21', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(463, 1, 2, '2018-04-22 22:53:51', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(464, 4, 2, '2018-04-22 22:53:51', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(465, 1, 2, '2018-04-22 22:53:51', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(466, 4, 2, '2018-04-22 22:53:51', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(467, 1, 2, '2018-04-22 22:54:14', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(468, 4, 2, '2018-04-22 22:54:14', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(469, 1, 2, '2018-04-22 22:54:14', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(470, 4, 2, '2018-04-22 22:54:14', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(471, 1, 2, '2018-04-22 22:54:33', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(472, 4, 2, '2018-04-22 22:54:33', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(473, 1, 2, '2018-04-22 22:54:33', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(474, 4, 2, '2018-04-22 22:54:33', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(475, 1, 2, '2018-04-22 22:55:03', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(476, 4, 2, '2018-04-22 22:55:03', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(477, 1, 2, '2018-04-22 22:55:03', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(478, 4, 2, '2018-04-22 22:55:03', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(479, 1, 2, '2018-04-22 22:56:18', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(480, 4, 2, '2018-04-22 22:56:18', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(481, 1, 2, '2018-04-22 22:56:18', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(482, 4, 2, '2018-04-22 22:56:18', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(483, 1, 2, '2018-04-22 22:56:45', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(484, 4, 2, '2018-04-22 22:56:45', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(485, 1, 2, '2018-04-22 22:56:45', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(486, 4, 2, '2018-04-22 22:56:45', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(487, 1, 2, '2018-04-22 22:57:03', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(488, 4, 2, '2018-04-22 22:57:03', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(489, 1, 2, '2018-04-22 22:57:03', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(490, 4, 2, '2018-04-22 22:57:03', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(491, 1, 2, '2018-04-22 22:57:15', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(492, 4, 2, '2018-04-22 22:57:15', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(493, 1, 2, '2018-04-22 22:57:15', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(494, 4, 2, '2018-04-22 22:57:15', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(495, 1, 2, '2018-04-22 22:57:29', 'Compra', 'Nao Identificado', 1, 16200, 16200, '473-57'),
(496, 4, 2, '2018-04-22 22:57:29', 'Compra', 'Nao Identificado', 1, 2700, 2700, '473-57'),
(497, 1, 2, '2018-04-22 22:57:29', 'Compra', 'Nao Identificado', 10, 162000, 16200, '473-57'),
(498, 4, 2, '2018-04-22 22:57:29', 'Compra', 'Nao Identificado', 6, 16200, 2700, '473-57'),
(499, 1, 2, '2018-04-22 22:58:16', 'Venda', '1', 1, 16200, 16200, '577/50'),
(500, 1, 2, '2018-04-27 21:36:36', 'Venda', '6', 3, 48600, 16200, '453/51'),
(501, 1, 2, '2018-04-27 21:42:15', 'Venda', '1', 3, 48600, 16200, '453/51'),
(502, 1, 2, '2018-04-27 21:42:15', 'Venda', '1', 1, 16200, 16200, '453/51'),
(503, 1, 2, '2018-04-27 21:50:22', 'Venda', '1', 3, 48600, 16200, '453/51'),
(504, 1, 2, '2018-04-27 21:50:22', 'Venda', '1', 1, 16200, 16200, '453/51'),
(505, 1, 2, '2018-04-27 21:50:45', 'Venda', '1', 3, 48600, 16200, '453/51'),
(506, 1, 2, '2018-04-27 21:50:45', 'Venda', '1', 1, 16200, 16200, '453/51'),
(507, 1, 2, '2018-04-27 21:51:31', 'Venda', '1', 3, 48600, 16200, '453/51'),
(508, 1, 2, '2018-04-27 21:51:31', 'Venda', '1', 1, 16200, 16200, '453/51'),
(509, 1, 2, '2018-04-27 21:51:31', 'Venda', '1', 9, 145800, 16200, '453/51'),
(510, 1, 2, '2018-04-27 23:27:47', 'Venda', '1', 3, 48600, 16200, '453/51'),
(511, 1, 2, '2018-04-27 23:27:47', 'Venda', '1', 1, 16200, 16200, '453/51'),
(512, 1, 2, '2018-04-27 23:27:47', 'Venda', '1', 9, 145800, 16200, '453/51'),
(513, 1, 2, '2018-04-29 11:07:22', 'Venda', 'Nao Identificado', 46, 745200, 16200, '447/47'),
(514, 1, 2, '2018-04-29 11:07:42', 'Venda', 'Nao Identificado', 46, 745200, 16200, '447/47'),
(515, 1, 2, '2018-04-29 11:07:46', 'Venda', 'Nao Identificado', 46, 745200, 16200, '447/47'),
(516, 1, 2, '2018-04-29 11:08:41', 'Compra', 'Nao Identificado', 10, 162000, 16200, '882-11'),
(517, 4, 2, '2018-04-29 11:13:19', 'Compra', '2', 100, 270000, 2700, '635-53'),
(518, 4, 2, '2018-04-29 11:13:42', 'Compra', '2', 100, 270000, 2700, '635-53'),
(519, 4, 2, '2018-04-29 11:14:32', 'Compra', '2', 100, 270000, 2700, '635-53'),
(520, 4, 2, '2018-04-29 11:18:12', 'Compra', '2', 100, 270000, 2700, '635-53'),
(521, 4, 2, '2018-04-29 11:18:53', 'Compra', '2', 100, 270000, 2700, '635-53'),
(522, 4, 2, '2018-04-29 11:20:04', 'Compra', '2', 100, 270000, 2700, '635-53'),
(523, 4, 2, '2018-04-29 11:20:42', 'Compra', '2', 100, 270000, 2700, '635-53'),
(524, 4, 2, '2018-04-29 11:22:51', 'Compra', '2', 100, 270000, 2700, '635-53'),
(525, 4, 2, '2018-04-29 11:23:56', 'Compra', '2', 100, 270000, 2700, '635-53'),
(526, 4, 2, '2018-04-29 12:19:06', 'Compra', '2', 100, 270000, 2700, '635-53'),
(527, 1, 2, '2018-04-29 12:20:12', 'Compra', '2', 1, 16200, 16200, '71/06'),
(528, 4, 2, '2018-04-29 12:20:12', 'Compra', '2', 1, 2700, 2700, '71/06'),
(529, 4, 2, '2018-04-29 12:20:12', 'Compra', '2', 1, 2700, 2700, '71/06'),
(530, 4, 2, '2018-04-29 12:20:53', 'Compra', '2', 1, 2700, 2700, '589/12'),
(531, 4, 2, '2018-04-29 12:22:12', 'Compra', '2', 1, 2700, 2700, '589/12'),
(532, 4, 2, '2018-04-29 12:37:47', 'Compra', '2', 1, 2700, 2700, '589/12'),
(533, 4, 2, '2018-04-29 12:38:01', 'Compra', '2', 1, 2700, 2700, '589/12'),
(534, 4, 2, '2018-04-29 12:38:38', 'Compra', '2', 1, 2700, 2700, '589/18'),
(535, 4, 2, '2018-04-29 12:38:55', 'Compra', '2', 1, 2700, 2700, '589/18'),
(536, 4, 2, '2018-04-29 12:39:13', 'Compra', '2', 1, 2700, 2700, '589/18'),
(537, 4, 2, '2018-04-29 12:39:26', 'Compra', '2', 1, 2700, 2700, '589/18'),
(538, 4, 2, '2018-04-29 12:39:37', 'Compra', '2', 1, 2700, 2700, '589/18'),
(539, 4, 2, '2018-04-29 12:39:57', 'Compra', '2', 1, 2700, 2700, '589/18'),
(540, 4, 2, '2018-04-29 12:40:41', 'Compra', '2', 1, 2700, 2700, '345-57'),
(541, 1, 2, '2018-04-29 12:40:41', 'Compra', '2', 1, 16200, 16200, '345-57'),
(542, 4, 2, '2018-04-29 12:42:09', 'Compra', '2', 1, 2700, 2700, '494/41'),
(543, 4, 2, '2018-04-29 12:42:09', 'Compra', '2', 1, 2700, 2700, '494/41'),
(544, 4, 2, '2018-04-29 12:43:46', 'Compra', '2', 1, 2700, 2700, '494/41'),
(545, 4, 2, '2018-04-29 12:43:46', 'Compra', '2', 1, 2700, 2700, '494/41'),
(546, 4, 2, '2018-04-29 12:44:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(547, 4, 2, '2018-04-29 12:44:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(548, 4, 2, '2018-04-29 13:11:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(549, 4, 2, '2018-04-29 13:11:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(550, 4, 2, '2018-04-29 13:12:02', 'Compra', '2', 1, 2700, 2700, '494/41'),
(551, 4, 2, '2018-04-29 13:12:02', 'Compra', '2', 1, 2700, 2700, '494/41'),
(552, 4, 2, '2018-04-29 13:12:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(553, 4, 2, '2018-04-29 13:12:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(554, 4, 2, '2018-04-29 13:14:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(555, 4, 2, '2018-04-29 13:14:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(556, 4, 2, '2018-04-29 13:15:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(557, 4, 2, '2018-04-29 13:15:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(558, 4, 2, '2018-04-29 13:16:02', 'Compra', '2', 1, 2700, 2700, '494/41'),
(559, 4, 2, '2018-04-29 13:16:02', 'Compra', '2', 1, 2700, 2700, '494/41'),
(560, 4, 2, '2018-04-29 13:28:10', 'Compra', '2', 1, 2700, 2700, '494/41'),
(561, 4, 2, '2018-04-29 13:28:10', 'Compra', '2', 1, 2700, 2700, '494/41'),
(562, 4, 2, '2018-04-29 13:29:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(563, 4, 2, '2018-04-29 13:29:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(564, 4, 2, '2018-04-29 13:29:57', 'Compra', '2', 1, 2700, 2700, '494/41'),
(565, 4, 2, '2018-04-29 13:29:57', 'Compra', '2', 1, 2700, 2700, '494/41'),
(566, 4, 2, '2018-04-29 13:30:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(567, 4, 2, '2018-04-29 13:30:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(568, 4, 2, '2018-04-29 13:32:01', 'Compra', '2', 1, 2700, 2700, '494/41'),
(569, 4, 2, '2018-04-29 13:32:01', 'Compra', '2', 1, 2700, 2700, '494/41'),
(570, 4, 2, '2018-04-29 13:37:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(571, 4, 2, '2018-04-29 13:37:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(572, 4, 2, '2018-04-29 13:38:45', 'Compra', '2', 1, 2700, 2700, '494/41'),
(573, 4, 2, '2018-04-29 13:38:45', 'Compra', '2', 1, 2700, 2700, '494/41'),
(574, 4, 2, '2018-04-29 13:39:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(575, 4, 2, '2018-04-29 13:39:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(576, 4, 2, '2018-04-29 13:39:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(577, 4, 2, '2018-04-29 13:39:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(578, 4, 2, '2018-04-29 13:41:08', 'Compra', '2', 1, 2700, 2700, '494/41'),
(579, 4, 2, '2018-04-29 13:41:08', 'Compra', '2', 1, 2700, 2700, '494/41'),
(580, 4, 2, '2018-04-29 13:44:33', 'Compra', '2', 1, 2700, 2700, '494/41'),
(581, 4, 2, '2018-04-29 13:44:33', 'Compra', '2', 1, 2700, 2700, '494/41'),
(582, 4, 2, '2018-04-29 13:49:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(583, 4, 2, '2018-04-29 13:49:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(584, 4, 2, '2018-04-29 13:50:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(585, 4, 2, '2018-04-29 13:50:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(586, 4, 2, '2018-04-29 13:50:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(587, 4, 2, '2018-04-29 13:50:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(588, 4, 2, '2018-04-29 13:50:42', 'Compra', '2', 1, 2700, 2700, '494/41'),
(589, 4, 2, '2018-04-29 13:50:42', 'Compra', '2', 1, 2700, 2700, '494/41'),
(590, 4, 2, '2018-04-29 13:51:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(591, 4, 2, '2018-04-29 13:51:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(592, 4, 2, '2018-04-29 13:51:18', 'Compra', '2', 1, 2700, 2700, '494/41'),
(593, 4, 2, '2018-04-29 13:51:18', 'Compra', '2', 1, 2700, 2700, '494/41'),
(594, 4, 2, '2018-04-29 13:51:44', 'Compra', '2', 1, 2700, 2700, '494/41'),
(595, 4, 2, '2018-04-29 13:51:44', 'Compra', '2', 1, 2700, 2700, '494/41'),
(596, 4, 2, '2018-04-29 13:52:09', 'Compra', '2', 1, 2700, 2700, '494/41'),
(597, 4, 2, '2018-04-29 13:52:09', 'Compra', '2', 1, 2700, 2700, '494/41'),
(598, 4, 2, '2018-04-29 13:52:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(599, 4, 2, '2018-04-29 13:52:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(600, 4, 2, '2018-04-29 13:53:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(601, 4, 2, '2018-04-29 13:53:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(602, 4, 2, '2018-04-29 13:53:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(603, 4, 2, '2018-04-29 13:53:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(604, 4, 2, '2018-04-29 13:54:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(605, 4, 2, '2018-04-29 13:54:47', 'Compra', '2', 1, 2700, 2700, '494/41'),
(606, 4, 2, '2018-04-29 13:55:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(607, 4, 2, '2018-04-29 13:55:03', 'Compra', '2', 1, 2700, 2700, '494/41'),
(608, 4, 2, '2018-04-29 13:55:20', 'Compra', '2', 1, 2700, 2700, '494/41'),
(609, 4, 2, '2018-04-29 13:55:20', 'Compra', '2', 1, 2700, 2700, '494/41'),
(610, 4, 2, '2018-04-29 13:55:38', 'Compra', '2', 1, 2700, 2700, '494/41'),
(611, 4, 2, '2018-04-29 13:55:38', 'Compra', '2', 1, 2700, 2700, '494/41'),
(612, 4, 2, '2018-04-29 13:55:59', 'Compra', '2', 1, 2700, 2700, '494/41'),
(613, 4, 2, '2018-04-29 13:55:59', 'Compra', '2', 1, 2700, 2700, '494/41');
INSERT INTO `transacoes_estoques` (`idtransacoes_estoques`, `Produto_idProduto`, `Pessoal_idPessoal`, `data_trans`, `tipo`, `clienteFornecedor`, `quantidade`, `valor`, `preco`, `nDocumento`) VALUES
(614, 4, 2, '2018-04-29 13:58:56', 'Compra', '2', 1, 2700, 2700, '494/41'),
(615, 4, 2, '2018-04-29 13:58:56', 'Compra', '2', 1, 2700, 2700, '494/41'),
(616, 4, 2, '2018-04-29 13:59:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(617, 4, 2, '2018-04-29 13:59:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(618, 4, 2, '2018-04-29 13:59:33', 'Compra', '2', 1, 2700, 2700, '494/41'),
(619, 4, 2, '2018-04-29 13:59:33', 'Compra', '2', 1, 2700, 2700, '494/41'),
(620, 4, 2, '2018-04-29 13:59:53', 'Compra', '2', 1, 2700, 2700, '494/41'),
(621, 4, 2, '2018-04-29 13:59:53', 'Compra', '2', 1, 2700, 2700, '494/41'),
(622, 4, 2, '2018-04-29 14:00:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(623, 4, 2, '2018-04-29 14:00:15', 'Compra', '2', 1, 2700, 2700, '494/41'),
(624, 4, 2, '2018-04-29 14:00:34', 'Compra', '2', 1, 2700, 2700, '494/41'),
(625, 4, 2, '2018-04-29 14:00:34', 'Compra', '2', 1, 2700, 2700, '494/41'),
(626, 4, 2, '2018-04-29 14:00:48', 'Compra', '2', 1, 2700, 2700, '494/41'),
(627, 4, 2, '2018-04-29 14:00:48', 'Compra', '2', 1, 2700, 2700, '494/41'),
(628, 4, 2, '2018-04-29 14:01:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(629, 4, 2, '2018-04-29 14:01:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(630, 4, 2, '2018-04-29 14:01:27', 'Compra', '2', 1, 2700, 2700, '494/41'),
(631, 4, 2, '2018-04-29 14:01:27', 'Compra', '2', 1, 2700, 2700, '494/41'),
(632, 4, 2, '2018-04-29 14:01:54', 'Compra', '2', 1, 2700, 2700, '494/41'),
(633, 4, 2, '2018-04-29 14:01:54', 'Compra', '2', 1, 2700, 2700, '494/41'),
(634, 4, 2, '2018-04-29 14:04:12', 'Compra', '2', 1, 2700, 2700, '494/41'),
(635, 4, 2, '2018-04-29 14:04:12', 'Compra', '2', 1, 2700, 2700, '494/41'),
(636, 4, 2, '2018-04-29 14:04:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(637, 4, 2, '2018-04-29 14:04:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(638, 4, 2, '2018-04-29 14:04:41', 'Compra', '2', 1, 2700, 2700, '494/41'),
(639, 4, 2, '2018-04-29 14:04:41', 'Compra', '2', 1, 2700, 2700, '494/41'),
(640, 4, 2, '2018-04-29 14:04:53', 'Compra', '2', 1, 2700, 2700, '494/41'),
(641, 4, 2, '2018-04-29 14:04:53', 'Compra', '2', 1, 2700, 2700, '494/41'),
(642, 4, 2, '2018-04-29 14:05:07', 'Compra', '2', 1, 2700, 2700, '494/41'),
(643, 4, 2, '2018-04-29 14:05:07', 'Compra', '2', 1, 2700, 2700, '494/41'),
(644, 4, 2, '2018-04-29 14:05:22', 'Compra', '2', 1, 2700, 2700, '494/41'),
(645, 4, 2, '2018-04-29 14:05:22', 'Compra', '2', 1, 2700, 2700, '494/41'),
(646, 4, 2, '2018-04-29 14:05:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(647, 4, 2, '2018-04-29 14:05:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(648, 4, 2, '2018-04-29 14:05:50', 'Compra', '2', 1, 2700, 2700, '494/41'),
(649, 4, 2, '2018-04-29 14:05:50', 'Compra', '2', 1, 2700, 2700, '494/41'),
(650, 4, 2, '2018-04-29 14:06:04', 'Compra', '2', 1, 2700, 2700, '494/41'),
(651, 4, 2, '2018-04-29 14:06:04', 'Compra', '2', 1, 2700, 2700, '494/41'),
(652, 4, 2, '2018-04-29 14:06:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(653, 4, 2, '2018-04-29 14:06:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(654, 4, 2, '2018-04-29 14:06:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(655, 4, 2, '2018-04-29 14:06:36', 'Compra', '2', 1, 2700, 2700, '494/41'),
(656, 4, 2, '2018-04-29 14:09:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(657, 4, 2, '2018-04-29 14:09:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(658, 4, 2, '2018-04-29 14:09:37', 'Compra', '2', 1, 2700, 2700, '494/41'),
(659, 4, 2, '2018-04-29 14:09:37', 'Compra', '2', 1, 2700, 2700, '494/41'),
(660, 4, 2, '2018-04-29 14:09:50', 'Compra', '2', 1, 2700, 2700, '494/41'),
(661, 4, 2, '2018-04-29 14:09:50', 'Compra', '2', 1, 2700, 2700, '494/41'),
(662, 4, 2, '2018-04-29 14:10:04', 'Compra', '2', 1, 2700, 2700, '494/41'),
(663, 4, 2, '2018-04-29 14:10:04', 'Compra', '2', 1, 2700, 2700, '494/41'),
(664, 4, 2, '2018-04-29 14:10:23', 'Compra', '2', 1, 2700, 2700, '494/41'),
(665, 4, 2, '2018-04-29 14:10:23', 'Compra', '2', 1, 2700, 2700, '494/41'),
(666, 4, 2, '2018-04-29 14:11:13', 'Compra', '2', 1, 2700, 2700, '494/41'),
(667, 4, 2, '2018-04-29 14:11:13', 'Compra', '2', 1, 2700, 2700, '494/41'),
(668, 4, 2, '2018-04-30 21:14:30', 'Compra', '2', 1, 2700, 2700, '494/41'),
(669, 4, 2, '2018-04-30 21:14:30', 'Compra', '2', 1, 2700, 2700, '494/41'),
(670, 4, 2, '2018-04-30 21:15:07', 'Compra', '2', 1, 2700, 2700, '494/41'),
(671, 4, 2, '2018-04-30 21:15:07', 'Compra', '2', 1, 2700, 2700, '494/41'),
(672, 4, 2, '2018-04-30 21:15:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(673, 4, 2, '2018-04-30 21:15:26', 'Compra', '2', 1, 2700, 2700, '494/41'),
(674, 4, 2, '2018-04-30 21:17:01', 'Compra', '2', 1, 2700, 2700, '494/41'),
(675, 4, 2, '2018-04-30 21:17:01', 'Compra', '2', 1, 2700, 2700, '494/41'),
(676, 4, 2, '2018-04-30 21:17:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(677, 4, 2, '2018-04-30 21:17:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(678, 4, 2, '2018-04-30 21:17:58', 'Compra', '2', 1, 2700, 2700, '494/41'),
(679, 4, 2, '2018-04-30 21:17:58', 'Compra', '2', 1, 2700, 2700, '494/41'),
(680, 4, 2, '2018-04-30 21:18:43', 'Compra', '2', 1, 2700, 2700, '494/41'),
(681, 4, 2, '2018-04-30 21:18:43', 'Compra', '2', 1, 2700, 2700, '494/41'),
(682, 4, 2, '2018-04-30 21:20:56', 'Compra', '2', 1, 2700, 2700, '494/41'),
(683, 4, 2, '2018-04-30 21:20:56', 'Compra', '2', 1, 2700, 2700, '494/41'),
(684, 4, 2, '2018-04-30 21:21:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(685, 4, 2, '2018-04-30 21:21:16', 'Compra', '2', 1, 2700, 2700, '494/41'),
(686, 4, 2, '2018-04-30 21:21:32', 'Compra', '2', 1, 2700, 2700, '494/41'),
(687, 4, 2, '2018-04-30 21:21:32', 'Compra', '2', 1, 2700, 2700, '494/41'),
(688, 4, 2, '2018-04-30 21:22:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(689, 4, 2, '2018-04-30 21:22:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(690, 4, 2, '2018-04-30 21:22:24', 'Compra', '2', 1, 2700, 2700, '494/41'),
(691, 4, 2, '2018-04-30 21:22:24', 'Compra', '2', 1, 2700, 2700, '494/41'),
(692, 4, 2, '2018-04-30 21:23:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(693, 4, 2, '2018-04-30 21:23:05', 'Compra', '2', 1, 2700, 2700, '494/41'),
(694, 4, 2, '2018-04-30 21:26:10', 'Compra', '2', 1, 2700, 2700, '494/41'),
(695, 4, 2, '2018-04-30 21:26:10', 'Compra', '2', 1, 2700, 2700, '494/41'),
(696, 4, 2, '2018-04-30 21:26:41', 'Compra', '2', 1, 2700, 2700, '494/41'),
(697, 4, 2, '2018-04-30 21:26:41', 'Compra', '2', 1, 2700, 2700, '494/41'),
(698, 4, 2, '2018-04-30 21:31:31', 'Compra', '2', 1, 2700, 2700, '494/41'),
(699, 4, 2, '2018-04-30 21:31:31', 'Compra', '2', 1, 2700, 2700, '494/41'),
(700, 4, 2, '2018-04-30 21:32:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(701, 4, 2, '2018-04-30 21:32:25', 'Compra', '2', 1, 2700, 2700, '494/41'),
(702, 4, 2, '2018-04-30 21:33:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(703, 4, 2, '2018-04-30 21:33:00', 'Compra', '2', 1, 2700, 2700, '494/41'),
(704, 4, 2, '2018-04-30 21:38:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(705, 4, 2, '2018-04-30 21:38:11', 'Compra', '2', 1, 2700, 2700, '494/41'),
(706, 4, 2, '2018-04-30 21:39:14', 'Compra', '2', 1, 2700, 2700, '494/41'),
(707, 4, 2, '2018-04-30 21:39:14', 'Compra', '2', 1, 2700, 2700, '494/41'),
(708, 4, 2, '2018-04-30 21:39:32', 'Compra', '2', 1, 2700, 2700, '494/41'),
(709, 4, 2, '2018-04-30 21:39:32', 'Compra', '2', 1, 2700, 2700, '494/41'),
(710, 4, 2, '2018-04-30 21:40:12', 'Compra', '2', 1, 2700, 2700, '494/41'),
(711, 4, 2, '2018-04-30 21:40:12', 'Compra', '2', 1, 2700, 2700, '494/41'),
(712, 4, 2, '2018-04-30 21:40:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(713, 4, 2, '2018-04-30 21:40:28', 'Compra', '2', 1, 2700, 2700, '494/41'),
(714, 4, 2, '2018-04-30 21:41:34', 'Compra', '2', 1, 2700, 2700, '494/41'),
(715, 4, 2, '2018-04-30 21:41:34', 'Compra', '2', 1, 2700, 2700, '494/41'),
(716, 4, 2, '2018-04-30 21:50:55', 'Compra', '2', 1, 2700, 2700, '880-59'),
(717, 1, 2, '2018-04-30 21:50:55', 'Compra', '2', 2, 32400, 16200, '880-59'),
(718, 4, 2, '2018-04-30 21:50:55', 'Compra', '2', 4, 10800, 2700, '880-59'),
(719, 4, 2, '2018-04-30 21:51:56', 'Compra', '2', 1, 2700, 2700, '880-59'),
(720, 1, 2, '2018-04-30 21:51:56', 'Compra', '2', 2, 32400, 16200, '880-59'),
(721, 4, 2, '2018-04-30 21:51:56', 'Compra', '2', 4, 10800, 2700, '880-59'),
(722, 1, 2, '2018-04-30 21:51:56', 'Compra', '2', 1, 16200, 16200, '880-59'),
(723, 4, 2, '2018-04-30 21:56:30', 'Compra', '2', 1, 2700, 2700, '880-59'),
(724, 1, 2, '2018-04-30 21:56:30', 'Compra', '2', 2, 32400, 16200, '880-59'),
(725, 4, 2, '2018-04-30 21:56:30', 'Compra', '2', 4, 10800, 2700, '880-59'),
(726, 1, 2, '2018-04-30 21:56:30', 'Compra', '2', 1, 16200, 16200, '880-59'),
(727, 4, 2, '2018-04-30 21:57:10', 'Compra', '2', 1, 2700, 2700, '880-59'),
(728, 1, 2, '2018-04-30 21:57:10', 'Compra', '2', 2, 32400, 16200, '880-59'),
(729, 4, 2, '2018-04-30 21:57:10', 'Compra', '2', 4, 10800, 2700, '880-59'),
(730, 1, 2, '2018-04-30 21:57:10', 'Compra', '2', 1, 16200, 16200, '880-59'),
(731, 4, 2, '2018-04-30 21:58:01', 'Compra', '2', 1, 2700, 2700, '880-59'),
(732, 1, 2, '2018-04-30 21:58:01', 'Compra', '2', 2, 32400, 16200, '880-59'),
(733, 4, 2, '2018-04-30 21:58:01', 'Compra', '2', 4, 10800, 2700, '880-59'),
(734, 1, 2, '2018-04-30 21:58:01', 'Compra', '2', 1, 16200, 16200, '880-59'),
(735, 4, 2, '2018-04-30 21:58:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(736, 1, 2, '2018-04-30 21:58:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(737, 4, 2, '2018-04-30 21:58:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(738, 1, 2, '2018-04-30 21:58:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(739, 4, 2, '2018-04-30 21:58:35', 'Compra', '2', 1, 2700, 2700, '880-59'),
(740, 1, 2, '2018-04-30 21:58:35', 'Compra', '2', 2, 32400, 16200, '880-59'),
(741, 4, 2, '2018-04-30 21:58:35', 'Compra', '2', 4, 10800, 2700, '880-59'),
(742, 1, 2, '2018-04-30 21:58:35', 'Compra', '2', 1, 16200, 16200, '880-59'),
(743, 4, 2, '2018-04-30 21:59:12', 'Compra', '2', 1, 2700, 2700, '880-59'),
(744, 1, 2, '2018-04-30 21:59:12', 'Compra', '2', 2, 32400, 16200, '880-59'),
(745, 4, 2, '2018-04-30 21:59:12', 'Compra', '2', 4, 10800, 2700, '880-59'),
(746, 1, 2, '2018-04-30 21:59:12', 'Compra', '2', 1, 16200, 16200, '880-59'),
(747, 4, 2, '2018-04-30 21:59:37', 'Compra', '2', 1, 2700, 2700, '880-59'),
(748, 1, 2, '2018-04-30 21:59:37', 'Compra', '2', 2, 32400, 16200, '880-59'),
(749, 4, 2, '2018-04-30 21:59:37', 'Compra', '2', 4, 10800, 2700, '880-59'),
(750, 1, 2, '2018-04-30 21:59:37', 'Compra', '2', 1, 16200, 16200, '880-59'),
(751, 4, 2, '2018-04-30 22:00:00', 'Compra', '2', 1, 2700, 2700, '880-59'),
(752, 1, 2, '2018-04-30 22:00:00', 'Compra', '2', 2, 32400, 16200, '880-59'),
(753, 4, 2, '2018-04-30 22:00:00', 'Compra', '2', 4, 10800, 2700, '880-59'),
(754, 1, 2, '2018-04-30 22:00:00', 'Compra', '2', 1, 16200, 16200, '880-59'),
(755, 4, 2, '2018-04-30 22:00:19', 'Compra', '2', 1, 2700, 2700, '880-59'),
(756, 1, 2, '2018-04-30 22:00:19', 'Compra', '2', 2, 32400, 16200, '880-59'),
(757, 4, 2, '2018-04-30 22:00:19', 'Compra', '2', 4, 10800, 2700, '880-59'),
(758, 1, 2, '2018-04-30 22:00:19', 'Compra', '2', 1, 16200, 16200, '880-59'),
(759, 4, 2, '2018-04-30 22:00:55', 'Compra', '2', 1, 2700, 2700, '880-59'),
(760, 1, 2, '2018-04-30 22:00:55', 'Compra', '2', 2, 32400, 16200, '880-59'),
(761, 4, 2, '2018-04-30 22:00:55', 'Compra', '2', 4, 10800, 2700, '880-59'),
(762, 1, 2, '2018-04-30 22:00:55', 'Compra', '2', 1, 16200, 16200, '880-59'),
(763, 4, 2, '2018-04-30 22:01:18', 'Compra', '2', 1, 2700, 2700, '880-59'),
(764, 1, 2, '2018-04-30 22:01:18', 'Compra', '2', 2, 32400, 16200, '880-59'),
(765, 4, 2, '2018-04-30 22:01:18', 'Compra', '2', 4, 10800, 2700, '880-59'),
(766, 1, 2, '2018-04-30 22:01:18', 'Compra', '2', 1, 16200, 16200, '880-59'),
(767, 4, 2, '2018-04-30 22:01:48', 'Compra', '2', 1, 2700, 2700, '880-59'),
(768, 1, 2, '2018-04-30 22:01:48', 'Compra', '2', 2, 32400, 16200, '880-59'),
(769, 4, 2, '2018-04-30 22:01:48', 'Compra', '2', 4, 10800, 2700, '880-59'),
(770, 1, 2, '2018-04-30 22:01:48', 'Compra', '2', 1, 16200, 16200, '880-59'),
(771, 4, 2, '2018-04-30 22:02:50', 'Compra', '2', 1, 2700, 2700, '880-59'),
(772, 1, 2, '2018-04-30 22:02:50', 'Compra', '2', 2, 32400, 16200, '880-59'),
(773, 4, 2, '2018-04-30 22:02:50', 'Compra', '2', 4, 10800, 2700, '880-59'),
(774, 1, 2, '2018-04-30 22:02:50', 'Compra', '2', 1, 16200, 16200, '880-59'),
(775, 4, 2, '2018-04-30 22:03:07', 'Compra', '2', 1, 2700, 2700, '880-59'),
(776, 1, 2, '2018-04-30 22:03:07', 'Compra', '2', 2, 32400, 16200, '880-59'),
(777, 4, 2, '2018-04-30 22:03:07', 'Compra', '2', 4, 10800, 2700, '880-59'),
(778, 1, 2, '2018-04-30 22:03:07', 'Compra', '2', 1, 16200, 16200, '880-59'),
(779, 4, 2, '2018-04-30 22:05:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(780, 1, 2, '2018-04-30 22:05:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(781, 4, 2, '2018-04-30 22:05:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(782, 1, 2, '2018-04-30 22:05:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(783, 4, 2, '2018-04-30 22:07:35', 'Compra', '2', 1, 2700, 2700, '880-59'),
(784, 1, 2, '2018-04-30 22:07:35', 'Compra', '2', 2, 32400, 16200, '880-59'),
(785, 4, 2, '2018-04-30 22:07:35', 'Compra', '2', 4, 10800, 2700, '880-59'),
(786, 1, 2, '2018-04-30 22:07:35', 'Compra', '2', 1, 16200, 16200, '880-59'),
(787, 4, 2, '2018-04-30 22:08:01', 'Compra', '2', 1, 2700, 2700, '880-59'),
(788, 1, 2, '2018-04-30 22:08:01', 'Compra', '2', 2, 32400, 16200, '880-59'),
(789, 4, 2, '2018-04-30 22:08:01', 'Compra', '2', 4, 10800, 2700, '880-59'),
(790, 1, 2, '2018-04-30 22:08:01', 'Compra', '2', 1, 16200, 16200, '880-59'),
(791, 4, 2, '2018-04-30 22:08:15', 'Compra', '2', 1, 2700, 2700, '880-59'),
(792, 1, 2, '2018-04-30 22:08:15', 'Compra', '2', 2, 32400, 16200, '880-59'),
(793, 4, 2, '2018-04-30 22:08:15', 'Compra', '2', 4, 10800, 2700, '880-59'),
(794, 1, 2, '2018-04-30 22:08:15', 'Compra', '2', 1, 16200, 16200, '880-59'),
(795, 4, 2, '2018-04-30 22:08:32', 'Compra', '2', 1, 2700, 2700, '880-59'),
(796, 1, 2, '2018-04-30 22:08:32', 'Compra', '2', 2, 32400, 16200, '880-59'),
(797, 4, 2, '2018-04-30 22:08:32', 'Compra', '2', 4, 10800, 2700, '880-59'),
(798, 1, 2, '2018-04-30 22:08:32', 'Compra', '2', 1, 16200, 16200, '880-59'),
(799, 4, 2, '2018-04-30 22:09:32', 'Compra', '2', 1, 2700, 2700, '880-59'),
(800, 1, 2, '2018-04-30 22:09:32', 'Compra', '2', 2, 32400, 16200, '880-59'),
(801, 4, 2, '2018-04-30 22:09:32', 'Compra', '2', 4, 10800, 2700, '880-59'),
(802, 1, 2, '2018-04-30 22:09:32', 'Compra', '2', 1, 16200, 16200, '880-59'),
(803, 4, 2, '2018-04-30 22:09:53', 'Compra', '2', 1, 2700, 2700, '880-59'),
(804, 1, 2, '2018-04-30 22:09:53', 'Compra', '2', 2, 32400, 16200, '880-59'),
(805, 4, 2, '2018-04-30 22:09:53', 'Compra', '2', 4, 10800, 2700, '880-59'),
(806, 1, 2, '2018-04-30 22:09:53', 'Compra', '2', 1, 16200, 16200, '880-59'),
(807, 4, 2, '2018-04-30 22:12:15', 'Compra', '2', 1, 2700, 2700, '880-59'),
(808, 1, 2, '2018-04-30 22:12:15', 'Compra', '2', 2, 32400, 16200, '880-59'),
(809, 4, 2, '2018-04-30 22:12:15', 'Compra', '2', 4, 10800, 2700, '880-59'),
(810, 1, 2, '2018-04-30 22:12:15', 'Compra', '2', 1, 16200, 16200, '880-59'),
(811, 4, 2, '2018-04-30 22:12:47', 'Compra', '2', 1, 2700, 2700, '880-59'),
(812, 1, 2, '2018-04-30 22:12:47', 'Compra', '2', 2, 32400, 16200, '880-59'),
(813, 4, 2, '2018-04-30 22:12:47', 'Compra', '2', 4, 10800, 2700, '880-59'),
(814, 1, 2, '2018-04-30 22:12:47', 'Compra', '2', 1, 16200, 16200, '880-59'),
(815, 4, 2, '2018-04-30 22:13:27', 'Compra', '2', 1, 2700, 2700, '880-59'),
(816, 1, 2, '2018-04-30 22:13:27', 'Compra', '2', 2, 32400, 16200, '880-59'),
(817, 4, 2, '2018-04-30 22:13:27', 'Compra', '2', 4, 10800, 2700, '880-59'),
(818, 1, 2, '2018-04-30 22:13:27', 'Compra', '2', 1, 16200, 16200, '880-59'),
(819, 4, 2, '2018-04-30 22:15:26', 'Compra', '2', 1, 2700, 2700, '880-59'),
(820, 1, 2, '2018-04-30 22:15:26', 'Compra', '2', 2, 32400, 16200, '880-59'),
(821, 4, 2, '2018-04-30 22:15:26', 'Compra', '2', 4, 10800, 2700, '880-59'),
(822, 1, 2, '2018-04-30 22:15:26', 'Compra', '2', 1, 16200, 16200, '880-59'),
(823, 4, 2, '2018-04-30 22:16:50', 'Compra', '2', 1, 2700, 2700, '880-59'),
(824, 1, 2, '2018-04-30 22:16:50', 'Compra', '2', 2, 32400, 16200, '880-59'),
(825, 4, 2, '2018-04-30 22:16:50', 'Compra', '2', 4, 10800, 2700, '880-59'),
(826, 1, 2, '2018-04-30 22:16:50', 'Compra', '2', 1, 16200, 16200, '880-59'),
(827, 4, 2, '2018-04-30 22:17:22', 'Compra', '2', 1, 2700, 2700, '880-59'),
(828, 1, 2, '2018-04-30 22:17:22', 'Compra', '2', 2, 32400, 16200, '880-59'),
(829, 4, 2, '2018-04-30 22:17:22', 'Compra', '2', 4, 10800, 2700, '880-59'),
(830, 1, 2, '2018-04-30 22:17:22', 'Compra', '2', 1, 16200, 16200, '880-59'),
(831, 4, 2, '2018-04-30 22:17:36', 'Compra', '2', 1, 2700, 2700, '880-59'),
(832, 1, 2, '2018-04-30 22:17:36', 'Compra', '2', 2, 32400, 16200, '880-59'),
(833, 4, 2, '2018-04-30 22:17:36', 'Compra', '2', 4, 10800, 2700, '880-59'),
(834, 1, 2, '2018-04-30 22:17:36', 'Compra', '2', 1, 16200, 16200, '880-59'),
(835, 4, 2, '2018-04-30 22:17:48', 'Compra', '2', 1, 2700, 2700, '880-59'),
(836, 1, 2, '2018-04-30 22:17:48', 'Compra', '2', 2, 32400, 16200, '880-59'),
(837, 4, 2, '2018-04-30 22:17:48', 'Compra', '2', 4, 10800, 2700, '880-59'),
(838, 1, 2, '2018-04-30 22:17:48', 'Compra', '2', 1, 16200, 16200, '880-59'),
(839, 4, 2, '2018-04-30 22:18:10', 'Compra', '2', 1, 2700, 2700, '880-59'),
(840, 1, 2, '2018-04-30 22:18:10', 'Compra', '2', 2, 32400, 16200, '880-59'),
(841, 4, 2, '2018-04-30 22:18:10', 'Compra', '2', 4, 10800, 2700, '880-59'),
(842, 1, 2, '2018-04-30 22:18:10', 'Compra', '2', 1, 16200, 16200, '880-59'),
(843, 4, 2, '2018-04-30 22:19:12', 'Compra', '2', 1, 2700, 2700, '880-59'),
(844, 1, 2, '2018-04-30 22:19:12', 'Compra', '2', 2, 32400, 16200, '880-59'),
(845, 4, 2, '2018-04-30 22:19:12', 'Compra', '2', 4, 10800, 2700, '880-59'),
(846, 1, 2, '2018-04-30 22:19:12', 'Compra', '2', 1, 16200, 16200, '880-59'),
(847, 4, 2, '2018-04-30 22:19:32', 'Compra', '2', 1, 2700, 2700, '880-59'),
(848, 1, 2, '2018-04-30 22:19:32', 'Compra', '2', 2, 32400, 16200, '880-59'),
(849, 4, 2, '2018-04-30 22:19:32', 'Compra', '2', 4, 10800, 2700, '880-59'),
(850, 1, 2, '2018-04-30 22:19:32', 'Compra', '2', 1, 16200, 16200, '880-59'),
(851, 4, 2, '2018-04-30 22:19:52', 'Compra', '2', 1, 2700, 2700, '880-59'),
(852, 1, 2, '2018-04-30 22:19:52', 'Compra', '2', 2, 32400, 16200, '880-59'),
(853, 4, 2, '2018-04-30 22:19:52', 'Compra', '2', 4, 10800, 2700, '880-59'),
(854, 1, 2, '2018-04-30 22:19:52', 'Compra', '2', 1, 16200, 16200, '880-59'),
(855, 4, 2, '2018-04-30 22:26:40', 'Compra', '2', 1, 2700, 2700, '880-59'),
(856, 1, 2, '2018-04-30 22:26:40', 'Compra', '2', 2, 32400, 16200, '880-59'),
(857, 4, 2, '2018-04-30 22:26:40', 'Compra', '2', 4, 10800, 2700, '880-59'),
(858, 1, 2, '2018-04-30 22:26:40', 'Compra', '2', 1, 16200, 16200, '880-59'),
(859, 4, 2, '2018-04-30 22:26:56', 'Compra', '2', 1, 2700, 2700, '880-59'),
(860, 1, 2, '2018-04-30 22:26:56', 'Compra', '2', 2, 32400, 16200, '880-59'),
(861, 4, 2, '2018-04-30 22:26:56', 'Compra', '2', 4, 10800, 2700, '880-59'),
(862, 1, 2, '2018-04-30 22:26:56', 'Compra', '2', 1, 16200, 16200, '880-59'),
(863, 4, 2, '2018-04-30 22:27:47', 'Compra', '2', 1, 2700, 2700, '880-59'),
(864, 1, 2, '2018-04-30 22:27:47', 'Compra', '2', 2, 32400, 16200, '880-59'),
(865, 4, 2, '2018-04-30 22:27:47', 'Compra', '2', 4, 10800, 2700, '880-59'),
(866, 1, 2, '2018-04-30 22:27:47', 'Compra', '2', 1, 16200, 16200, '880-59'),
(867, 4, 2, '2018-04-30 22:28:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(868, 1, 2, '2018-04-30 22:28:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(869, 4, 2, '2018-04-30 22:28:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(870, 1, 2, '2018-04-30 22:28:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(871, 4, 2, '2018-04-30 22:28:22', 'Compra', '2', 1, 2700, 2700, '880-59'),
(872, 1, 2, '2018-04-30 22:28:22', 'Compra', '2', 2, 32400, 16200, '880-59'),
(873, 4, 2, '2018-04-30 22:28:22', 'Compra', '2', 4, 10800, 2700, '880-59'),
(874, 1, 2, '2018-04-30 22:28:22', 'Compra', '2', 1, 16200, 16200, '880-59'),
(875, 4, 2, '2018-04-30 22:28:37', 'Compra', '2', 1, 2700, 2700, '880-59'),
(876, 1, 2, '2018-04-30 22:28:37', 'Compra', '2', 2, 32400, 16200, '880-59'),
(877, 4, 2, '2018-04-30 22:28:37', 'Compra', '2', 4, 10800, 2700, '880-59'),
(878, 1, 2, '2018-04-30 22:28:37', 'Compra', '2', 1, 16200, 16200, '880-59'),
(879, 4, 2, '2018-04-30 22:28:55', 'Compra', '2', 1, 2700, 2700, '880-59'),
(880, 1, 2, '2018-04-30 22:28:55', 'Compra', '2', 2, 32400, 16200, '880-59'),
(881, 4, 2, '2018-04-30 22:28:55', 'Compra', '2', 4, 10800, 2700, '880-59'),
(882, 1, 2, '2018-04-30 22:28:55', 'Compra', '2', 1, 16200, 16200, '880-59'),
(883, 4, 2, '2018-04-30 22:29:13', 'Compra', '2', 1, 2700, 2700, '880-59'),
(884, 1, 2, '2018-04-30 22:29:13', 'Compra', '2', 2, 32400, 16200, '880-59'),
(885, 4, 2, '2018-04-30 22:29:13', 'Compra', '2', 4, 10800, 2700, '880-59'),
(886, 1, 2, '2018-04-30 22:29:13', 'Compra', '2', 1, 16200, 16200, '880-59'),
(887, 4, 2, '2018-04-30 22:29:28', 'Compra', '2', 1, 2700, 2700, '880-59'),
(888, 1, 2, '2018-04-30 22:29:28', 'Compra', '2', 2, 32400, 16200, '880-59'),
(889, 4, 2, '2018-04-30 22:29:28', 'Compra', '2', 4, 10800, 2700, '880-59'),
(890, 1, 2, '2018-04-30 22:29:28', 'Compra', '2', 1, 16200, 16200, '880-59'),
(891, 4, 2, '2018-04-30 22:29:41', 'Compra', '2', 1, 2700, 2700, '880-59'),
(892, 1, 2, '2018-04-30 22:29:41', 'Compra', '2', 2, 32400, 16200, '880-59'),
(893, 4, 2, '2018-04-30 22:29:41', 'Compra', '2', 4, 10800, 2700, '880-59'),
(894, 1, 2, '2018-04-30 22:29:41', 'Compra', '2', 1, 16200, 16200, '880-59'),
(895, 4, 2, '2018-04-30 22:29:56', 'Compra', '2', 1, 2700, 2700, '880-59'),
(896, 1, 2, '2018-04-30 22:29:56', 'Compra', '2', 2, 32400, 16200, '880-59'),
(897, 4, 2, '2018-04-30 22:29:56', 'Compra', '2', 4, 10800, 2700, '880-59'),
(898, 1, 2, '2018-04-30 22:29:56', 'Compra', '2', 1, 16200, 16200, '880-59'),
(899, 4, 2, '2018-04-30 22:30:09', 'Compra', '2', 1, 2700, 2700, '880-59'),
(900, 1, 2, '2018-04-30 22:30:09', 'Compra', '2', 2, 32400, 16200, '880-59'),
(901, 4, 2, '2018-04-30 22:30:09', 'Compra', '2', 4, 10800, 2700, '880-59'),
(902, 1, 2, '2018-04-30 22:30:09', 'Compra', '2', 1, 16200, 16200, '880-59'),
(903, 4, 2, '2018-04-30 22:31:18', 'Compra', '2', 1, 2700, 2700, '880-59'),
(904, 1, 2, '2018-04-30 22:31:18', 'Compra', '2', 2, 32400, 16200, '880-59'),
(905, 4, 2, '2018-04-30 22:31:18', 'Compra', '2', 4, 10800, 2700, '880-59'),
(906, 1, 2, '2018-04-30 22:31:18', 'Compra', '2', 1, 16200, 16200, '880-59'),
(907, 4, 2, '2018-04-30 22:31:38', 'Compra', '2', 1, 2700, 2700, '880-59'),
(908, 1, 2, '2018-04-30 22:31:38', 'Compra', '2', 2, 32400, 16200, '880-59'),
(909, 4, 2, '2018-04-30 22:31:38', 'Compra', '2', 4, 10800, 2700, '880-59'),
(910, 1, 2, '2018-04-30 22:31:38', 'Compra', '2', 1, 16200, 16200, '880-59'),
(911, 4, 2, '2018-04-30 22:36:50', 'Compra', '2', 1, 2700, 2700, '880-59'),
(912, 1, 2, '2018-04-30 22:36:50', 'Compra', '2', 2, 32400, 16200, '880-59'),
(913, 4, 2, '2018-04-30 22:36:50', 'Compra', '2', 4, 10800, 2700, '880-59'),
(914, 1, 2, '2018-04-30 22:36:50', 'Compra', '2', 1, 16200, 16200, '880-59'),
(915, 4, 2, '2018-04-30 22:37:28', 'Compra', '2', 1, 2700, 2700, '880-59'),
(916, 1, 2, '2018-04-30 22:37:28', 'Compra', '2', 2, 32400, 16200, '880-59'),
(917, 4, 2, '2018-04-30 22:37:29', 'Compra', '2', 4, 10800, 2700, '880-59'),
(918, 1, 2, '2018-04-30 22:37:29', 'Compra', '2', 1, 16200, 16200, '880-59'),
(919, 4, 2, '2018-04-30 22:37:49', 'Compra', '2', 1, 2700, 2700, '880-59'),
(920, 1, 2, '2018-04-30 22:37:49', 'Compra', '2', 2, 32400, 16200, '880-59'),
(921, 4, 2, '2018-04-30 22:37:49', 'Compra', '2', 4, 10800, 2700, '880-59'),
(922, 1, 2, '2018-04-30 22:37:49', 'Compra', '2', 1, 16200, 16200, '880-59'),
(923, 4, 2, '2018-04-30 22:38:00', 'Compra', '2', 1, 2700, 2700, '880-59'),
(924, 1, 2, '2018-04-30 22:38:00', 'Compra', '2', 2, 32400, 16200, '880-59'),
(925, 4, 2, '2018-04-30 22:38:00', 'Compra', '2', 4, 10800, 2700, '880-59'),
(926, 1, 2, '2018-04-30 22:38:00', 'Compra', '2', 1, 16200, 16200, '880-59'),
(927, 4, 2, '2018-04-30 22:38:10', 'Compra', '2', 1, 2700, 2700, '880-59'),
(928, 1, 2, '2018-04-30 22:38:10', 'Compra', '2', 2, 32400, 16200, '880-59'),
(929, 4, 2, '2018-04-30 22:38:10', 'Compra', '2', 4, 10800, 2700, '880-59'),
(930, 1, 2, '2018-04-30 22:38:10', 'Compra', '2', 1, 16200, 16200, '880-59'),
(931, 4, 2, '2018-04-30 22:41:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(932, 1, 2, '2018-04-30 22:41:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(933, 4, 2, '2018-04-30 22:41:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(934, 1, 2, '2018-04-30 22:41:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(935, 4, 2, '2018-04-30 22:42:01', 'Compra', '2', 1, 2700, 2700, '880-59'),
(936, 1, 2, '2018-04-30 22:42:01', 'Compra', '2', 2, 32400, 16200, '880-59'),
(937, 4, 2, '2018-04-30 22:42:01', 'Compra', '2', 4, 10800, 2700, '880-59'),
(938, 1, 2, '2018-04-30 22:42:01', 'Compra', '2', 1, 16200, 16200, '880-59'),
(939, 4, 2, '2018-04-30 22:42:22', 'Compra', '2', 1, 2700, 2700, '880-59'),
(940, 1, 2, '2018-04-30 22:42:22', 'Compra', '2', 2, 32400, 16200, '880-59'),
(941, 4, 2, '2018-04-30 22:42:22', 'Compra', '2', 4, 10800, 2700, '880-59'),
(942, 1, 2, '2018-04-30 22:42:22', 'Compra', '2', 1, 16200, 16200, '880-59'),
(943, 4, 2, '2018-04-30 22:42:51', 'Compra', '2', 1, 2700, 2700, '880-59'),
(944, 1, 2, '2018-04-30 22:42:51', 'Compra', '2', 2, 32400, 16200, '880-59'),
(945, 4, 2, '2018-04-30 22:42:51', 'Compra', '2', 4, 10800, 2700, '880-59'),
(946, 1, 2, '2018-04-30 22:42:51', 'Compra', '2', 1, 16200, 16200, '880-59'),
(947, 4, 2, '2018-04-30 22:43:10', 'Compra', '2', 1, 2700, 2700, '880-59'),
(948, 1, 2, '2018-04-30 22:43:10', 'Compra', '2', 2, 32400, 16200, '880-59'),
(949, 4, 2, '2018-04-30 22:43:10', 'Compra', '2', 4, 10800, 2700, '880-59'),
(950, 1, 2, '2018-04-30 22:43:10', 'Compra', '2', 1, 16200, 16200, '880-59'),
(951, 4, 2, '2018-04-30 22:43:42', 'Compra', '2', 1, 2700, 2700, '880-59'),
(952, 1, 2, '2018-04-30 22:43:42', 'Compra', '2', 2, 32400, 16200, '880-59'),
(953, 4, 2, '2018-04-30 22:43:42', 'Compra', '2', 4, 10800, 2700, '880-59'),
(954, 1, 2, '2018-04-30 22:43:42', 'Compra', '2', 1, 16200, 16200, '880-59'),
(955, 4, 2, '2018-04-30 22:44:09', 'Compra', '2', 1, 2700, 2700, '880-59'),
(956, 1, 2, '2018-04-30 22:44:09', 'Compra', '2', 2, 32400, 16200, '880-59'),
(957, 4, 2, '2018-04-30 22:44:09', 'Compra', '2', 4, 10800, 2700, '880-59'),
(958, 1, 2, '2018-04-30 22:44:09', 'Compra', '2', 1, 16200, 16200, '880-59'),
(959, 4, 2, '2018-04-30 22:45:14', 'Compra', '2', 1, 2700, 2700, '880-59'),
(960, 1, 2, '2018-04-30 22:45:14', 'Compra', '2', 2, 32400, 16200, '880-59'),
(961, 4, 2, '2018-04-30 22:45:14', 'Compra', '2', 4, 10800, 2700, '880-59'),
(962, 1, 2, '2018-04-30 22:45:14', 'Compra', '2', 1, 16200, 16200, '880-59'),
(963, 4, 2, '2018-04-30 22:46:35', 'Compra', '2', 1, 2700, 2700, '880-59'),
(964, 1, 2, '2018-04-30 22:46:35', 'Compra', '2', 2, 32400, 16200, '880-59'),
(965, 4, 2, '2018-04-30 22:46:35', 'Compra', '2', 4, 10800, 2700, '880-59'),
(966, 1, 2, '2018-04-30 22:46:35', 'Compra', '2', 1, 16200, 16200, '880-59'),
(967, 4, 2, '2018-04-30 22:54:45', 'Compra', '2', 1, 2700, 2700, '880-59'),
(968, 1, 2, '2018-04-30 22:54:45', 'Compra', '2', 2, 32400, 16200, '880-59'),
(969, 4, 2, '2018-04-30 22:54:45', 'Compra', '2', 4, 10800, 2700, '880-59'),
(970, 1, 2, '2018-04-30 22:54:45', 'Compra', '2', 1, 16200, 16200, '880-59'),
(971, 4, 2, '2018-04-30 22:55:08', 'Compra', '2', 1, 2700, 2700, '880-59'),
(972, 1, 2, '2018-04-30 22:55:08', 'Compra', '2', 2, 32400, 16200, '880-59'),
(973, 4, 2, '2018-04-30 22:55:08', 'Compra', '2', 4, 10800, 2700, '880-59'),
(974, 1, 2, '2018-04-30 22:55:08', 'Compra', '2', 1, 16200, 16200, '880-59'),
(975, 4, 2, '2018-04-30 22:55:38', 'Compra', '2', 1, 2700, 2700, '880-59'),
(976, 1, 2, '2018-04-30 22:55:38', 'Compra', '2', 2, 32400, 16200, '880-59'),
(977, 4, 2, '2018-04-30 22:55:38', 'Compra', '2', 4, 10800, 2700, '880-59'),
(978, 1, 2, '2018-04-30 22:55:38', 'Compra', '2', 1, 16200, 16200, '880-59'),
(979, 4, 2, '2018-04-30 22:56:06', 'Compra', '2', 1, 2700, 2700, '880-59'),
(980, 1, 2, '2018-04-30 22:56:06', 'Compra', '2', 2, 32400, 16200, '880-59'),
(981, 4, 2, '2018-04-30 22:56:06', 'Compra', '2', 4, 10800, 2700, '880-59'),
(982, 1, 2, '2018-04-30 22:56:06', 'Compra', '2', 1, 16200, 16200, '880-59'),
(983, 4, 2, '2018-04-30 22:56:30', 'Compra', '2', 1, 2700, 2700, '880-59'),
(984, 1, 2, '2018-04-30 22:56:30', 'Compra', '2', 2, 32400, 16200, '880-59'),
(985, 4, 2, '2018-04-30 22:56:30', 'Compra', '2', 4, 10800, 2700, '880-59'),
(986, 1, 2, '2018-04-30 22:56:30', 'Compra', '2', 1, 16200, 16200, '880-59'),
(987, 4, 2, '2018-04-30 22:56:56', 'Compra', '2', 1, 2700, 2700, '880-59'),
(988, 1, 2, '2018-04-30 22:56:56', 'Compra', '2', 2, 32400, 16200, '880-59'),
(989, 4, 2, '2018-04-30 22:56:56', 'Compra', '2', 4, 10800, 2700, '880-59'),
(990, 1, 2, '2018-04-30 22:56:56', 'Compra', '2', 1, 16200, 16200, '880-59'),
(991, 4, 2, '2018-04-30 22:57:30', 'Compra', '2', 1, 2700, 2700, '880-59'),
(992, 1, 2, '2018-04-30 22:57:30', 'Compra', '2', 2, 32400, 16200, '880-59'),
(993, 4, 2, '2018-04-30 22:57:30', 'Compra', '2', 4, 10800, 2700, '880-59'),
(994, 1, 2, '2018-04-30 22:57:30', 'Compra', '2', 1, 16200, 16200, '880-59'),
(995, 4, 2, '2018-04-30 22:57:56', 'Compra', '2', 1, 2700, 2700, '880-59'),
(996, 1, 2, '2018-04-30 22:57:56', 'Compra', '2', 2, 32400, 16200, '880-59'),
(997, 4, 2, '2018-04-30 22:57:56', 'Compra', '2', 4, 10800, 2700, '880-59'),
(998, 1, 2, '2018-04-30 22:57:56', 'Compra', '2', 1, 16200, 16200, '880-59'),
(999, 4, 2, '2018-04-30 22:58:22', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1000, 1, 2, '2018-04-30 22:58:22', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1001, 4, 2, '2018-04-30 22:58:22', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1002, 1, 2, '2018-04-30 22:58:22', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1003, 4, 2, '2018-04-30 22:59:16', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1004, 1, 2, '2018-04-30 22:59:16', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1005, 4, 2, '2018-04-30 22:59:16', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1006, 1, 2, '2018-04-30 22:59:16', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1007, 4, 2, '2018-04-30 22:59:28', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1008, 1, 2, '2018-04-30 22:59:28', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1009, 4, 2, '2018-04-30 22:59:28', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1010, 1, 2, '2018-04-30 22:59:28', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1011, 4, 2, '2018-04-30 23:00:15', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1012, 1, 2, '2018-04-30 23:00:15', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1013, 4, 2, '2018-04-30 23:00:15', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1014, 1, 2, '2018-04-30 23:00:15', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1015, 4, 2, '2018-04-30 23:00:37', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1016, 1, 2, '2018-04-30 23:00:37', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1017, 4, 2, '2018-04-30 23:00:37', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1018, 1, 2, '2018-04-30 23:00:37', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1019, 4, 2, '2018-04-30 23:01:13', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1020, 1, 2, '2018-04-30 23:01:13', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1021, 4, 2, '2018-04-30 23:01:13', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1022, 1, 2, '2018-04-30 23:01:13', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1023, 4, 2, '2018-04-30 23:01:38', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1024, 1, 2, '2018-04-30 23:01:38', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1025, 4, 2, '2018-04-30 23:01:38', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1026, 1, 2, '2018-04-30 23:01:38', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1027, 4, 2, '2018-04-30 23:01:53', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1028, 1, 2, '2018-04-30 23:01:53', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1029, 4, 2, '2018-04-30 23:01:53', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1030, 1, 2, '2018-04-30 23:01:53', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1031, 4, 2, '2018-04-30 23:02:33', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1032, 1, 2, '2018-04-30 23:02:33', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1033, 4, 2, '2018-04-30 23:02:33', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1034, 1, 2, '2018-04-30 23:02:33', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1035, 4, 2, '2018-04-30 23:02:51', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1036, 1, 2, '2018-04-30 23:02:51', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1037, 4, 2, '2018-04-30 23:02:51', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1038, 1, 2, '2018-04-30 23:02:51', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1039, 4, 2, '2018-04-30 23:03:05', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1040, 1, 2, '2018-04-30 23:03:05', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1041, 4, 2, '2018-04-30 23:03:05', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1042, 1, 2, '2018-04-30 23:03:05', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1043, 4, 2, '2018-04-30 23:03:20', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1044, 1, 2, '2018-04-30 23:03:20', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1045, 4, 2, '2018-04-30 23:03:20', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1046, 1, 2, '2018-04-30 23:03:20', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1047, 4, 2, '2018-04-30 23:03:36', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1048, 1, 2, '2018-04-30 23:03:36', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1049, 4, 2, '2018-04-30 23:03:36', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1050, 1, 2, '2018-04-30 23:03:36', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1051, 4, 2, '2018-04-30 23:03:53', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1052, 1, 2, '2018-04-30 23:03:53', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1053, 4, 2, '2018-04-30 23:03:53', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1054, 1, 2, '2018-04-30 23:03:53', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1055, 4, 2, '2018-04-30 23:04:21', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1056, 1, 2, '2018-04-30 23:04:21', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1057, 4, 2, '2018-04-30 23:04:21', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1058, 1, 2, '2018-04-30 23:04:21', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1059, 4, 2, '2018-04-30 23:05:45', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1060, 1, 2, '2018-04-30 23:05:45', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1061, 4, 2, '2018-04-30 23:05:45', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1062, 1, 2, '2018-04-30 23:05:45', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1063, 4, 2, '2018-04-30 23:07:21', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1064, 1, 2, '2018-04-30 23:07:21', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1065, 4, 2, '2018-04-30 23:07:21', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1066, 1, 2, '2018-04-30 23:07:21', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1067, 4, 2, '2018-04-30 23:08:15', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1068, 1, 2, '2018-04-30 23:08:15', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1069, 4, 2, '2018-04-30 23:08:15', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1070, 1, 2, '2018-04-30 23:08:15', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1071, 4, 2, '2018-04-30 23:08:34', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1072, 1, 2, '2018-04-30 23:08:34', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1073, 4, 2, '2018-04-30 23:08:34', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1074, 1, 2, '2018-04-30 23:08:34', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1075, 4, 2, '2018-04-30 23:09:08', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1076, 1, 2, '2018-04-30 23:09:08', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1077, 4, 2, '2018-04-30 23:09:08', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1078, 1, 2, '2018-04-30 23:09:08', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1079, 4, 2, '2018-04-30 23:10:41', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1080, 1, 2, '2018-04-30 23:10:41', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1081, 4, 2, '2018-04-30 23:10:41', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1082, 1, 2, '2018-04-30 23:10:41', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1083, 4, 2, '2018-04-30 23:10:58', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1084, 1, 2, '2018-04-30 23:10:58', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1085, 4, 2, '2018-04-30 23:10:58', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1086, 1, 2, '2018-04-30 23:10:58', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1087, 4, 2, '2018-04-30 23:11:20', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1088, 1, 2, '2018-04-30 23:11:20', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1089, 4, 2, '2018-04-30 23:11:20', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1090, 1, 2, '2018-04-30 23:11:20', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1091, 4, 2, '2018-04-30 23:11:46', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1092, 1, 2, '2018-04-30 23:11:46', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1093, 4, 2, '2018-04-30 23:11:46', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1094, 1, 2, '2018-04-30 23:11:46', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1095, 4, 2, '2018-04-30 23:12:02', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1096, 1, 2, '2018-04-30 23:12:02', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1097, 4, 2, '2018-04-30 23:12:02', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1098, 1, 2, '2018-04-30 23:12:02', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1099, 4, 2, '2018-04-30 23:12:18', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1100, 1, 2, '2018-04-30 23:12:18', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1101, 4, 2, '2018-04-30 23:12:18', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1102, 1, 2, '2018-04-30 23:12:18', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1103, 4, 2, '2018-04-30 23:12:46', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1104, 1, 2, '2018-04-30 23:12:46', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1105, 4, 2, '2018-04-30 23:12:46', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1106, 1, 2, '2018-04-30 23:12:46', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1107, 4, 2, '2018-04-30 23:13:04', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1108, 1, 2, '2018-04-30 23:13:04', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1109, 4, 2, '2018-04-30 23:13:04', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1110, 1, 2, '2018-04-30 23:13:04', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1111, 4, 2, '2018-04-30 23:13:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1112, 1, 2, '2018-04-30 23:13:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1113, 4, 2, '2018-04-30 23:13:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1114, 1, 2, '2018-04-30 23:13:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1115, 4, 2, '2018-04-30 23:13:32', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1116, 1, 2, '2018-04-30 23:13:32', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1117, 4, 2, '2018-04-30 23:13:32', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1118, 1, 2, '2018-04-30 23:13:32', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1119, 4, 2, '2018-04-30 23:13:51', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1120, 1, 2, '2018-04-30 23:13:51', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1121, 4, 2, '2018-04-30 23:13:51', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1122, 1, 2, '2018-04-30 23:13:51', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1123, 4, 2, '2018-04-30 23:14:20', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1124, 1, 2, '2018-04-30 23:14:20', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1125, 4, 2, '2018-04-30 23:14:20', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1126, 1, 2, '2018-04-30 23:14:20', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1127, 4, 2, '2018-05-01 07:37:57', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1128, 1, 2, '2018-05-01 07:37:57', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1129, 4, 2, '2018-05-01 07:37:57', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1130, 1, 2, '2018-05-01 07:37:57', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1131, 4, 2, '2018-05-01 07:39:44', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1132, 1, 2, '2018-05-01 07:39:44', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1133, 4, 2, '2018-05-01 07:39:44', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1134, 1, 2, '2018-05-01 07:39:44', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1135, 4, 2, '2018-05-01 07:40:04', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1136, 1, 2, '2018-05-01 07:40:04', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1137, 4, 2, '2018-05-01 07:40:04', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1138, 1, 2, '2018-05-01 07:40:04', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1139, 4, 2, '2018-05-01 07:41:51', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1140, 1, 2, '2018-05-01 07:41:51', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1141, 4, 2, '2018-05-01 07:41:51', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1142, 1, 2, '2018-05-01 07:41:51', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1143, 4, 2, '2018-05-01 07:42:45', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1144, 1, 2, '2018-05-01 07:42:45', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1145, 4, 2, '2018-05-01 07:42:45', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1146, 1, 2, '2018-05-01 07:42:45', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1147, 4, 2, '2018-05-01 07:43:02', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1148, 1, 2, '2018-05-01 07:43:02', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1149, 4, 2, '2018-05-01 07:43:02', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1150, 1, 2, '2018-05-01 07:43:02', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1151, 4, 2, '2018-05-01 07:43:31', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1152, 1, 2, '2018-05-01 07:43:31', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1153, 4, 2, '2018-05-01 07:43:31', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1154, 1, 2, '2018-05-01 07:43:31', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1155, 4, 2, '2018-05-01 07:44:04', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1156, 1, 2, '2018-05-01 07:44:04', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1157, 4, 2, '2018-05-01 07:44:04', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1158, 1, 2, '2018-05-01 07:44:04', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1159, 4, 2, '2018-05-01 07:44:24', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1160, 1, 2, '2018-05-01 07:44:24', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1161, 4, 2, '2018-05-01 07:44:24', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1162, 1, 2, '2018-05-01 07:44:24', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1163, 4, 2, '2018-05-01 07:44:35', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1164, 1, 2, '2018-05-01 07:44:35', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1165, 4, 2, '2018-05-01 07:44:35', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1166, 1, 2, '2018-05-01 07:44:35', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1167, 4, 2, '2018-05-01 07:44:48', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1168, 1, 2, '2018-05-01 07:44:48', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1169, 4, 2, '2018-05-01 07:44:48', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1170, 1, 2, '2018-05-01 07:44:48', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1171, 4, 2, '2018-05-01 07:45:01', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1172, 1, 2, '2018-05-01 07:45:01', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1173, 4, 2, '2018-05-01 07:45:01', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1174, 1, 2, '2018-05-01 07:45:01', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1175, 4, 2, '2018-05-01 07:45:13', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1176, 1, 2, '2018-05-01 07:45:13', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1177, 4, 2, '2018-05-01 07:45:13', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1178, 1, 2, '2018-05-01 07:45:13', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1179, 4, 2, '2018-05-01 07:45:24', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1180, 1, 2, '2018-05-01 07:45:24', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1181, 4, 2, '2018-05-01 07:45:24', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1182, 1, 2, '2018-05-01 07:45:24', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1183, 4, 2, '2018-05-01 07:45:38', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1184, 1, 2, '2018-05-01 07:45:38', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1185, 4, 2, '2018-05-01 07:45:38', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1186, 1, 2, '2018-05-01 07:45:38', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1187, 4, 2, '2018-05-01 07:46:17', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1188, 1, 2, '2018-05-01 07:46:17', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1189, 4, 2, '2018-05-01 07:46:17', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1190, 1, 2, '2018-05-01 07:46:17', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1191, 4, 2, '2018-05-01 07:46:35', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1192, 1, 2, '2018-05-01 07:46:35', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1193, 4, 2, '2018-05-01 07:46:35', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1194, 1, 2, '2018-05-01 07:46:35', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1195, 4, 2, '2018-05-01 07:46:48', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1196, 1, 2, '2018-05-01 07:46:48', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1197, 4, 2, '2018-05-01 07:46:48', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1198, 1, 2, '2018-05-01 07:46:48', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1199, 4, 2, '2018-05-01 07:48:29', 'Compra', '2', 1, 2700, 2700, '880-59'),
(1200, 1, 2, '2018-05-01 07:48:29', 'Compra', '2', 2, 32400, 16200, '880-59'),
(1201, 4, 2, '2018-05-01 07:48:29', 'Compra', '2', 4, 10800, 2700, '880-59'),
(1202, 1, 2, '2018-05-01 07:48:29', 'Compra', '2', 1, 16200, 16200, '880-59'),
(1203, 1, 2, '2018-05-04 21:24:00', 'Venda', '1', 3, 50544, 16848, '343/26'),
(1204, 1, 2, '2018-07-24 13:55:38', 'Compra', '', 0, 0, 0, '726-37'),
(1205, 5, 2, '2018-07-24 13:55:38', 'Compra', '', 0, 0, 0, '726-37'),
(1206, 1, 2, '2018-07-24 14:01:55', 'Compra', '', 9, 18162, 2018, '726-37'),
(1207, 5, 2, '2018-07-24 14:01:55', 'Compra', '', 10, 20180, 2018, '726-37'),
(1208, 1, 2, '2018-07-24 14:02:09', 'Compra', '', 9, 0, 2018, '726-37'),
(1209, 5, 2, '2018-07-24 14:02:09', 'Compra', '', 10, 0, 2018, '726-37'),
(1210, 1, 2, '2018-07-24 14:02:18', 'Compra', '', 9, 0, 2018, '726-37'),
(1211, 5, 2, '2018-07-24 14:02:18', 'Compra', '', 10, 0, 2018, '726-37'),
(1212, 1, 2, '2018-07-24 14:02:54', 'Compra', '', 9, 0, 2018, '726-37'),
(1213, 5, 2, '2018-07-24 14:02:54', 'Compra', '', 10, 0, 2018, '726-37'),
(1214, 1, 2, '2018-07-24 14:04:21', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1215, 5, 2, '2018-07-24 14:04:21', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1216, 1, 2, '2018-07-24 14:25:42', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1217, 5, 2, '2018-07-24 14:25:42', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1218, 1, 2, '2018-07-24 14:25:50', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1219, 5, 2, '2018-07-24 14:25:50', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1220, 1, 2, '2018-07-24 14:26:37', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1221, 5, 2, '2018-07-24 14:26:37', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1222, 1, 2, '2018-07-24 14:27:00', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1223, 1, 2, '2018-07-24 14:27:02', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1224, 1, 2, '2018-07-24 14:27:20', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1225, 5, 2, '2018-07-24 14:27:20', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1226, 1, 2, '2018-07-24 14:27:20', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1227, 5, 2, '2018-07-24 14:27:20', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1228, 1, 2, '2018-07-24 14:27:21', 'Acerto', ' ', 9, 0, 2018, '726-37'),
(1229, 5, 2, '2018-07-24 14:27:21', 'Acerto', ' ', 10, 0, 2018, '726-37'),
(1230, 5, 2, '2018-07-24 14:42:43', 'Acerto', ' ', 25, 0, 2018, '726-37'),
(1231, 5, 2, '2018-07-24 14:43:06', 'Acerto', ' ', 25, 0, 2018, '726-37'),
(1232, 5, 2, '2018-07-24 14:45:56', 'Acerto', ' ', 25, 0, 2018, '726-37'),
(1233, 1, 2, '2018-07-25 08:34:45', 'Venda', '1', 1, 16848, 16848, '501/58'),
(1234, 5, 2, '2018-07-25 08:34:45', 'Venda', '1', 1, 1716, 1716, '501/58'),
(1235, 14, 2, '2018-07-26 13:02:19', 'Venda', '1', 100, 327000, 3270, '294/04'),
(1236, 1, 4, '2018-07-26 13:29:49', 'Venda', '6', 1, 16848, 16848, '778/42'),
(1237, 5, 4, '2018-07-26 13:29:49', 'Venda', '6', 1, 1716, 1716, '778/42'),
(1238, 17, 2, '2018-07-26 13:37:21', 'Venda', '1', 1, 57200, 57200, '494/08'),
(1239, 17, 2, '2018-07-26 13:38:07', 'Venda', '1', 1, 57200, 57200, '494/08'),
(1240, 17, 2, '2018-07-26 13:38:07', 'Venda', '1', 1, 57200, 57200, '494/08'),
(1241, 4, 2, '2018-07-26 13:39:15', 'Venda', '1', 100, 280800, 2808, '231/07'),
(1242, 28, 2, '2018-07-27 21:01:40', 'Venda', '1', 1, 675.8, 675.8, '486/42'),
(1243, 12, 2, '2018-07-27 21:01:40', 'Venda', '1', 1, 610.4, 610.4, '486/42'),
(1244, 13, 2, '2018-07-27 21:01:40', 'Venda', '1', 6, 784.8, 130.8, '486/42'),
(1245, 5, 2, '2018-07-28 07:17:12', 'Venda', '1', 1, 1716, 1716, '866/19'),
(1246, 5, 2, '2018-07-28 07:21:42', 'Venda', '1', 1, 1716, 1716, '866/19'),
(1247, 5, 2, '2018-07-28 07:22:35', 'Venda', '6', 1, 1716, 1716, '866/19'),
(1248, 19, 2, '2018-07-28 07:22:35', 'Venda', '6', 1, 21840, 21840, '866/19'),
(1249, 28, 2, '2018-07-28 07:22:35', 'Venda', '6', 1, 675.8, 675.8, '866/19'),
(1250, 19, 2, '2018-07-28 07:39:29', 'Venda', '6', 5, 109200, 21840, '531/52'),
(1251, 19, 2, '2018-07-28 07:40:17', 'Venda', '6', 5, 109200, 21840, '531/52'),
(1252, 9, 2, '2018-07-28 07:41:27', 'Venda', '1', 5, 839.3, 167.86, '719/17'),
(1253, 5, 2, '2018-07-28 07:41:27', 'Venda', '1', 10, 17160, 1716, '719/17'),
(1254, 29, 2, '2018-07-28 07:41:27', 'Venda', '1', 4, 51168, 12792, '719/17'),
(1255, 30, 2, '2018-07-28 07:42:35', 'Venda', '6', 19, 248520, 13080, '645/27'),
(1256, 5, 2, '2018-07-28 07:43:45', 'Venda', '6', 1, 1716, 1716, '869/35'),
(1257, 5, 2, '2018-07-28 07:44:45', 'Venda', '6', 1, 1716, 1716, '869/35'),
(1258, 5, 2, '2018-07-28 07:45:17', 'Venda', '6', 1, 1716, 1716, '869/35'),
(1259, 19, 2, '2018-07-28 07:45:17', 'Venda', '6', 1, 21840, 21840, '869/35'),
(1260, 5, 2, '2018-07-28 07:45:36', 'Venda', '6', 1, 1716, 1716, '869/35'),
(1261, 19, 2, '2018-07-28 07:45:36', 'Venda', '6', 1, 21840, 21840, '869/35'),
(1262, 19, 2, '2018-07-28 08:33:49', 'Venda', '6', 1, 21840, 21840, '894/54'),
(1263, 23, 2, '2018-07-28 08:33:49', 'Venda', '6', 12, 1872, 156, '894/54'),
(1264, 19, 2, '2018-07-28 21:45:20', 'Venda', '1', 10, 218400, 21840, '530/49'),
(1265, 35, 2, '2018-07-28 21:45:20', 'Venda', '1', 40, 291200, 7280, '530/49'),
(1266, 19, 2, '2018-07-28 21:48:33', 'Venda', '1', 10, 218400, 21840, '530/49'),
(1267, 35, 2, '2018-07-28 21:48:33', 'Venda', '1', 40, 291200, 7280, '530/49'),
(1268, 10, 2, '2018-07-28 21:48:33', 'Venda', '1', 1, 250.7, 250.7, '530/49'),
(1269, 19, 2, '2018-07-28 21:48:48', 'Venda', '1', 10, 218400, 21840, '530/49'),
(1270, 35, 2, '2018-07-28 21:48:48', 'Venda', '1', 40, 291200, 7280, '530/49'),
(1271, 10, 2, '2018-07-28 21:48:48', 'Venda', '1', 1, 250.7, 250.7, '530/49'),
(1272, 28, 2, '2018-07-29 15:16:01', 'Venda', '1', 1, 675.8, 675.8, '607/21'),
(1273, 1, 2, '2018-07-29 15:16:01', 'Venda', '1', 10, 168480, 16848, '607/21'),
(1274, 4, 2, '2018-07-29 15:35:11', 'Venda', '1', 6, 16848, 2808, '548/54');
INSERT INTO `transacoes_estoques` (`idtransacoes_estoques`, `Produto_idProduto`, `Pessoal_idPessoal`, `data_trans`, `tipo`, `clienteFornecedor`, `quantidade`, `valor`, `preco`, `nDocumento`) VALUES
(1275, 20, 2, '2018-07-29 15:36:57', 'Venda', '1', 1, 76.3, 76.3, '924/11'),
(1276, 20, 2, '2018-07-29 15:39:41', 'Venda', '1', 1, 76.3, 76.3, '924/11'),
(1277, 20, 2, '2018-07-29 15:40:05', 'Venda', '1', 1, 76.3, 76.3, '924/11'),
(1278, 15, 2, '2018-07-29 15:43:02', 'Venda', '1', 10, 54500, 5450, '579/05'),
(1279, 27, 2, '2018-07-31 08:56:50', 'Venda', '1', 1, 654, 654, '138/24'),
(1280, 27, 2, '2018-07-31 08:58:39', 'Venda', '1', 1, 654, 654, '138/24'),
(1281, 27, 2, '2018-07-31 08:58:56', 'Venda', '1', 1, 654, 654, '138/24'),
(1282, 27, 2, '2018-07-31 08:58:56', 'Venda', '1', 1, 654, 654, '138/24'),
(1283, 27, 2, '2018-07-31 09:10:33', 'Venda', '1', 1, 654, 654, '138/24'),
(1284, 27, 2, '2018-07-31 09:10:33', 'Venda', '1', 1, 654, 654, '138/24'),
(1285, 27, 2, '2018-07-31 09:11:06', 'Venda', '1', 1, 654, 654, '138/24'),
(1286, 27, 2, '2018-07-31 09:11:06', 'Venda', '1', 1, 654, 654, '138/24'),
(1287, 27, 2, '2018-07-31 09:13:16', 'Venda', '1', 1, 654, 654, '138/24'),
(1288, 27, 2, '2018-07-31 09:13:16', 'Venda', '1', 1, 654, 654, '138/24'),
(1289, 27, 2, '2018-07-31 09:14:17', 'Venda', '1', 1, 654, 654, '569/00'),
(1290, 5, 2, '2018-07-31 09:15:28', 'Venda', '1', 1, 1716, 1716, '979/09'),
(1291, 13, 2, '2018-07-31 09:16:21', 'Venda', '1', 1, 130.8, 130.8, '29/48'),
(1292, 13, 2, '2018-07-31 09:54:11', 'Venda', '1', 1, 130.8, 130.8, '29/48'),
(1293, 5, 2, '2018-07-31 09:54:11', 'Venda', '1', 1, 1716, 1716, '29/48'),
(1294, 5, 2, '2018-08-01 07:45:25', 'Venda', '1', 1, 1716, 1716, '505/13'),
(1295, 28, 2, '2018-08-01 14:11:52', 'Venda', '1', 1, 675.8, 675.8, '823/27'),
(1296, 8, 2, '2018-08-01 14:11:52', 'Venda', '1', 1, 644.8, 644.8, '823/27'),
(1297, 30, 2, '2018-08-01 14:11:52', 'Venda', '1', 10, 130800, 13080, '823/27'),
(1298, 17, 2, '2018-08-01 14:17:01', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1299, 17, 2, '2018-08-01 14:17:24', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1300, 17, 2, '2018-08-01 14:29:33', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1301, 17, 2, '2018-08-01 14:31:20', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1302, 28, 2, '2018-08-01 14:31:20', 'Venda', '1', 1, 675.8, 675.8, '853/52'),
(1303, 28, 2, '2018-08-01 14:33:08', 'Venda', '1', 1, 675.8, 675.8, '853/59'),
(1304, 15, 2, '2018-08-01 14:33:09', 'Venda', '1', 1, 5450, 5450, '853/59'),
(1305, 28, 2, '2018-08-01 14:36:40', 'Venda', '1', 1, 675.8, 675.8, '853/59'),
(1306, 15, 2, '2018-08-01 14:36:40', 'Venda', '1', 1, 5450, 5450, '853/59'),
(1307, 1, 2, '2018-08-01 14:36:40', 'Venda', '1', 1, 16848, 16848, '853/59'),
(1308, 1, 2, '2018-08-01 14:38:50', 'Venda', '1', 1, 16848, 16848, '853/80'),
(1309, 17, 2, '2018-08-01 14:45:55', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1310, 28, 2, '2018-08-01 14:45:55', 'Venda', '1', 1, 675.8, 675.8, '853/52'),
(1311, 5, 2, '2018-08-01 14:45:55', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1312, 17, 2, '2018-08-01 14:46:07', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1313, 28, 2, '2018-08-01 14:46:07', 'Venda', '1', 1, 675.8, 675.8, '853/52'),
(1314, 5, 2, '2018-08-01 14:46:07', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1315, 17, 2, '2018-08-01 14:47:53', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1316, 28, 2, '2018-08-01 14:47:53', 'Venda', '1', 1, 675.8, 675.8, '853/52'),
(1317, 5, 2, '2018-08-01 14:47:53', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1318, 17, 2, '2018-08-01 14:59:57', 'Venda', '1', 1, 57200, 57200, '853/52'),
(1319, 28, 2, '2018-08-01 14:59:57', 'Venda', '1', 1, 675.8, 675.8, '853/52'),
(1320, 5, 2, '2018-08-01 14:59:57', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1321, 5, 2, '2018-08-01 14:59:57', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1322, 5, 2, '2018-08-01 15:07:14', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1323, 5, 2, '2018-08-01 15:14:15', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1324, 19, 2, '2018-08-01 15:53:49', 'Venda', '1', 1, 21840, 21840, '853/52'),
(1325, 30, 2, '2018-08-01 15:57:04', 'Venda', '1', 1, 13080, 13080, '853/52'),
(1326, 5, 2, '2018-08-01 15:58:40', 'Venda', '6', 1, 1716, 1716, '853/52'),
(1327, 10, 2, '2018-08-01 16:00:16', 'Venda', '1', 1, 250.7, 250.7, '853/52'),
(1328, 5, 2, '2018-08-01 16:39:22', 'Venda', '1', 1, 1716, 1716, '853/52'),
(1329, 5, 2, '2018-08-01 16:45:42', 'Venda', '1', 1, 1716, 1716, '295/54'),
(1330, 28, 2, '2018-08-01 16:51:01', 'Venda', '1', 6, 4054.8, 675.8, '295/54'),
(1331, 5, 2, '2018-08-01 17:00:36', 'Venda', '6', 1, 1716, 1716, '295/54'),
(1332, 30, 2, '2018-08-01 17:00:36', 'Venda', '6', 1, 13080, 13080, '295/54'),
(1333, 4, 2, '2018-08-01 17:03:57', 'Venda', '6', 1, 2808, 2808, '295/54'),
(1334, 10, 2, '2018-08-01 17:03:57', 'Venda', '6', 1, 250.7, 250.7, '295/54'),
(1335, 30, 2, '2018-08-01 17:03:57', 'Venda', '6', 4, 52320, 13080, '295/54'),
(1336, 19, 2, '2018-08-01 17:05:46', 'Venda', '1', 10, 218400, 21840, '295/54'),
(1337, 30, 2, '2018-08-01 17:05:46', 'Venda', '1', 8, 104640, 13080, '295/54'),
(1338, 5, 2, '2018-08-06 16:06:52', 'Venda', '1', 100, 171600, 1716, '699/20'),
(1339, 1, 2, '2018-08-06 16:06:52', 'Venda', '1', 100, 1684800, 16848, '699/20'),
(1340, 23, 2, '2018-09-17 13:20:36', 'Venda', '1', 1, 156, 156, '144/51'),
(1341, 1, 2, '2018-09-17 13:20:36', 'Venda', '1', 4, 67392, 16848, '144/51'),
(1342, 1, 2, '2018-09-17 15:52:23', 'Venda', '0', 4, 67392, 16848, '89/41'),
(1343, 15, 2, '2018-09-17 15:52:23', 'Venda', '0', 1, 5100, 5100, '89/41'),
(1344, 5, 2, '2018-09-17 15:53:01', 'Venda', '1', 1, 1716, 1716, '89/41'),
(1345, 1, 2, '2018-09-17 16:08:21', 'Venda', '0', 1, 16848, 16848, '825/34'),
(1346, 1, 2, '2018-09-17 16:11:06', 'Venda', '0', 1, 16848, 16848, '825/34'),
(1347, 1, 2, '2018-09-17 16:12:05', 'Venda', '0', 1, 16848, 16848, '825/34'),
(1348, 5, 2, '2018-09-17 16:38:43', 'Compra', '1', 10, 17160, 1716, '843-54'),
(1349, 5, 2, '2018-09-17 16:49:57', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1350, 5, 2, '2018-09-17 16:55:32', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1351, 5, 2, '2018-09-17 17:34:30', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1352, 5, 2, '2018-09-17 17:35:56', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1353, 5, 2, '2018-09-17 17:37:14', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1354, 5, 2, '2018-09-17 17:38:11', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1355, 5, 2, '2018-09-17 17:38:42', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1356, 5, 2, '2018-09-17 17:39:49', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1357, 5, 2, '2018-09-17 17:40:23', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1358, 5, 2, '2018-09-17 17:41:10', 'Compra', '2', 10, 17160, 1716, '843-54'),
(1359, 5, 2, '2018-09-17 17:46:54', 'Compra', '2', 100, 171600, 1716, '843-54'),
(1360, 23, 2, '2019-05-01 14:22:40', 'Venda', '0', 4, 624, 156, '339/52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unidades`
--

CREATE TABLE `unidades` (
  `idunidades` int(10) UNSIGNED NOT NULL,
  `unidade` varchar(255) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `unidades`
--

INSERT INTO `unidades` (`idunidades`, `unidade`) VALUES
(5, 'Laminas'),
(2, 'unidades'),
(3, 'litros'),
(4, 'kilos'),
(6, 'garrafas'),
(7, 'frascos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE `venda` (
  `idVenda` int(10) UNSIGNED NOT NULL,
  `Modo_Pagamento_idModo_Pagamento` int(10) UNSIGNED NOT NULL,
  `Cliente_idCliente` int(10) UNSIGNED NOT NULL,
  `Pessoal_idPessoal` int(10) UNSIGNED NOT NULL,
  `Caixa_idCaixa` int(10) UNSIGNED NOT NULL,
  `dataVenda` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `Factura` varchar(255) COLLATE latin1_general_cs DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `desconto` float DEFAULT NULL,
  `valorLiquido` float DEFAULT NULL,
  `pago` float DEFAULT NULL,
  `diferenca` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `venda`
--

INSERT INTO `venda` (`idVenda`, `Modo_Pagamento_idModo_Pagamento`, `Cliente_idCliente`, `Pessoal_idPessoal`, `Caixa_idCaixa`, `dataVenda`, `Factura`, `valor`, `desconto`, `valorLiquido`, `pago`, `diferenca`) VALUES
(1, 1, 1, 2, 1, '2018-03-23 20:33:21', '673-03', 16200, 0, 16200, 16200, 0),
(2, 1, 1, 2, 1, '2018-03-23 20:33:46', '673-03', 16200, 0, 16200, 16200, 0),
(3, 1, 1, 2, 6, '2018-03-24 17:23:16', '490-40', 48600, 0, 48600, 48600, 0),
(4, 1, 1, 2, 6, '2018-03-24 17:29:03', '935-37', 48600, 0, 48600, 48600, 0),
(5, 1, 1, 2, 6, '2018-03-24 17:33:05', '493-29', 2700, 0, 2700, 2700, 0),
(6, 1, 1, 2, 6, '2018-03-24 17:34:04', '358/05', 2700, 0, 2700, 2700, 0),
(7, 1, 1, 2, 6, '2018-03-24 17:39:31', '358/05', 2700, 0, 2700, 2700, 0),
(8, 1, 1, 2, 6, '2018-03-24 17:40:31', '386-58', 16200, 0, 16200, 16200, 0),
(9, 1, 1, 2, 6, '2018-03-24 17:43:46', '868/31', 16200, 0, 16200, 16200, 0),
(10, 1, 1, 2, 6, '2018-03-24 17:46:14', '406/46', 16200, 0, 16200, 16200, 0),
(11, 1, 1, 2, 6, '2018-03-24 17:51:26', '774/14', 16200, 0, 16200, 16200, 0),
(12, 1, 1, 2, 6, '2018-03-24 17:52:18', '774/14', 16200, 0, 16200, 16200, 0),
(13, 1, 1, 2, 6, '2018-03-24 17:53:05', '774/14', 16200, 0, 16200, 16200, 0),
(14, 1, 1, 2, 6, '2018-03-24 17:53:45', '470/05', 16200, 0, 16200, 16200, 0),
(15, 1, 1, 2, 6, '2018-03-24 17:54:27', '470/05', 16200, 0, 16200, 16200, 0),
(16, 1, 1, 2, 6, '2018-03-24 17:56:27', '985/27', 16200, 0, 16200, 16200, 0),
(17, 1, 1, 2, 6, '2018-03-24 18:14:01', '41/21', 16200, 0, 16200, 16200, 0),
(18, 1, 1, 2, 6, '2018-03-24 18:14:37', '41/21', 16200, 0, 16200, 16200, 0),
(19, 1, 1, 2, 6, '2018-03-24 18:15:00', '41/21', 16200, 0, 16200, 16200, 0),
(20, 1, 1, 2, 8, '2018-04-04 15:16:35', '941/06', 56700, 5, 53865, 53866, -1),
(21, 1, 6, 2, 8, '2018-04-04 15:19:26', '671/19', 40500, 0, 40500, 40500, 0),
(22, 1, 1, 2, 8, '2018-04-16 22:24:37', '432/38', 16200, 0, 16200, 16200, 0),
(23, 1, 1, 2, 8, '2018-04-16 22:25:27', '432/38', 16200, 0, 16200, 16200, 0),
(24, 1, 1, 2, 8, '2018-04-16 22:26:21', '432/38', 16200, 0, 16200, 16200, 0),
(25, 1, 1, 2, 8, '2018-04-16 22:27:46', '432/38', 16200, 0, 16200, 16200, 0),
(26, 1, 1, 2, 8, '2018-04-16 22:28:11', '432/38', 16200, 0, 16200, 16200, 0),
(27, 1, 6, 2, 8, '2018-04-16 22:35:18', '432/38', 16200, 0, 16200, 16200, 0),
(28, 1, 6, 2, 8, '2018-04-19 19:38:24', '432/38', 16200, 0, 16200, 16200, 0),
(29, 1, 1, 2, 8, '2018-04-19 19:40:13', '432/38', 16200, 45, 8910, 8910, 0),
(30, 1, 1, 2, 8, '2018-04-19 19:43:55', '432/38', 16200, 45, 8910, 8910, 0),
(31, 1, 6, 2, 8, '2018-04-19 19:46:27', '432/38', 16200, 0, 16200, 16200, 0),
(32, 1, 6, 2, 8, '2018-04-19 19:47:29', '432/38', 16200, 0, 16200, 16200, 0),
(33, 1, 6, 2, 8, '2018-04-19 19:47:45', '432/38', 16200, 0, 16200, 16200, 0),
(34, 1, 6, 2, 8, '2018-04-19 19:48:15', '432/38', 16200, 0, 16200, 16200, 0),
(35, 1, 6, 2, 8, '2018-04-19 19:48:49', '432/38', 16200, 0, 16200, 16200, 0),
(36, 1, 6, 2, 8, '2018-04-19 19:49:22', '432/38', 16200, 0, 16200, 16200, 0),
(37, 1, 6, 2, 8, '2018-04-19 19:50:03', '432/38', 16200, 0, 16200, 16200, 0),
(38, 1, 6, 2, 8, '2018-04-19 19:50:42', '432/38', 16200, 0, 16200, 16200, 0),
(39, 1, 6, 2, 8, '2018-04-19 19:52:43', '432/38', 16200, 0, 16200, 16200, 0),
(40, 1, 6, 2, 8, '2018-04-19 19:54:52', '432/38', 16200, 0, 16200, 16200, 0),
(41, 1, 6, 2, 8, '2018-04-19 19:56:55', '432/38', 16200, 0, 16200, 16200, 0),
(42, 1, 6, 2, 8, '2018-04-19 19:57:29', '432/38', 16200, 0, 16200, 16200, 0),
(43, 1, 6, 2, 8, '2018-04-19 19:57:50', '432/38', 16200, 0, 16200, 16200, 0),
(44, 1, 6, 2, 8, '2018-04-19 19:58:07', '432/38', 16200, 0, 16200, 16200, 0),
(45, 1, 6, 2, 8, '2018-04-19 19:59:29', '432/38', 16200, 0, 16200, 16200, 0),
(46, 1, 6, 2, 8, '2018-04-19 20:00:28', '432/38', 16200, 0, 16200, 16200, 0),
(47, 1, 6, 2, 8, '2018-04-19 20:00:55', '432/38', 16200, 0, 16200, 16200, 0),
(48, 1, 6, 2, 8, '2018-04-19 20:01:24', '432/38', 16200, 0, 16200, 16200, 0),
(49, 1, 6, 2, 8, '2018-04-19 20:02:21', '432/38', 16200, 0, 16200, 16200, 0),
(50, 1, 6, 2, 8, '2018-04-19 20:02:33', '432/38', 16200, 0, 16200, 16200, 0),
(51, 1, 6, 2, 8, '2018-04-19 20:03:35', '432/38', 16200, 0, 16200, 16200, 0),
(52, 1, 6, 2, 8, '2018-04-19 20:04:24', '432/38', 16200, 0, 16200, 16200, 0),
(53, 1, 6, 2, 8, '2018-04-19 20:05:37', '432/38', 16200, 0, 16200, 16200, 0),
(54, 1, 6, 2, 8, '2018-04-19 20:06:58', '432/38', 16200, 0, 16200, 16200, 0),
(55, 1, 6, 2, 8, '2018-04-19 20:07:17', '432/38', 16200, 0, 16200, 16200, 0),
(56, 1, 6, 2, 8, '2018-04-19 20:07:28', '432/38', 16200, 0, 16200, 16200, 0),
(57, 1, 1, 2, 8, '2018-04-19 20:10:23', '700/29', 2700, 0, 2700, 2700, 0),
(58, 1, 1, 2, 8, '2018-04-19 21:18:56', '608/14', 2700, 0, 2700, 2700, 0),
(59, 1, 1, 2, 8, '2018-04-19 21:19:10', '608/14', 2700, 0, 2700, 2700, 0),
(60, 1, 1, 2, 8, '2018-04-19 21:20:01', '608/14', 2700, 0, 2700, 2700, 0),
(61, 1, 1, 2, 8, '2018-04-19 21:20:27', '608/14', 2700, 0, 2700, 2700, 0),
(62, 1, 1, 2, 8, '2018-04-19 21:20:46', '608/14', 2700, 0, 2700, 2700, 0),
(63, 1, 1, 2, 8, '2018-04-19 21:35:21', '608/14', 162000, 0, 162000, 162000, 0),
(64, 1, 1, 2, 8, '2018-04-19 21:36:53', '608/14', 162000, 0, 162000, 162000, 0),
(65, 1, 1, 2, 8, '2018-04-19 21:37:26', '608/14', 162000, 0, 162000, 162000, 0),
(66, 1, 6, 2, 8, '2018-04-19 21:38:13', '608/14', 275400, 0, 275400, 275400, 0),
(67, 1, 6, 2, 8, '2018-04-19 21:38:58', '608/14', 275400, 0, 275400, 275400, 0),
(68, 1, 6, 2, 8, '2018-04-19 21:40:07', '608/14', 275400, 0, 275400, 275400, 0),
(69, 1, 6, 2, 8, '2018-04-19 21:44:28', '608/14', 27000, 0, 27000, 27000, 0),
(70, 1, 6, 2, 8, '2018-04-19 21:44:45', '608/14', 27000, 0, 27000, 27000, 0),
(71, 1, 6, 2, 8, '2018-04-19 21:59:36', '608/14', 27000, 0, 27000, 27000, 0),
(72, 1, 6, 2, 8, '2018-04-19 22:01:14', '608/14', 24300, 10, 21870, 21870, 0),
(73, 1, 1, 2, 8, '2018-04-22 22:58:16', '577/50', 16200, 0, 16200, 16200, 0),
(74, 1, 6, 2, 8, '2018-04-27 21:36:36', '453/51', 48600, 0, 48600, 48600, 0),
(75, 1, 1, 2, 8, '2018-04-27 21:42:15', '453/51', 16200, 0, 16200, 16200, 0),
(76, 1, 1, 2, 8, '2018-04-27 21:50:22', '453/51', 16200, 0, 16200, 16200, 0),
(77, 1, 1, 2, 8, '2018-04-27 21:50:45', '453/51', 16200, 0, 16200, 16200, 0),
(78, 1, 1, 2, 8, '2018-04-27 21:51:31', '453/51', 145800, 0, 145800, 145800, 0),
(79, 1, 1, 2, 8, '2018-04-27 23:27:47', '453/51', 145800, 0, 145800, 145800, 0),
(80, 1, 1, 2, 9, '2018-05-04 21:24:00', '343/26', 50544, 0, 50544, 50544, 0),
(81, 2, 1, 2, 9, '2018-07-25 08:34:45', '501/58', 18564, 0, 18564, 18564, 0),
(82, 2, 1, 2, 9, '2018-07-26 13:02:20', '294/04', 327000, 0, 327000, 327000, 0),
(83, 1, 6, 4, 9, '2018-07-26 13:29:49', '778/42', 18564, 0, 18564, 26564, -8000),
(84, 1, 1, 2, 9, '2018-07-26 13:37:21', '494/08', 57200, 0, 57200, 52700, 4500),
(85, 1, 1, 2, 9, '2018-07-26 13:38:07', '494/08', 57200, 0, 57200, 57200, 0),
(86, 1, 1, 2, 9, '2018-07-26 13:39:16', '231/07', 280800, 0, 280800, 280800, 0),
(87, 1, 1, 2, 9, '2018-07-27 21:01:40', '486/42', 2071, 0, 2071, 2071, 0),
(88, 1, 1, 2, 9, '2018-07-28 07:17:12', '866/19', 1716, 0, 1716, 1716, 0),
(89, 1, 1, 2, 9, '2018-07-28 07:21:42', '866/19', 1716, 0, 1716, 1716, 0),
(90, 1, 6, 2, 9, '2018-07-28 07:22:35', '866/19', 22515.8, 0, 22515.8, 22515, 0.8),
(91, 1, 6, 2, 9, '2018-07-28 07:39:29', '531/52', 109200, 0, 109200, 109200, 0),
(92, 1, 6, 2, 9, '2018-07-28 07:40:17', '531/52', 109200, 0, 109200, 109200, 0),
(93, 1, 1, 2, 9, '2018-07-28 07:41:27', '719/17', 69167.3, 0, 69167.3, 69167, 0.3),
(94, 1, 6, 2, 9, '2018-07-28 07:42:35', '645/27', 248520, 0, 248520, 248520, 0),
(95, 1, 6, 2, 9, '2018-07-28 07:43:45', '869/35', 1716, 0, 1716, 1716, 0),
(96, 1, 6, 2, 9, '2018-07-28 07:44:45', '869/35', 1716, 0, 1716, 1716, 0),
(97, 1, 6, 2, 9, '2018-07-28 07:45:17', '869/35', 21840, 0, 21840, 21840, 0),
(98, 1, 6, 2, 9, '2018-07-28 07:45:36', '869/35', 21840, 0, 21840, 21840, 0),
(99, 1, 6, 2, 9, '2018-07-28 08:33:49', '894/54', 23712, 0, 23712, 23712, 0),
(100, 1, 1, 2, 9, '2018-07-28 21:45:20', '530/49', 509600, 0, 509600, 509600, 0),
(101, 1, 1, 2, 9, '2018-07-28 21:48:33', '530/49', 250.7, 0, 250.7, 250, 0.7),
(102, 1, 1, 2, 9, '2018-07-28 21:48:48', '530/49', 250.7, 0, 250.7, 250, 0.7),
(103, 1, 1, 2, 19, '2018-07-29 15:16:01', '607/21', 169156, 0, 169156, 169155, 0.8),
(104, 1, 1, 2, 20, '2018-07-29 15:35:11', '548/54', 16848, 0, 16848, 16848, 0),
(105, 1, 1, 2, 20, '2018-07-29 15:40:05', '924/11', 76.3, 0, 76.3, 75, 1.3),
(106, 1, 1, 2, 20, '2018-07-29 15:43:02', '579/05', 54500, 0, 54500, 54500, 0),
(107, 1, 1, 2, 25, '2018-07-31 08:56:50', '138/24', 654, 0, 654, 645, 9),
(108, 1, 1, 2, 25, '2018-07-31 08:58:39', '138/24', 654, 0, 654, 645, 9),
(109, 1, 1, 2, 25, '2018-07-31 08:58:56', '138/24', 654, 0, 654, 654, 0),
(110, 1, 1, 2, 25, '2018-07-31 09:10:33', '138/24', 654, 0, 654, 654, 0),
(111, 1, 1, 2, 25, '2018-07-31 09:11:06', '138/24', 654, 0, 654, 654, 0),
(112, 1, 1, 2, 25, '2018-07-31 09:13:16', '138/24', 654, 0, 654, 654, 0),
(113, 1, 1, 2, 25, '2018-07-31 09:14:17', '569/00', 654, 0, 654, 654, 0),
(114, 1, 1, 2, 25, '2018-07-31 09:15:28', '979/09', 1716, 0, 1716, 1716, 0),
(115, 1, 1, 2, 25, '2018-07-31 09:16:21', '29/48', 130.8, 0, 130.8, 130, 0.8),
(116, 1, 1, 2, 25, '2018-07-31 09:54:11', '29/48', 1716, 0, 1716, 1716, 0),
(117, 2, 1, 2, 25, '2018-08-01 07:45:25', '505/13', 1716, 0, 1716, 1716, 0),
(118, 1, 1, 2, 27, '2018-08-01 14:11:52', '823/27', 132121, 0, 132121, 132120, 0.6),
(119, 1, 1, 2, 27, '2018-08-01 14:17:01', '853/52', 57200, 0, 57200, 57200, 0),
(120, 1, 1, 2, 27, '2018-08-01 14:17:24', '853/52', 57200, 0, 57200, 57200, 0),
(121, 1, 1, 2, 27, '2018-08-01 14:29:33', '853/52', 57200, 0, 57200, 57200, 0),
(122, 1, 1, 2, 27, '2018-08-01 14:31:20', '853/52', 675.8, 0, 675.8, 675, 0.8),
(123, 2, 1, 2, 27, '2018-08-01 14:33:09', '853/59', 6125.8, 0, 6125.8, 6125, 0.8),
(124, 2, 1, 2, 27, '2018-08-01 14:36:40', '853/59', 16848, 0, 16848, 16848, 0),
(125, 1, 1, 2, 27, '2018-08-01 14:38:50', '853/80', 16848, 0, 16848, 16848, 0),
(126, 2, 1, 2, 27, '2018-08-01 14:46:07', '853/52', 1716, 0, 1716, 8900, -7184),
(127, 2, 1, 2, 27, '2018-08-01 14:47:53', '853/52', 0, 0, 0, 0, 0),
(128, 1, 1, 2, 27, '2018-08-01 14:59:57', '853/52', 1716, 0, 1716, 1716, 0),
(129, 1, 1, 2, 27, '2018-08-01 15:07:14', '853/52', 1716, 0, 1716, 1716, 0),
(130, 1, 1, 2, 27, '2018-08-01 15:14:15', '853/52', 1716, 0, 1716, 1716, 0),
(131, 2, 1, 2, 27, '2018-08-01 15:53:49', '853/52', 21840, 0, 21840, 21840, 0),
(132, 1, 1, 2, 27, '2018-08-01 15:57:04', '853/52', 13080, 0, 13080, 13080, 0),
(133, 2, 6, 2, 27, '2018-08-01 15:58:40', '853/52', 1716, 0, 1716, 1716, 0),
(134, 2, 1, 2, 27, '2018-08-01 16:00:16', '853/52', 250.7, 0, 250.7, 250, 0.7),
(135, 1, 1, 2, 27, '2018-08-01 16:39:22', '853/52', 1716, 0, 1716, 1716, 0),
(136, 1, 1, 2, 27, '2018-08-01 16:45:42', '295/54', 1716, 0, 1716, 1716, 0),
(137, 1, 1, 2, 27, '2018-08-01 16:51:01', '295/54', 4054.8, 0, 4054.8, 4054, 0.8),
(138, 2, 6, 2, 27, '2018-08-01 17:00:36', '295/54', 14796, 0, 14796, 14796, 0),
(139, 2, 6, 2, 27, '2018-08-01 17:03:57', '295/54', 55378.7, 0, 55378.7, 55378, 0.7),
(140, 2, 1, 2, 27, '2018-08-01 17:05:46', '295/54', 323040, 0, 323040, 323050, -10),
(141, 1, 1, 2, 27, '2018-08-06 16:06:52', '699/20', 1856400, 0, 1856400, 1856400, 0),
(142, 2, 1, 2, 28, '2018-09-17 13:20:36', '144/51', 67548, 0, 67548, 67453, 95),
(143, 1, 0, 2, 28, '2018-09-17 15:52:23', '89/41', 72492, 0, 72492, 72492, 0),
(144, 1, 1, 2, 28, '2018-09-17 15:53:01', '89/41', 1716, 0, 1716, 1716, 0),
(145, 2, 0, 2, 28, '2018-09-17 16:08:21', '825/34', 16848, 0, 16848, 16848, 0),
(146, 1, 0, 2, 28, '2018-09-17 16:11:06', '825/34', 16848, 0, 16848, 16848, 0),
(147, 1, 0, 2, 28, '2018-09-17 16:12:05', '825/34', 16848, 0, 16848, 16848, 0),
(148, 2, 0, 2, 28, '2019-05-01 14:22:40', '339/52', 624, 0, 624, 650, -26);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_pend`
--

CREATE TABLE `vendas_pend` (
  `idVendas_Pend` int(10) UNSIGNED NOT NULL,
  `nFactura` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `Qty` int(10) UNSIGNED DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `precUni` float DEFAULT NULL,
  `ipc` float NOT NULL,
  `date` date DEFAULT NULL,
  `desconto` float DEFAULT NULL,
  `status_2` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `DesProduto` varchar(100) COLLATE latin1_general_cs DEFAULT NULL,
  `idProduto` int(10) UNSIGNED DEFAULT NULL,
  `tipo` varchar(30) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Extraindo dados da tabela `vendas_pend`
--

INSERT INTO `vendas_pend` (`idVendas_Pend`, `nFactura`, `Qty`, `valor`, `precUni`, `ipc`, `date`, `desconto`, `status_2`, `DesProduto`, `idProduto`, `tipo`) VALUES
(92, '937-27', 1, 1716, 1716, 0, '2018-07-24', 0, 'PEND', 'Fluditec-Xarope de tosse', 5, 'entrada'),
(5, '967-18', 1, 2700, 2700, 0, '2018-03-18', 0, 'pago', 'papas de soja', 4, 'saida'),
(6, '393-27', 4, 64800, 16200, 0, '2018-03-18', 0, 'PEND', 'Teclados ', 1, 'saida'),
(7, '393-27', 1, 2700, 2700, 0, '2018-03-18', 0, 'PEND', 'papas de soja', 4, 'saida'),
(11, '173-40', 3, 48600, 16200, 0, '2018-03-23', 0, 'pago', 'Teclados ', 1, 'saida'),
(10, '568-54', 1, 16200, 16200, 0, '2018-03-19', 0, 'pago', 'Teclados ', 1, 'saida'),
(12, '173-40', 1, 16200, 16200, 0, '2018-03-23', 0, 'pago', 'Teclados ', 1, 'saida'),
(13, '673-03', 1, 16200, 16200, 0, '2018-03-23', 0, 'pago', 'Teclados ', 1, 'saida'),
(14, '490-40', 3, 48600, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(15, '490-40', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(16, '935-37', 3, 48600, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(17, '935-37', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(18, '493-29', 1, 2700, 2700, 0, '2018-03-24', 0, 'pago', 'papas de soja', 4, 'saida'),
(19, '358/05', 1, 2700, 2700, 0, '2018-03-24', 0, 'pago', 'papas de soja', 4, 'saida'),
(20, '386-58', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(21, '868/31', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(22, '406/46', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(23, '774/14', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(24, '470/05', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(25, '985/27', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(26, '416/27', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(27, '41/21', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(28, '41/21', 1, 16200, 16200, 0, '2018-03-24', 0, 'pago', 'Teclados ', 1, 'saida'),
(32, '990-10', 10, 162000, 16200, 0, '2018-03-26', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(33, '531-18', 3, 48600, 16200, 0, '2018-03-26', 0, 'PEND', 'Teclados ', 1, 'saida'),
(34, '941/06', 3, 48600, 16200, 0, '2018-04-04', 0, 'pago', 'Teclados ', 1, 'saida'),
(35, '941/06', 3, 8100, 2700, 0, '2018-04-04', 0, 'pago', 'papas de soja', 4, 'saida'),
(36, '671/19', 1, 16200, 16200, 0, '2018-04-04', 0, 'pago', 'Teclados ', 1, 'saida'),
(37, '671/19', 1, 16200, 16200, 0, '2018-04-04', 0, 'pago', 'Teclados ', 1, 'saida'),
(38, '671/19', 1, 2700, 2700, 0, '2018-04-04', 0, 'pago', 'papas de soja', 4, 'saida'),
(39, '671/19', 1, 2700, 2700, 0, '2018-04-04', 0, 'pago', 'papas de soja', 4, 'saida'),
(40, '671/19', 1, 2700, 2700, 0, '2018-04-04', 0, 'pago', 'papas de soja', 4, 'saida'),
(41, '467-12', 10, 162000, 16200, 0, '2018-04-04', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(42, '432/38', 1, 16200, 16200, 0, '2018-04-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(43, '432/38', 1, 16200, 16200, 0, '2018-04-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(44, '432/38', 1, 16200, 16200, 0, '2018-04-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(45, '432/38', 1, 16200, 16200, 0, '2018-04-19', 0, 'pago', 'Teclados ', 1, 'saida'),
(46, '432/38', 1, 16200, 16200, 0, '2018-04-19', 0, 'pago', 'Teclados ', 1, 'saida'),
(47, '432/38', 1, 16200, 16200, 0, '2018-04-19', 0, 'pago', 'Teclados ', 1, 'saida'),
(48, '700/29', 1, 2700, 2700, 0, '2018-04-19', 0, 'pago', 'papas de soja', 4, 'saida'),
(49, '608/14', 1, 2700, 2700, 0, '2018-04-20', 0, 'pago', 'papas de soja', 4, 'saida'),
(50, '608/14', 10, 162000, 16200, 0, '2018-04-20', 0, 'pago', 'Teclados ', 1, 'saida'),
(51, '608/14', 17, 275400, 16200, 0, '2018-04-20', 0, 'pago', 'Teclados ', 1, 'saida'),
(52, '608/14', 2, 32400, 16200, 0, '2018-04-20', 0, 'pago', 'Teclados ', 1, 'saida'),
(53, '608/14', 10, 27000, 2700, 0, '2018-04-20', 0, 'pago', 'papas de soja', 4, 'saida'),
(54, '608/14', 4, 10800, 2700, 0, '2018-04-20', 0, 'pago', 'papas de soja', 4, 'saida'),
(55, '608/14', 9, 24300, 2700, 0, '2018-04-20', 0, 'pago', 'papas de soja', 4, 'saida'),
(56, '370-22', 1, 16200, 16200, 0, '2018-04-21', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(57, '806/32', 4, 64800, 16200, 0, '2018-04-22', 0, 'PEND', 'Teclados ', 1, 'saida'),
(58, '473-57', 1, 16200, 16200, 0, '2018-04-22', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(59, '473-57', 1, 2700, 2700, 0, '2018-04-22', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(60, '473-57', 10, 162000, 16200, 0, '2018-04-22', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(61, '473-57', 6, 16200, 2700, 0, '2018-04-22', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(62, '577/50', 1, 16200, 16200, 0, '2018-04-23', 0, 'pago', 'Teclados ', 1, 'saida'),
(63, '453/51', 3, 48600, 16200, 0, '2018-04-28', 0, 'pago', 'Teclados ', 1, 'saida'),
(64, '453/51', 1, 16200, 16200, 0, '2018-04-28', 0, 'pago', 'Teclados ', 1, 'saida'),
(65, '453/51', 9, 145800, 16200, 0, '2018-04-28', 0, 'pago', 'Teclados ', 1, 'saida'),
(66, '447/47', 46, 745200, 16200, 0, '2018-04-29', 0, 'pago', 'Teclados ', 1, 'saida'),
(67, '882-11', 10, 162000, 16200, 0, '2018-04-29', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(68, '957/07', 10, 27000, 2700, 0, '2018-04-29', 0, 'PEND', 'papas de soja', 4, 'saida'),
(69, '635-53', 100, 270000, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(70, '71/06', 1, 16200, 16200, 0, '2018-04-29', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(71, '71/06', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(72, '71/06', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(73, '589/12', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(74, '589/18', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(75, '345-57', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(76, '345-57', 1, 16200, 16200, 0, '2018-04-29', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(77, '494/41', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(78, '494/41', 1, 2700, 2700, 0, '2018-04-29', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(79, '880-59', 1, 2700, 2700, 0, '2018-05-01', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(80, '880-59', 2, 32400, 16200, 0, '2018-05-01', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(81, '880-59', 4, 10800, 2700, 0, '2018-05-01', 0, 'fechado', 'papas de soja', 4, 'entrada'),
(82, '880-59', 1, 16200, 16200, 0, '2018-05-01', 0, 'fechado', 'Teclados ', 1, 'entrada'),
(83, '271/42', 1, 1716, 1716, 0, '2018-05-01', 0, 'PEND', 'Fluditec-Xarope de tosse', 5, 'saida'),
(84, '343/26', 3, 50544, 16848, 0, '2018-05-05', 0, 'pago', 'Teclados ', 1, 'saida'),
(85, '384-57', 1, 486, 486, 0, '2018-07-21', 0, 'PEND', 'Fluditec-Xarope de tosse', 18, 'entrada'),
(86, '534-49', 1, 486, 486, 0, '2018-07-21', 0, 'PEND', 'Fluditec-Xarope de tosse', 18, 'acerto'),
(87, '418-24', 1, 486, 486, 0, '2018-07-21', 0, 'PEND', 'Fluditec-Xarope de tosse', 18, 'acerto'),
(88, '418-24', 1, 486, 486, 0, '2018-07-21', 0, 'PEND', 'Fluditec-Xarope de tosse', 18, 'acerto'),
(91, '1000-56', 1, 1716, 1716, 0, '2018-07-24', 0, 'PEND', 'Fluditec-Xarope de tosse', 5, 'entrada'),
(96, '582/42', 1, 16848, 16848, 673.92, '2018-07-25', 0, 'PEND', 'Teclados ', 1, 'saida'),
(97, '501/58', 1, 16848, 16848, 673.92, '2018-07-25', 0, 'pago', 'Teclados ', 1, 'saida'),
(98, '501/58', 1, 1716, 1716, 68.64, '2018-07-25', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(99, '294/04', 100, 327000, 3270, 29430, '2018-07-26', 0, 'pago', 'Smartphone', 14, 'saida'),
(100, '494/08', 1, 57200, 57200, 2288, '2018-07-26', 0, 'pago', 'IMPRESSORA OFFICEJET PRO 8210', 17, 'saida'),
(101, '778/42', 1, 16848, 16848, 673.92, '2018-07-26', 0, 'pago', 'Teclados ', 1, 'saida'),
(102, '778/42', 1, 1716, 1716, 68.64, '2018-07-26', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(103, '494/08', 1, 57200, 57200, 2288, '2018-07-26', 0, 'pago', 'IMPRESSORA OFFICEJET PRO 8210', 17, 'saida'),
(104, '231/07', 100, 280800, 2808, 11232, '2018-07-26', 0, 'pago', 'papas de soja', 4, 'saida'),
(105, '486/42', 1, 675.8, 675.8, 60.822, '2018-07-27', 0, 'pago', 'Leite fresco', 28, 'saida'),
(106, '486/42', 1, 610.4, 610.4, 54.936, '2018-07-27', 0, 'pago', 'Carregador de Computador', 12, 'saida'),
(107, '486/42', 6, 784.8, 130.8, 70.632, '2018-07-27', 0, 'pago', 'Capa de Celular', 13, 'saida'),
(108, '866/19', 1, 1716, 1716, 68.64, '2018-07-28', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(109, '866/19', 1, 21840, 21840, 873.6, '2018-07-28', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(110, '866/19', 1, 675.8, 675.8, 60.822, '2018-07-28', 0, 'pago', 'Leite fresco', 28, 'saida'),
(111, '531/52', 5, 109200, 21840, 4368, '2018-07-28', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(112, '719/17', 5, 839.3, 167.86, 75.537, '2018-07-28', 0, 'pago', 'Tapete Gamer', 9, 'saida'),
(113, '719/17', 10, 17160, 1716, 686.4, '2018-07-28', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(114, '719/17', 4, 51168, 12792, 2046.72, '2018-07-28', 0, 'pago', 'Cabo HDMI 3M', 29, 'saida'),
(115, '645/27', 19, 248520, 13080, 22366.8, '2018-07-28', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(116, '869/35', 1, 1716, 1716, 68.64, '2018-07-28', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(117, '869/35', 1, 21840, 21840, 873.6, '2018-07-28', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(118, '894/54', 1, 21840, 21840, 873.6, '2018-07-28', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(119, '894/54', 12, 1872, 156, 74.88, '2018-07-28', 0, 'pago', 'Banana PÃ£o ', 23, 'saida'),
(120, '530/49', 10, 218400, 21840, 8736, '2018-07-28', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(121, '530/49', 40, 291200, 7280, 11648, '2018-07-28', 0, 'pago', 'APC CARREGADOR SMARTPHONE + TABLET USB 1A AZUL', 35, 'saida'),
(122, '530/49', 1, 250.7, 250.7, 22.563, '2018-07-28', 0, 'pago', 'Papel A4', 10, 'saida'),
(123, '607/21', 1, 675.8, 675.8, 60.822, '2018-07-29', 0, 'pago', 'Leite fresco', 28, 'saida'),
(124, '607/21', 10, 168480, 16848, 6739.2, '2018-07-29', 0, 'pago', 'Teclados ', 1, 'saida'),
(125, '548/54', 6, 16848, 2808, 673.92, '2018-07-29', 0, 'pago', 'papas de soja', 4, 'saida'),
(126, '924/11', 1, 76.3, 76.3, 6.867, '2018-07-29', 0, 'pago', 'Paracetamol', 20, 'saida'),
(127, '579/05', 10, 54500, 5450, 4905, '2018-07-29', 0, 'pago', 'Iphone 5s', 15, 'saida'),
(128, '138/24', 1, 654, 654, 58.86, '2018-07-31', 0, 'pago', 'CafÃ© expresso', 27, 'saida'),
(129, '138/24', 1, 654, 654, 58.86, '2018-07-31', 0, 'pago', 'CafÃ© expresso', 27, 'saida'),
(130, '569/00', 1, 654, 654, 58.86, '2018-07-31', 0, 'pago', 'CafÃ© expresso', 27, 'saida'),
(131, '979/09', 1, 1716, 1716, 68.64, '2018-07-31', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(132, '29/48', 1, 130.8, 130.8, 11.772, '2018-07-31', 0, 'pago', 'Capa de Celular', 13, 'saida'),
(133, '29/48', 1, 1716, 1716, 68.64, '2018-07-31', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(134, '505/13', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(135, '823/27', 1, 675.8, 675.8, 60.822, '2018-08-01', 0, 'pago', 'Leite fresco', 28, 'saida'),
(136, '823/27', 1, 644.8, 644.8, 25.792, '2018-08-01', 0, 'pago', 'Monitores Gamer', 8, 'saida'),
(137, '823/27', 10, 130800, 13080, 11772, '2018-08-01', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(138, '853/52', 1, 57200, 57200, 2288, '2018-08-01', 0, 'pago', 'IMPRESSORA OFFICEJET PRO 8210', 17, 'saida'),
(139, '853/52', 1, 675.8, 675.8, 60.822, '2018-08-01', 0, 'pago', 'Leite fresco', 28, 'saida'),
(140, '853/59', 1, 675.8, 675.8, 60.822, '2018-08-01', 0, 'pago', 'Leite fresco', 28, 'saida'),
(142, '853/59', 1, 5450, 5450, 490.5, '2018-08-01', 0, 'pago', 'Iphone 5s', 15, 'saida'),
(143, '853/59', 1, 16848, 16848, 673.92, '2018-08-01', 0, 'pago', 'Teclados ', 1, 'saida'),
(144, '853/80', 1, 16848, 16848, 673.92, '2018-08-01', 0, 'pago', 'Teclados ', 1, 'saida'),
(145, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(146, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(147, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(148, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(149, '853/52', 1, 21840, 21840, 873.6, '2018-08-01', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(150, '853/52', 1, 13080, 13080, 1177.2, '2018-08-01', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(151, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(152, '853/52', 1, 250.7, 250.7, 22.563, '2018-08-01', 0, 'pago', 'Papel A4', 10, 'saida'),
(153, '853/52', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(154, '295/54', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(155, '295/54', 6, 4054.8, 675.8, 364.932, '2018-08-01', 0, 'pago', 'Leite fresco', 28, 'saida'),
(156, '295/54', 1, 1716, 1716, 68.64, '2018-08-01', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(157, '295/54', 1, 13080, 13080, 1177.2, '2018-08-01', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(158, '295/54', 1, 2808, 2808, 112.32, '2018-08-01', 0, 'pago', 'papas de soja', 4, 'saida'),
(159, '295/54', 1, 250.7, 250.7, 22.563, '2018-08-01', 0, 'pago', 'Papel A4', 10, 'saida'),
(160, '295/54', 4, 52320, 13080, 4708.8, '2018-08-01', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(161, '295/54', 10, 218400, 21840, 8736, '2018-08-01', 0, 'pago', 'SONY JOGO PS4 CALL OF DUTY WWII', 19, 'saida'),
(162, '295/54', 8, 104640, 13080, 9417.6, '2018-08-01', 0, 'pago', 'Cadeira Gammer Vermelha', 30, 'saida'),
(163, '699/20', 100, 171600, 1716, 6864, '2018-08-06', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(164, '699/20', 100, 1684800, 16848, 67392, '2018-08-06', 0, 'pago', 'Teclados ', 1, 'saida'),
(167, '144/51', 1, 156, 156, 6.24, '2018-09-17', 0, 'pago', 'Banana PÃ£o ', 23, 'saida'),
(169, '144/51', 4, 67392, 16848, 2695.68, '2018-09-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(170, '89/41', 4, 67392, 16848, 2695.68, '2018-09-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(171, '89/41', 1, 5100, 5100, 102, '2018-09-17', 0, 'pago', 'Iphone 5s', 15, 'saida'),
(172, '89/41', 1, 1716, 1716, 68.64, '2018-09-17', 0, 'pago', 'Fluditec-Xarope de tosse', 5, 'saida'),
(173, '825/34', 1, 16848, 16848, 673.92, '2018-09-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(174, '825/34', 1, 16848, 16848, 673.92, '2018-09-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(175, '825/34', 1, 16848, 16848, 673.92, '2018-09-17', 0, 'pago', 'Teclados ', 1, 'saida'),
(180, '843-54', 100, 171600, 1716, 2, '2018-09-17', 0, 'fechado', 'Fluditec-Xarope de tosse', 5, 'entrada'),
(181, '339/52', 4, 624, 156, 24.96, '2019-05-01', 0, 'pago', 'Banana PÃ£o ', 23, 'saida');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `caixa`
--
ALTER TABLE `caixa`
  ADD PRIMARY KEY (`idCaixa`,`Pessoal_idPessoal`),
  ADD KEY `Caixa_FKIndex1` (`Pessoal_idPessoal`);

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Índices para tabela `contasbancarias`
--
ALTER TABLE `contasbancarias`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `divida`
--
ALTER TABLE `divida`
  ADD PRIMARY KEY (`iddivida`,`Venda_Cliente_idCliente`,`Venda_Modo_Pagamento_idModo_Pagamento`,`Venda_idVenda`,`Cliente_idCliente`),
  ADD KEY `divida_FKIndex1` (`Venda_idVenda`,`Venda_Modo_Pagamento_idModo_Pagamento`,`Venda_Cliente_idCliente`),
  ADD KEY `divida_FKIndex2` (`Cliente_idCliente`);

--
-- Índices para tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Índices para tabela `encomendas`
--
ALTER TABLE `encomendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cliente` (`cliente`);

--
-- Índices para tabela `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`idEntrada`,`Produto_idProduto`,`Produto_familia_idfamilia`,`Modo_Pagamento_idModo_Pagamento`,`Produto_unidades_idunidades`),
  ADD KEY `Compra_FKIndex2` (`Produto_idProduto`,`Produto_familia_idfamilia`,`Produto_unidades_idunidades`),
  ADD KEY `entrada_FKIndex3` (`Modo_Pagamento_idModo_Pagamento`);

--
-- Índices para tabela `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`idfamilia`);

--
-- Índices para tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD PRIMARY KEY (`idfornecedores`);

--
-- Índices para tabela `ipc`
--
ALTER TABLE `ipc`
  ADD PRIMARY KEY (`idIpc`);

--
-- Índices para tabela `ipcvalor`
--
ALTER TABLE `ipcvalor`
  ADD PRIMARY KEY (`idipcValor`);

--
-- Índices para tabela `itens_vendas`
--
ALTER TABLE `itens_vendas`
  ADD PRIMARY KEY (`iditens_vendas`,`Venda_Cliente_idCliente`,`Venda_Modo_Pagamento_idModo_Pagamento`,`Venda_idVenda`,`Produto_idProduto`,`Produto_familia_idfamilia`,`Produto_unidades_idunidades`),
  ADD KEY `itens_vendas_FKIndex1` (`Venda_idVenda`,`Venda_Modo_Pagamento_idModo_Pagamento`,`Venda_Cliente_idCliente`),
  ADD KEY `itens_vendas_FKIndex2` (`Produto_idProduto`,`Produto_familia_idfamilia`,`Produto_unidades_idunidades`);

--
-- Índices para tabela `linhasacertos`
--
ALTER TABLE `linhasacertos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idProduto` (`idProduto`);

--
-- Índices para tabela `modo_pagamento`
--
ALTER TABLE `modo_pagamento`
  ADD PRIMARY KEY (`idModo_Pagamento`);

--
-- Índices para tabela `perfis`
--
ALTER TABLE `perfis`
  ADD PRIMARY KEY (`Perfil`);

--
-- Índices para tabela `pessoal`
--
ALTER TABLE `pessoal`
  ADD PRIMARY KEY (`idPessoal`);

--
-- Índices para tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idProduto`,`familia_idfamilia`,`unidades_idunidades`),
  ADD KEY `Produto_FKIndex1` (`familia_idfamilia`),
  ADD KEY `Produto_FKIndex2` (`unidades_idunidades`),
  ADD KEY `ipc` (`ipc`);

--
-- Índices para tabela `produto_imagens`
--
ALTER TABLE `produto_imagens`
  ADD KEY `produto` (`produto`);

--
-- Índices para tabela `transacaocaixa`
--
ALTER TABLE `transacaocaixa`
  ADD PRIMARY KEY (`idtransacaoCaixa`,`Caixa_idCaixa`),
  ADD KEY `transacaoCaixa_FKIndex1` (`Caixa_idCaixa`);

--
-- Índices para tabela `transacoes_estoques`
--
ALTER TABLE `transacoes_estoques`
  ADD PRIMARY KEY (`idtransacoes_estoques`,`Produto_idProduto`,`Pessoal_idPessoal`),
  ADD KEY `transacoes_estoques_FKIndex1` (`Produto_idProduto`),
  ADD KEY `transacoes_estoques_FKIndex2` (`Pessoal_idPessoal`);

--
-- Índices para tabela `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`idunidades`);

--
-- Índices para tabela `venda`
--
ALTER TABLE `venda`
  ADD PRIMARY KEY (`idVenda`,`Modo_Pagamento_idModo_Pagamento`,`Cliente_idCliente`),
  ADD KEY `Venda_FKIndex1` (`Modo_Pagamento_idModo_Pagamento`),
  ADD KEY `Venda_FKIndex3` (`Cliente_idCliente`),
  ADD KEY `Venda_FKIndex4` (`Pessoal_idPessoal`);

--
-- Índices para tabela `vendas_pend`
--
ALTER TABLE `vendas_pend`
  ADD PRIMARY KEY (`idVendas_Pend`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `caixa`
--
ALTER TABLE `caixa`
  MODIFY `idCaixa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de tabela `contasbancarias`
--
ALTER TABLE `contasbancarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `divida`
--
ALTER TABLE `divida`
  MODIFY `iddivida` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `encomendas`
--
ALTER TABLE `encomendas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `entrada`
--
ALTER TABLE `entrada`
  MODIFY `idEntrada` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `familia`
--
ALTER TABLE `familia`
  MODIFY `idfamilia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  MODIFY `idfornecedores` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `ipc`
--
ALTER TABLE `ipc`
  MODIFY `idIpc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `ipcvalor`
--
ALTER TABLE `ipcvalor`
  MODIFY `idipcValor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de tabela `itens_vendas`
--
ALTER TABLE `itens_vendas`
  MODIFY `iditens_vendas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `linhasacertos`
--
ALTER TABLE `linhasacertos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `modo_pagamento`
--
ALTER TABLE `modo_pagamento`
  MODIFY `idModo_Pagamento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `pessoal`
--
ALTER TABLE `pessoal`
  MODIFY `idPessoal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `produto`
--
ALTER TABLE `produto`
  MODIFY `idProduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT de tabela `transacaocaixa`
--
ALTER TABLE `transacaocaixa`
  MODIFY `idtransacaoCaixa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de tabela `transacoes_estoques`
--
ALTER TABLE `transacoes_estoques`
  MODIFY `idtransacoes_estoques` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1361;

--
-- AUTO_INCREMENT de tabela `unidades`
--
ALTER TABLE `unidades`
  MODIFY `idunidades` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `venda`
--
ALTER TABLE `venda`
  MODIFY `idVenda` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT de tabela `vendas_pend`
--
ALTER TABLE `vendas_pend`
  MODIFY `idVendas_Pend` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `encomendas`
--
ALTER TABLE `encomendas`
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`idCliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
