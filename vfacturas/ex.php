<?php
// (c) Xavier Nicolay
// Exemple de g�n�ration de devis/facture PDF
require('dadosvFactura.php');
require('invoice.php');
$empresa="";
$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->AliasNbPages();
$pdf->addSociete($empresa,
                  $EmpresaEndereco."\n" .
                  $EmpresaTelefone."\n" .
                  "NIF : ".$EmpresaNIB."");
$pdf->fact_dev( "V�Factura ",$fact);
$pdf->temporaire("");
$pdf->addDate( date('d/m/Y'));
$pdf->addClient($entidade);
$pdf->addPageNumber("1");
$pdf->addClientAdresse("Fornecedor\n".$cliente."\n".$endereco."\n".$telefone."\n".$email);
$pdf->addReglement('');
$pdf->addEcheance(date('d-m-Y'));
$pdf->addNumTVA("FR888777666");
//$pdf->addReference("Devis ... du ....");
$pdf->SetFont( "Arial", "",10);
$cols=array( "Refer�ncia"    => 23,
             "Designa��o"  => 78,
             "Quantidade"     => 22,
             "Pre�o Un."      => 26,
             "Valor" => 30/*,
             "IPC"          => 11*/ );
$pdf->addCols($cols);
$cols=array( "Refer�ncia"    => "C",
             "Designa��o"  => "L",
             "Quantidade"     => "C",
             "Pre�o Un."      => "R",
             "Valor" => "R"/*,
             "IPC"          => "C" */);
$pdf->addLineFormat( $cols);
$pdf->addLineFormat($cols);
$y    = 109;


for ($i=0; $i <count($produtt); $i++) { 
  $line = array( "Refer�ncia"    => $idProdutoo[$i],
                 "Designa��o"  => utf8_decode($produtt[$i]),
                 "Quantidade"     => $quantid[$i],
                 "Pre�o Un."      => number_format($prect[$i],2,',',' '),
                 "Valor" => number_format($prect[$i]*$quantid[$i],2,',',' ')
                  );
  $size = $pdf->addLine( $y, $line );
  $y   += $size + 2;

}

$tot_prods = array( array ( "px_unit" => 600, "qte" => 1, "tva" => 1 ),
                    array ( "px_unit" =>  10, "qte" => 1, "tva" => 1 ));
$tab_tva = array( "1"       => 19.6,
                  "2"       => 5.5);
$params  = array( "RemiseGlobale" => 1,
                      "remise_tva"     => 1,       // {la remise s'applique sur ce code TVA}
                      "remise"         => 0,       // {montant de la remise}
                      "remise_percent" => 10,      // {pourcentage de remise sur ce montant de TVA}
                  "FraisPort"     => 1,
                      "portTTC"        => 10,      // montant des frais de ports TTC
                                                   // par defaut la TVA = 19.6 %
                      "portHT"         => 0,       // montant des frais de ports HT
                      "portTVA"        => 19.6,    // valeur de la TVA a appliquer sur le montant HT
                  "AccompteExige" => 1,
                      "accompte"         => 0,     // montant de l'acompte (TTC)
                      "accompte_percent" => 15,    // pourcentage d'acompte (TTC)
                  "Remarque" => "Avec un acompte, svp..." );

//$pdf->addTVAs( $params, $tab_tva, $tot_prods);
$pdf->addCadreEurosFrancs($total,0);
$pdf->Codabar(10,255,$fact);

$pdf->Output();
?>
