<?php
//die("testando");
	require("fpdf.php");

	class PDF extends FPDF
{
	/*function Header()
	{
	    $this->SetFont('Arial','B',15);
	    $this->Cell(0,10,'Isso é o cabecalho',1);
	}*/

	function Footer()
	{
		$this->SetY(-20);
	  	$this->SetFont('Arial','B',8);
	  	//$this->Line(0, 250, 250, 0);
	    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}
}

	//A4 width:219mm
	//default margin:10mm each side
	//writable horizontal:219-(10*2)=189mm

	$pdf= new PDF('p','mm','A4');
	

	$pdf->Addpage();
	//define font to arial, bold, 14pt
	$pdf->SetFont('Arial','B',14);

	//cell(width,height, text,border,end line, [align])
	//variavel a apresentar
	$str="Vª FACTURA";
	$data=date('d-m-Y');
	$hora=date('H:m:s');
	$height=5;
	$largo=130;
	$curto=59;
	$str = utf8_decode($str);
	$pdf->Cell(130,2,'SEVEN-Tecnologias',0,0);
	$pdf->Cell(59,$height,$str,0,1);

	//define font to arial, regular, 12pt
	$pdf->SetFont('Arial','',11);
	$pdf->Cell(130,$height,utf8_decode('Rua Principal da EDEL'),0,0);
	$pdf->Cell(59,$height,utf8_decode(''),0,1);
	$pdf->Cell(130,$height,utf8_decode('Camama, Luanda-Angola'),0,0);
	$pdf->Cell(25,$height,utf8_decode('Vª Factura #'),0,0);
	$pdf->Cell(34,$height,utf8_decode('[123456789]'),0,1);//final da linha
	$pdf->Cell(130,$height,utf8_decode('telefone+244 925 556 373'),0,0);
	$pdf->Cell(25,$height,utf8_decode('Data'),0,0);
	$pdf->Cell(34,$height,$data,0,1);//final da linha
	$pdf->Cell(130,$height,utf8_decode('Email:gerla@seven.com'),0,0);
	$pdf->Cell(25,$height,utf8_decode('Hora'),0,0);
	$pdf->Cell(34,$height,$hora,0,1);//final da linha
	
	//adicionar celula vazia
	$pdf->Cell(189,10,'',0,1);

	//Dados do fornecedor
	$pdf->Cell(100,$height,utf8_decode('Cliente'),0,1);
	$pdf->Cell(10,$height,'',0,0);
	$pdf->Cell(90,$height,utf8_decode('[Nome]'),0,1);
	$pdf->Cell(10,$height,'',0,0);
	$pdf->Cell(90,$height,utf8_decode('[Empresa]'),0,1);
	$pdf->Cell(10,$height,'',0,0);
	$pdf->Cell(90,$height,utf8_decode('[Endereço]'),0,1);
	$pdf->Cell(10,$height,'',0,0);
	$pdf->Cell(90,$height,utf8_decode('[Telefone]'),0,1);
	$pdf->Cell(10,$height,'',0,0);
	$pdf->Cell(90,$height,utf8_decode('[Email]'),0,1);

	//adicionar celula vazia
	$pdf->Cell(189,10,'',0,1);

	//itens da factura
	//altera a fonte 
	$pdf->SetFont('arial','B','12');
	$pdf->Cell(100,$height,utf8_decode('Produto'),1,0);
	$pdf->Cell(30,$height,utf8_decode('Prec.Un'),1,0);
	$pdf->Cell(20,$height,utf8_decode('Qty'),1,0);
	$pdf->Cell(39,$height,utf8_decode('Valor'),1,1);

	$pdf->SetFont('arial','','12');
	

	$linha=150;
	for ($i=0; $i <$linha ; $i++) { 
		$pdf->Cell(100,$height,utf8_decode('Teclado computador'),1,0);
		$pdf->Cell(30,$height,utf8_decode('1 200,00'),1,0,'R');
		$pdf->Cell(20,$height,utf8_decode('2'),1,0,'C');
		$pdf->Cell(39,$height,utf8_decode('2 400,00'),1,1,'R');

	}
	$pdf->Addpage();


	$pdf->Output();


?>