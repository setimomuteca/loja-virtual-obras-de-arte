<?php
    require_once("iuda_Shop.php");
    $objConexao = new conexao();
    $conexao=$objConexao->conectar();	
    $operacoes= new Operacao();
	
	if(isset($_GET["idDoc"]))
	{
		$sql = "SELECT p.Desigacao, f.familia, c.quantidade AS qtdCarrinho, p.quantidade AS qtdStock, c.preco \n"
				. "FROM carrinho c \n"
				. "INNER JOIN produto p ON c.produto=p.idProduto \n"
				. "INNER JOIN familia f ON p.familia_idfamilia = f.idfamilia\n"
				. "WHERE c.DocId=".$_GET["idDoc"];
					
		$obj=$operacoes->select($sql,$conexao);	
		$id = $_GET["idDoc"];
	}
	else
	{
		header("location:encomendas.php");
	}
	
    
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>Encomendas</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <style type="text/css">
        tr:nth-child(even) {background-color: #DDD3D3}
    </style>
</head>
    <body class="disable_transitions sidebar_main_open sidebar_main_swipe">
        <!-- main header -->
        <?php
            require("header.php");
        ?>
        <!-- main header end -->
        <!-- main sidebar -->
        <?php
            if($_SESSION['previlegio']=='Administrador')
            {
                require_once('menus\menuAdmin.php');
            }

            if($_SESSION['previlegio']=='Gvnd')
            {
                require_once('menus\menuGerenteVendas.php');
            }

            if($_SESSION['previlegio']=='Gstck')
            {
                require_once('menus\menuGerenteStock.php');
            }
        ?>
        <!-- main sidebar end -->

        <div id="page_content">
            <div id="page_content_inner">            
                <div class="md-card uk-margin-medium-bottom">
               
                    <div class="md-card-content">
                        <div class="dt_colVis_buttons"></div>
                        <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
								<th>Família</th>								
								<th>Qtd. Disponivel</th>                                						
								<th>Qtd. Pedido</th>
								<th>Preço</th>                                						                                
								<th>Total</th>								
																	                               
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i=0;									
                                    while($campos=$obj->fetch_object()){    										
										$i+=1; 
																				
                                ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $campos->Desigacao ?></td>
									<td><?php echo $campos->familia ?></td>
									<td><?php echo $campos->qtdStock ?></td>
									<td><?php echo $campos->qtdCarrinho ?></td>
									<td style="text-align: right;"><?php echo number_format($campos->preco,2,',','.')?></td>
									<td style="text-align: right;"><?php echo number_format($campos->qtdCarrinho*$campos->preco,2,',','.') ?></td>																                         
								</tr>
                            <?php							 
							  } 														
                            ?>
                          </tbody>
                        </table>						  						
                    </div>
                </div>
				<div class="md-card uk-margin-medium-bottom" style="Padding:10px;">
						<div class="uk-width-medium-1-2">
                            <select id="select_demo_1" class="md-input">
                                <option value="" disabled selected hidden>Estado...</option>
                                <optgroup label="Group 1">
                                    <option value="a1">Item A1</option>
                                    <option value="b1">Item B1</option>
                                    <option value="c1">Item C1</option>
                                </optgroup> 								
                            </select>
							<a class="md-btn md-btn-primary md-btn-wave-light" href="geraEncomenda.php?id= <?php echo $id ?>">
								Continuar
							</a>
                        </div>																						
				</div>
            </div>
        </div>

        <!-- common functions -->
        <script src="assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="assets/js/uikit_custom.min.js"></script>
        <!-- altair common functions/helpers -->
        <script src="assets/js/altair_admin_common.min.js"></script>

        <!-- page specific plugins -->
        <!-- datatables -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <!-- datatables buttons-->
        <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
        <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
        <script src="bower_components/jszip/dist/jszip.min.js"></script>
        <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
        <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
        <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
        
        <!-- datatables custom integration -->
        <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

        <!--  datatables functions -->
        <script src="assets/js/pages/plugins_datatables.min.js"></script>
    </body>
</html>