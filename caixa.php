<?php
function adZero($valor)
{
    return $valor>10?$valor:'0'.$valor;
}

function validar($dataInicial,$dataFinal)
{
    if($dataFinal=="")
    {
        $dataFinal=date('d-m-Y h:m:s');        
    }

    $date1=date_create($dataInicial);
    $date2=date_create($dataFinal);
    $tempo=date_diff($date1,$date2);

    if ($tempo->d==0)
    {
        if ($tempo->h==0) 
        {
            return adZero($tempo->i).' min';
        }
        else
        {
            return adZero($tempo->h).' h e '.adZero($tempo->i).' min';
        }
    } 
    else
    {
        return adZero($tempo->d).' d e '.adZero($tempo->h).' H e '.adZero($tempo->i).' min';
    }

}


require_once("iuda_Shop.php");
$ligar_BD = new conexao();
$liggar=$ligar_BD->conectar();
$player= new Operacao();
$query="SELECT*FROM caixa inner join pessoal on caixa.Pessoal_idPessoal=pessoal.idPessoal where status_Caixa='A'";
$forneceddores=$player->select($query,$liggar);
$atv=0;
while( $liga=$forneceddores->fetch_assoc())
{
    if ($liga['status_Caixa']=='A') 
    {
	   $atv=1;
    }
    $saldoFinal= $liga['SaldoFinal'];
    $idCaixa=$liga['idCaixa'];
}

$A="Ative";
$B="disabled";
if (isset($idCaixa))
{
    $id=$idCaixa;
} 
else
{
    $id="";
}

if ($atv!=0) 
{
    $saldoF= $saldoFinal;
    $B="Ative";
    $A="disabled";
}
else
{
    $saldoF=0;
}

?>

<!doctype html>
<html lang="pt"> 
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Remove Tap Highlight on Windows Phone IE -->
<meta name="msapplication-tap-highlight" content="no"/>
<link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

<title>Sistema de vendas</title>
<!-- uikit -->
<link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

<!-- flag icons -->
<link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

<!-- style switcher -->
<link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
<!-- altair admin -->
<link rel="stylesheet" href="assets/css/main.min.css" media="all">
<script type="text/javascript" src="JS/validar.js"></script>
<script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
<script type="text/javascript" src="JS/personalizado.js"></script>
<script type="text/javascript" src="JS/modalVenda.js"></script>
<script type="text/javascript" src="JS/pagamento.js"></script>
<script type="text/javascript" src="JS/validar.js"></script>
<!--adicionar e remover linhas-->
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>  
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#SaldoInformado").keyup(function() {
            var total=$("#ttvenda").val();
            var recebido=$("#SaldoInformado").val();
            var diferenca=total-recebido;
            var absolut=Math.abs(diferenca);
            $("#diferenca").val(absolut);
            if (diferenca>=0) {
                $("#lbl").text("Falta");
                $("#grp").css("color","#FF0000");
             } else{
              $("#lbl").text("Resta"); 
              $("#grp").css("color","#3A9708");

             }

        });
        //var tabela=$('#');
       
     var tabela= document.getElementById('dt_colVis');
      console.log(tabela.rows.length);
var soma=0;
for (var i =1; i < tabela.rows.length; i++){
	var valor=parseFloat(tabela.rows[i].cells[9].innerHTML);
	if (valor<0) {
		//this.css("color","#3A9708");
		//tabela.rows[i].cells[9].css('color','#3A9708');
		$('.abc').css('color','#AB0606');
	}
	console.log(valor);
	//soma=soma+parseInt(tble.rows[i].cells[9].innerHTML);
}
//document.getElementById('totVenda').value=soma;
        

    });
</script>   
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
<!-- main header -->
<?php 
require_once("header.php");
?>
<!-- main header end -->
<!-- main sidebar -->
<?php
require("menus/menuAdmin.php");
?>
<!-- main sidebar end -->
<div id="page_content">
<div id="page_content_inner">
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                	<!--Cabecalho das tabs-->
                        <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
                            <li class="uk-active"><a href="#">Caixa Actual</a></li>
                            <li class="named_tab"><a href="#">Caixas Anteriores</a></li>
                            <li class="named_tab"><a href="#">Contador de Dinheiro</a></li>
                        </ul>
                    <!--Cabecalho das tabs-->
                    <ul id="tabs_1_content" class="uk-switcher uk-margin">
                        <!--PRIMEIRA TAB-->
                        <li>
                            
                                <div class="md-card uk-margin-medium-bottom">
                                      <table class="table">
                                    	
                                    		<td>
                                    			<button class="md-btn md-btn-primary md-btn-block md-btn-wave-light <?php echo $A ?>" data-uk-modal="{target:'#modal_default'}">
                                    				Abrir Caixa
                                    				<i class="material-icons md-24">play_arrow</i>
                                    			</button>
                                    			<div class="uk-modal" id="modal_default">
                                                   <div class="uk-modal-dialog">
                                                    <button type="button" class="uk-modal-close uk-close"></button>
                                                    <h2 class="heading_a">Abrir novo Caixa</h2>
                                                        <form action="caixa2.php" method="POST">
                                                            <div class="class="uk-width-large-1-4" style="margin-top: 23px;">
                                                                <label>Saldo Inicial</label>
                                                                <input type="text" class="md-input label-fixed" name="SaldoInicial" id="SaldoInicial" onkeyup="somente_numero(this);" />
                                                            </div><br>
                                                           <input type="submit" class="md-btn md-btn-wave" name="guardar" value="Abrir Caixa"> 
                                                            
                                                        </form>
                                                    </div>
					                                 
                    							</div>
                                    		</td>
                                    		<td><button class="md-btn md-btn-primary md-btn-block md-btn-wave-light <?php echo $B ?>" data-uk-modal="{target:'#modal_default2'}" >Fechar Caixa
                                    		<i class="material-icons md-24 uk-text-danger">stop</i>
                                    		</button>
                                            <!--Inicio do Modal de Fecho-->

                                                <div class="uk-modal" id="modal_default2">
                                                   <div class="uk-modal-dialog">
                                                    <button type="button" class="uk-modal-close uk-close"></button>
                                                    <form action="fechar_Caixa.php" method="POST">
                                                    <h2 class="heading_a">Fechar o caixa actual<br><br>

                                                        Valor em caixa: Akz 
                                                        <span  style="border:none; color:#A22222; font-weight:bolder;"><?php echo number_format($saldoF,2,',',' ')  ?></span>
                                                        <input type="hidden" name="saldoFinal" readonly
                                                         value="<?php echo $saldoF ?>" id="ttvenda">
                                                      
                                                            <div class="class="uk-width-medium" style="margin-top: 23px;">
                                                                <input type="text" class="md-input label-fixed" name="SaldoInformado" id="SaldoInformado" onkeyup="somente_numero(this);" placeholder="Insira o saldo final" autocomplete="off" />
                                                                <label id="lbl"></label>
                                                                 <input type="text" class="md-input label-fixed" name="diferenca" readonly id="diferenca"  placeholder="Insira o saldo final" />
                                                                 <input type="hidden" name="idCaixa" value="<?php echo $id ?>">
                                                            </div><br>
                                                           <input type="submit" class="md-btn md-btn-wave" name="guardar" value="Fechar Caixa"> 
                                                            
                                                        </form>
                                                    </div>
                                                     
                                                </div>

                                            <!--Final do modal de fechamento-->

                                        </td>
                                    		<td><button class="md-btn md-btn-primary md-btn-block md-btn-wave-light <?php echo $B ?>">POS</button></td>
                                    		<td><button class="md-btn md-btn-primary md-btn-block md-btn-wave-light <?php echo $B ?>" data-uk-modal="{target:'#modal_default3'}">Entrada de Caixa</button>
                                                    <!--Inicio do Modal de Adicionar Dinheiro-->

                                                <div class="uk-modal" id="modal_default3">
                                                   <div class="uk-modal-dialog">
                                                    <button type="button" class="uk-modal-close uk-close"></button>
                                                    <form action="AddMoney.php" method="POST">
                                                    <h2 class="heading_a">Adicionar valor na Caixa Actual<br><br>
                                                            <div class="class="uk-width-medium" style="margin-top: 23px;">
                                                                <input type="text" class="md-input label-fixed" name="valorAdicionar" id="valorAdicionar" onkeyup="somente_numero(this);" placeholder="Valor" autocomplete="off" required />
                                                                <input class="md-input label-fixed" name="obs" id="diferenca"  placeholder="Observações" /></input>
                                                                 <input type="hidden" name="idCaixa" value="<?php echo $id ?>">
                                                            </div><br>
                                                           <input type="submit" class="md-btn md-btn-wave" name="guardar" value="Adicionar"> 
                                                            
                                                        </form>
                                                    </div>
                                                     
                                                </div>

                                            <!--Final do modal de Adicionar Dinheiro-->
                                    		</td>
                                    		<td><button class="md-btn md-btn-primary md-btn-block md-btn-wave-light <?php echo $B ?>" data-uk-modal="{target:'#modal_default4'}">Saida de Caixa 
                                            </button>
                                                                                                                                        
                                                    <div class="uk-modal" id="modal_default4">
                                                   <div class="uk-modal-dialog">
                                                    <button type="button" class="uk-modal-close uk-close"></button>
                                                    <form action="AddMoney.php" method="POST">
                                                    <h2 class="heading_a">Retirar valores na Caixa Actual<br><br>
                                                            <div class="class="uk-width-medium" style="margin-top: 23px;">
                                                                <input type="text" class="md-input label-fixed" name="valorRetirar" id="valorAdicionar" onkeyup="somente_numero(this);" placeholder="Valor" autocomplete="off" required />
                                                                <input class="md-input label-fixed" name="obsi" id="diferenca"  placeholder="Observações"/>
                                                                 <input type="hidden" name="idCaixa" value="<?php echo $id ?>"/>
                                                            </div><br>
                                                           <input type="submit" class="md-btn md-btn-wave" name="retirar" value="Adicionar"> 
                                                            
                                                        </form>
                                                    </div>
                                                     
                                                </div>

                                            <!--Final do modal de Adicionar Dinheiro-->
                                        </td>
                                    </tr>
                            		</table>
                                </div>
						</li>
                        <li>
                        <!--Caixas Anteriores-->
                         <div class="md-card uk-margin-medium-bottom">
                           <div class="md-card-content">
									 <div class="dt_colVis_buttons"></div>
                                        <table id="dt_colVis" class="uk-table uk-table-striped" cellspacing="0" width="100%">
											<thead>
						                        <tr>
						                            <th>Número</th>
						                            <th>Utilizador</th>
						                            <th>Status</th>
						                            <th>Duração</th>
						                            <th>Abertura</th>
						                            <th>Fecho</th>
						                            <th>Saldo Inicial</th>
						                            <th>Adicionado</th>
						                            <th>Retirado</th>
						                            <th>Saldo Final</th>
						                            <th>Saldo Informado</th>
                                                    <th>Quebra de Caixa</th>
                                                    <th>Ações</th>
						                 		</tr>
               								</thead>
                                            <tbody>  
               								<?php 
                                               require_once("PreviousCash.php");
                                               while( $liga=$forneceddores->fetch_assoc()){
                                                $i++;
                                                $id=$liga['idCaixa'];
                                            ?>
                                            <tr style="text-align: right;">
                                                 <td><?php echo $i?></td>
                                                 <td><?php echo $liga['nome']?></td>
                                                 <?php
                                                 	$abertura=$liga['Abertura'];
                                                 	//$fecho=$liga['Fecho'];
                                                 	$dataFecho= ($liga['Fecho']!='')?($liga['Fecho']):(Date('Y-m-d H:m:s'));
                                                 	
                                                 
                                                 	if($liga['Fecho']==''){
                                                 		echo "<td style='color:red'>Aberto</td>";
                                                 	}else{
                                                 		echo "<td>Fechado</td>";
                                                 	}

                                                 ?>
                                                 <td><?php echo validar($abertura,$liga['Fecho']); ?></td>
                                                 <td><?php echo $abertura ?></td>
                                                 <td><?php echo ($liga['Fecho']!='')?($liga['Fecho']):(''); ?></td>
                                                 <td><?php echo number_format($liga['Valor'],2,',','.') ?></td>
                                                 <td><?php echo number_format($liga['Adicionado'],2,',','.') ?></td>
                                                 <td><?php echo number_format($liga['Retirado'],2,',','.') ?></td>
                                                 <td><?php echo number_format($liga['SaldoFinal'],2,',','.') ?></td>
                                                 <td>
                                                 	<?php echo number_format($liga['saldoFinalInformado'],2,',','.')  ?>
                                                 </td>
                                                 <?php
                                                 	$valorFinal=$liga['saldoFinalInformado']+-$liga['SaldoFinal'];
                                                 	if ($valorFinal<0) {
                                               
                                                 	echo "<td style='color:#C72727'>".number_format($valorFinal,2,',','.')."</td>";

                                                 
                                                 	 } else{
                                                 	 	echo "<td>".number_format($valorFinal,2,',','.')."</td>";

                                                 	 }

                                                 ?>
                                                
                                                     
                                                 </td>
                                                 <td>
                                                    <a title="Detalhes do Caixa" href="detalhesCaixass.php?<?php echo 'cx='.$id ?>" >
                                                         <i class="material-icons">info</i>
                                                      </a>
                                                      <a title="Detalhes do Caixa" href="facturas/relatorioCaixa.php?<?php echo 'cx='.$id ?>"  target="_blanck">
                                                         <i class="material-icons">print</i>
                                                      </a>
                                                </td>
                                            </tr> 
                                              <?php
                                              }
                                              ?>
                                        </tbody>

									</table>
                                </div>
							</div>
                            <!--Caixas Final dos Anteriores-->
                        </li>
                        <li>
                            <?php 
                                require("contador.php");
                            ?>
                        </li>
                    </ul>	
                </div>
            </div>                        
        </div>
    </div>
</div>
</div>
<!-- common functions -->
<script src="assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="assets/js/uikit_custom.min.js"></script>
<!-- altair common functions/helpers -->
<script src="assets/js/altair_admin_common.min.js"></script>
<!-- page specific plugins -->
<!-- handlebars.js -->
<script src="bower_components/handlebars/handlebars.min.js"></script>
<script src="assets/js/custom/handlebars_helpers.min.js"></script>
<!--  product edit functions -->
<script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
<!-- datatables -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<!-- datatables buttons-->
<script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
<script src="assets/js/custom/datatables/buttons.uikit.js"></script>
<script src="bower_components/jszip/dist/jszip.min.js"></script>
<script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
<script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
<script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
<script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
<script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

<!-- datatables custom integration -->
<script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>
<script type="text/javascript" src=JS/jquery.maskMoney.js></script>
<!--  datatables functions -->
<script src="assets/js/pages/plugins_datatables.min.js"></script>
</body>
</html>