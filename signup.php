<?php 
if (isset($_SESSION['nameUser'])){
    session_destroy();
}

 ?>
<!doctype html>
 <html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>Area de Acesso ao Sistema</title>

   <!-- <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>-->

    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="assets/css/login_page.min.css" />
    <script type="text/javascript">
        function alerta(){
                alert('Attention!');
        }
    </script>
</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <form method="POST" name="form_Login" action="acesso.php">
                    <div class="uk-form-row">
                        <label for="login_username">Nome de Utilizador</label>
                        <input required class="md-input" type="text" id="login_username" name="login_username" autocomplete="off" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Senha de utilizador</label>
                        <input  class="md-input" type="password" id="login_password"  name="login_password" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <div class="uk-margin-top">
                            <input class="md-btn md-btn-primary md-btn-block md-btn-large" type="submit" name="guardar" value="Entrar">
                         </div>
                    </div>
                    <a href="#" id="login_help_show" class="uk-float-right">Precisa de Ajuda?</a>
                    
                 
    <?php
    if (isset($_GET['dstv'])) {
        echo "<span style='color:#383BBC; font-weight: bold'>Utilizador desativado!!</span>";  
    } 
     else if (isset($_GET['uknw'])) {
        echo "<span style='color:red; font-weight: bold'>Utilizador desconhecido!!</span>";  
    } else if (isset($_GET['scc'])) {
        echo "<span style='color:green; font-weight: bold'>Password atualizada com sucesso!!</span>";
    }
   
?>                  
</form>
    </div>
            <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_b uk-text-success">Não consegue acessar á sua conta?</h2>
                <p style="text-align: justify;">Siga as seguintes dicas:</p>
                <p style="text-align: justify;">Primeiro: se se lembras da tua senha e não está a funcionar, certifique-se que a tecla "Caps Lock" esteja ligada e que estejas a escrever a senha de forma correcta  e tente de novo.</p>
               <p style="text-align: justify;">Se as credenciais não estiverem   funcionando ou utilizador desativado contacte o administrador do sistema!! </p>
            </div> 
    </div>
</div>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair core functions -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- altair login page functions -->
    <script src="assets/js/pages/login.min.js"></script>
</body>
</html>