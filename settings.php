<!doctype html>
<html lang="pt"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Configurações de empresa</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php 
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require("menu.php");
   ?>
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
           <h3 class="heading_b uk-margin-bottom">Configurações</h3>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
                                <li class="uk-active"><a href="#">Empresa</a></li>
                                <li><a href="#">Preços</a></li>
                                <li class="named_tab"><a href="#">Documentos</a></li>
                            </ul>
                            <ul id="tabs_1_content" class="uk-switcher uk-margin">
                                <li><?php require('empresa.php');?></li>
                                <li><?php require('precos.php');?></li>
                                <li>Content 3</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    
  

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
    <script type="text/javascript" src="JS/validar.js"></script>
    <script type="text/javascript" src="JS/settings.js"></script>
    
     
</body>
</html>