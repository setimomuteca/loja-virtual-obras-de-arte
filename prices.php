<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="SELECT*from familia order by familia asc ";
    $forneceddores=$player->select($busca,$liggar);
    $i=1;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <title>FAMILIAS</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript">
        function confirma(id){
            window.location.href="delete_fam.php?id="+id;
        }
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require_once("menu.php");
   ?>
    <!-- main sidebar end -->
    <div id="page_content">
    <div id="page_content_inner">
        <div id="page_content_inner">
        <div class="row">
            <div class="col-md-12">
                    <form method="POST" name="FormPreco" action="saveSettings.php">
                    <div class="row">
                        <div class="col-md-12 md-card">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Atualizar Preços:</h4>
                                </div>
                                <hr>
                                <div class="col-md-4">
                                    <select class="form-control" name="filtroProdutos" id="filtroProdutos" onchange="FormPreco.submit();">
                                        <option value="-1">Todos Produtos</option>
                                        <option value="1">Familia</option>
                                        <option value="2">Marca</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <select class="form-control resultado" id="buscaFiltro" name="buscaFiltro" disabled="true">
                                        <option>Todos</option>
                                        <option>Familia</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="text-decoration: none;">
                                <div class="col-md-12">
                                    <input type="radio"  name="regraPreco" id="rdio" checked="true">
                                    <label for='rdio' onclick="atualiza(rdio,this)">Preço +
                                    <input type="text" name="" style="width: 60px;"> Akz</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="radio"  name="regraPreco" id="rdio1" checked="true">
                                    <label for='rdio1' onclick="atualiza(rdio,this)">Preço +
                                    <input type="text" name="" style="width: 60px;"> % </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 md-card">
                            <input type="checkbox" name="checkArred" id="chk"> <label for="chk"><h4>Arredondar os preços</h4></label>
                            <hr>
                            <div class="row" style="text-decoration: none;">
                                <div class="col-md-12">
                                    <input type="radio" class="ArrRadio" disabled="true" name="regraPreco" id="automatico" checked="true" value="1" checked="true">
                                    <label for='automatico'">Arredondamento Automático
                                </div>
                                <div class="col-md-12">
                                    <input type="radio" class="ArrRadio" disabled="true" name="regraPreco" id="excesso" value="2">
                                    <label for='excesso'">Arredondamento Excesso
                                </div>
                                <div class="col-md-12">
                                    <input type="radio" class="ArrRadio" disabled="true"  name="regraPreco" id="defeito" value="3">
                                    <label for='defeito'">Arredondamento Defeito
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 md-card">
                            <button type="submit" class="btn btn-primary" style="padding: 10px;">SALVAR</button>
                        </div>
                    </div>
                </form>

        </div>
    </div>
</div>
           
    </div>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script> 
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
    <script type="text/javascript" src="JS/settings.js"></script>
    <script type="text/javascript" src="JS/settings.js"></script>
    
</body>
</html>