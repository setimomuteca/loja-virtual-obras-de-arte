<aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="homeFunc.php" class="sSidebar_hide sidebar_logo_large"> 
                    <img class="logo_regular" src="assets/img/logo_main.png" alt="" height="15" width="71"/>
                    <img class="logo_light" src="assets/img/logo_main_white.png" alt="" height="15" width="71"/>
                </a>
                <a href="homeAdm.php" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="assets/img/logo_main_small.png" alt="" height="32" width="32"/>
                    <img class="logo_light" src="assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
                </a>
            </div>
            
        </div>
        
        <div class="menu_section">
            <ul>
                <li title="Invoices">
                    <?php require('numFactura.php');?>
                    <a href="vendas.php?XX=<?php echo $codFact ?>" class="disabled">
                        <span class="menu_title">Vendas</span>
                    </a>                                                
                </li>
                <li title="Invoices">
                    <a href="clientes.php">
                        <span class="menu_title">Clientes</span>
                    </a>                                                
                </li> 
                <li title="Invoices">
                    <a href="caixa.php">
                        <span class="menu_title">Caixa</span>
                    </a> 
                </li>                           
                <li title="Utilizadores">
                    <a href="user_list.php">
                        <span class="menu_title">Utilizadores</span>
                    </a>                                                
                </li>                                            
            </ul>
        </div>
    </aside>