<aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="homeFunc.php" class="sSidebar_hide sidebar_logo_large"> 
                    <img class="logo_regular" src="assets/img/logo_main.png" alt="" height="15" width="71"/>
                    <img class="logo_light" src="assets/img/logo_main_white.png" alt="" height="15" width="71"/>
                </a>
                <a href="homeAdm.php" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="assets/img/logo_main_small.png" alt="" height="32" width="32"/>
                    <img class="logo_light" src="assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
                </a>
            </div>
            
        </div>
        
        <div class="menu_section">
            <ul> 
                <li title="Produtos">

                    <a href="#">
                        
                        <span class="menu_title">Produtos</span>
                    </a>
                    <ul>
                        <li><a href="produtos.php">Produtos</a></li>
                        <li><a href="familias.php">Familias</a></li>
                        <li><a href="unidades.php">Unidades</a></li>
                        <li><a href="ipc.php">IPC</a></li>
                    </ul>
                </li>
                  <li title="Estoques">

                    <a href="estoqe.php">
                        <span class="menu_title">Estoque</span>
                    </a>
                </li>                                                                                
                <li title="Invoices">
                    <a href="fornecedores.php">
                        <span class="menu_title">Fornecedores</span>
                    </a> 
                </li>              
                <li title="Configurações da empresa">
                    <a href="#">
                        <span class="menu_title">Configurações</span>
                    </a>
                    <ul>
                        <li><a href="company.php">Empresa</a></li>
                        <li><a href="prices.php">Preços</a></li>
                         <li><a href="#">Documentos</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>