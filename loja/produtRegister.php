<?php	
	require_once("../iuda_Shop.php");
	$conexao = new conexao();
	$liggar=$conexao->conectar();
	$operacao = new Operacao();
	$queryFamilia = "SELECT*from familia order by familia asc";	
	$objFamilia = $operacao-> select($queryFamilia,$liggar);
	
	session_start();
	$id = session_id();
	


	require_once('mostraCarrinho.php');
	
?>
<html lang="en" charset="utf-8">
<head>
		<meta charset="utf-8">
		<title>Obras de Arte | A sua loja de artes</title>
		<meta name="description" content="iDea a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.html">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
		<link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

		<!-- iDea core CSS file -->
		<link href="css/style.css" rel="stylesheet">
	
		<!-- Custom css -->
		<link href="css/custom.css" rel="stylesheet">	
	</head>
	
	<body class="front no-trans">
		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop"><i class="icon-up-open-big"></i></div>
		
		<div class="header-top dark">
				<div class="container">
					<div class="row">					
						<div class="col-xs-10 col-sm-12">

							<!-- header-top-second start -->
							<!-- ================ -->
							<div id="header-top-second"  class="clearfix">

								<!-- header top dropdowns start -->
								<!-- ================ -->
								<div class="header-top-dropdown">
									<div class="btn-group dropdown">
										<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i> Pesquisa</button>
										<ul class="dropdown-menu dropdown-menu-right dropdown-animation">
											<li>
												<form role="search" class="search-box">
													<div class="form-group has-feedback">
														<input type="text" class="form-control" placeholder="Pesquisa">
														<i class="fa fa-search form-control-feedback"></i>
													</div>
												</form>
											</li>
										</ul>
									</div>							
									<div class="btn-group dropdown">
										<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Login</button>
										<ul class="dropdown-menu dropdown-menu-right dropdown-animation">
											<li>
												<form class="login-form" method="POST" action= "login.php">
													<div class="form-group has-feedback">
														<label class="control-label">Nome do Utilizador</label>
														<input type="text" name="user" class="form-control" placeholder="">
														<i class="fa fa-user form-control-feedback"></i>
													</div>
													<div class="form-group has-feedback">
														<label class="control-label">Palavra Passe</label>
														<input type="password" name="pass" class="form-control" placeholder="">
														<i class="fa fa-lock form-control-feedback"></i>
													</div>
													<button type="submit" name="entrar" class="btn btn-group btn-dark btn-sm">Log In</button>
													<span>or</span>
													<a type="submit" href="signup.php" class="btn btn-group btn-default btn-sm">Registe-se</a>

													<ul>
														<li><a href="#">Esqueceu a palavra passe ?</a></li>
													</ul>
													<div class="divider"></div>
												</form>
											</li>
										</ul>
									</div>
									<div class="btn-group dropdown">
										<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i> Carrinho <?php  echo"(".$numCarrinho.")" ?></button>
										<ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
											<li>
												<table class="table table-hover">
													<thead>
														<tr>
															<th class="quantity">Qtd.</th>
															<th class="product">Producto</th>
															<th class="amount">Subtotal</th>
														</tr>
													</thead>
													<tbody>
													<?php while ($obj = $objCarrinho->fetch_object() ){?> 
														<tr>
															<td class="quantity"><?php echo $obj->quantidade."x"?> </td>
															<td class="product"><a href="#"><?php echo $obj->Desigacao?> </a></td>
															<td class="amount"><?php echo number_format($obj->subtotal,2,',','.')?></td>
														</tr>
													<?php 
													  }
													?>																																								
													</tbody>
												</table>
												<div class="panel-body text-right">	
												<a href="shop-cart.php" class="btn btn-group btn-default btn-sm">Ver Carrinho</a>
												<a href="shop-checkout.php" class="btn btn-group btn-default btn-sm">Continuar</a>
												</div>
											</li>
										</ul>
									</div>									
								</div>
								<!--  header top dropdowns end -->

							</div>
							<!-- header-top-second end -->

						</div>
					</div>
				</div>
			</div>

		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">		
			<header class="header fixed transparent header-small clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-3">

							<!-- header-left start -->
							<!-- ================ -->
							<div class="header-left clearfix">

								<!-- logo -->
								<div class="logo">
									<a href="index-2.html"><img id="logo" src="images/logo_red.png" alt="iDea"></a>
								</div>

								<!-- name-and-slogan -->
								<div class="site-slogan">
									Clean &amp; Powerful Bootstrap Theme
								</div>

							</div>
							<!-- header-left end -->

						</div>						
					</div>
				</div>
			</header>
			<!-- header end -->

			<!-- banner start -->
			<!-- ================ -->
			<div class="banner shop-banner">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow white-bg">
					
					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container">
						<div class="slider-banner">
							<ul>
								<!-- slide 1 start -->
								<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on" data-title="Slide 1">
								
								<!-- main image -->
								<img src="images/slider-shop-slide-1.jpg"  alt="slidebg1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

								<!-- Translucent background -->
								<div class="tp-caption light-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="800"
									data-start="0"
									style="background-color:rgba(255,255,255,0.4);">
								</div>
																							
								
								<!-- slide 1 end -->

								<!-- slide 2 start -->
								<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on" data-title="Slide 2">
								
								<!-- main image -->
								<img src="images/slider-shop-slide-2.jpg"  alt="slidebg1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

								<!-- Translucent background -->
								<div class="tp-caption light-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="800"
									data-start="0"
									style="background-color:rgba(255,255,255,0.4);">
								</div>																		
								</li>
								<!-- slide 2 end -->

							</ul>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

				

			</div>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Como Vender</h1>
							<div class="separator-2"></div>						
							<!-- page-title end -->
														
							<div class="row">
								<div class="col-md-6" style="text-align: justify; line-height: 200%;"> 
								<p > Se deseja vender os seus bens (Quandros, Moveis, Pratarias, Porcelana Cristais, Vasos, Esculturas, Tapetes, etc),
								teremos o máximo prazer em lhe oferecer uma equipa de especialista que lhe darão toda atenção que necessita e merece
								
								<p> Para obter avaliação de seu bem, basta executar os passos a seguir: 
								<ol style="line-height: 300%;">
									<li>
										 Seleciona Fotos do item que deseja avaliar
										(máximo 10 fotos por envio atravez do formulário ao lado, nos formatos .Jpg ou .PNG
									</li>
									<li>
										 Fazer uma breve descrição do item e obsevações sobre o seu estado (se possui algum defeito, vício ou avaria).
									</li>
									<li>
										Enviar avaliação
									</li>
								</ol>
									<p>
										O serviço de avaliação é totalmente gratuito e efectivado com base nas ultimas vendas realizadas no mercado e no expertise dos nossos responsaveis. 
									
									<p>Após concordância quanto à avaliação, será formalizado um pequeno contrato autorizando a venda do bem 															
									<p> O transporte do bem, do local de origem até as nossas instalações deverá ser acordado entre o proprietário da peça e a galeria que realizará a venda
								
								
								</div>
								<div class="col-md-6"> 
								<form name="formVenda" action="registaProduto.php" method="POST" enctype = "multipart/form-data">
									<div class="form-group">
										<label for="exampleInputEmail1">Produto</label>
										<input type="text" name="produto" class="form-control" id="exampleInputEmail1">
									</div>
									<div class="form-group">
										<label for="categoria"> Categoria</label>
										<select name="categoria" id="categoria" class="form-control"> 
											<?php
												while($obj=$objFamilia->fetch_object()):
											?>
												<option value="<?php echo $obj->idfamilia ?>"><?php echo $obj->familia ?> </option>
											
											<?php
												endwhile;
											?>
										</select>
									</div>
									<div class="form-group">
										<label for="fotos"> Selecione as fotos do item a ser avaliado</label>
										<input type="File" name="fotos[]" class="form-control"  name="fotos" id="fotos" multiple>
									</div>
							
									<div class="form-group">
										<label for="obs"> Observações</label>
										<textArea id="obs" class="form-control" name="obs" style = "height: 255px;">
										</textArea>
									</div>
									<button type="submit" name="gravar" class="btn btn-default btn-block">Enviar</button>
								</form>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- main-container end -->				
			<!-- ================ -->
			<footer id="footer">

				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer">
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="footer-content">
									<div class="logo-footer"><img id="logo-footer" src="images/logo_red_footer.png" alt=""></div>
									<p>Veritatis officiis ullam libero quam aliquam, tenetur dolor incidunt praesentium dolorum laborum tempora.</p>
									<ul class="social-links circle">
										<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
										<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
										<li class="skype"><a target="_blank" href="http://www.skype.com/"><i class="fa fa-skype"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-6 col-md-2">
								<div class="footer-content">
									<h2>Links</h2>
									<nav>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="index-2.html">Home</a></li>
											<li><a href="blog-right-sidebar.html">Blog</a></li>
											<li><a href="portfolio-3col.html">Portfolio</a></li>
											<li><a href="page-about.html">About</a></li>
											<li><a href="page-contact.html">Contact</a></li>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-sm-6 col-md-2">
								<div class="footer-content">
									<h2>Links</h2>
									<nav>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="index-2.html">Home</a></li>
											<li><a href="blog-right-sidebar.html">Blog</a></li>
											<li><a href="portfolio-3col.html">Portfolio</a></li>
											<li><a href="page-about.html">About</a></li>
											<li><a href="page-contact.html">Contact</a></li>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-sm-6 col-md-2">
								<div class="footer-content">
									<h2>Links</h2>
									<nav>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="index-2.html">Home</a></li>
											<li><a href="blog-right-sidebar.html">Blog</a></li>
											<li><a href="portfolio-3col.html">Portfolio</a></li>
											<li><a href="page-about.html">About</a></li>
											<li><a href="page-contact.html">Contact</a></li>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 col-lg-3">
								<div class="footer-content">
									<h2>Subscribe</h2>
									<form class="margin-bottom-clear">
										<div class="form-group has-feedback">
											<label class="sr-only" for="subscribe">Email address</label>
											<input type="email" class="form-control" id="subscribe" placeholder="Enter email" name="subscribe" required>
											<i class="fa fa-envelope form-control-feedback"></i>
											<button type="submit" class="btn btn-white btn-sm">Submit</button>
										</div>
									</form>
									<div class="box small"><i class="fa fa-cc-paypal"></i></div>
									<div class="box small"><i class="fa fa-cc-visa"></i></div>
									<div class="box small"><i class="fa fa-cc-mastercard"></i></div>
									<div class="box small"><i class="fa fa-cc-discover"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<p>Copyright © 2016 iDea by <a target="_blank" href="http://htmlcoder.me/">HtmlCoder</a>. All Rights Reserved</p>
							</div>
							<div class="col-md-6">
								<nav class="navbar navbar-default" role="navigation">
									<!-- Toggle get grouped for better mobile display -->
									<div class="navbar-header">
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-2">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>
									</div>   
									<div class="collapse navbar-collapse" id="navbar-collapse-2">
										<ul class="nav navbar-nav">
											<li><a href="index-2.html">Home</a></li>
											<li><a href="page-about.html">About</a></li>
											<li><a href="blog-right-sidebar.html">Blog</a></li>
											<li><a href="portfolio-3col.html">Portfolio</a></li>
											<li><a href="page-contact.html">Contact</a></li>
										</ul>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
			<!-- footer end -->

		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- jQuery REVOLUTION Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/jquery.appear.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>

		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>						
	</body>
</html>
