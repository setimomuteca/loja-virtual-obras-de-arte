<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $operacoes= new Operacao();
    $queryEmpresa="SELECT* FROM empresa";
   $objQUery=$operacoes->select($queryEmpresa,$liggar);
   while ($registos=$objQUery->fetch_assoc()) {
       $nomCom=$registos['nomeComercial'];
       $nomeFiscal=$registos['nomeFiscal'];
       $endereco=$registos['endereco'];
       $telefone=$registos['telefone'];
       $email=$registos['email'];
       $numContr=$registos['NContribuinte'];

   }
   
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Configuração-Empresa</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <style type="text/css">
        #btn_Guardar{
            width: 100px;
            height: 50px;
            border-radius: 5px;
            cursor:pointer;
            font-weight: bolder;
        }
    </style>
    <script type="text/javascript">
        function confirma(id){
            window.location.href="delete_fam.php?id="+id;
        }
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php
        require_once("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
   <?php
        require_once("menu.php");
   ?>
    <!-- main sidebar end -->
    <div id="page_content">
        <div id="page_content_inner">
           


        <div id="page_content_inner">
            <form action="registaEmpresa.php" method="POST" class="uk-form-stacked" id="product_edit_form" multipart="form-data">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="md-card">
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                            <label for="product_edit_name_control">Nome comercial</label>
                                            <input type="text" class="md-input" id="descricao" name="nomeComercial" value="<?php echo $nomCom; ?>" required/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_name_control">Nome Fiscal</label>
                                            <input type="text" class="md-input" id="descricao" name="nomeFiscal" value="<?php echo $nomeFiscal ?>" required/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_manufacturer_control">Endereço</label>
                                            <input type="text" class="md-input" id="marca" name="endereco" required value="<?php echo $endereco ?>" />
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_edit_memory_control" class="uk-form-label">Telefone</label>
                                           <input type="text"class="md-input" required name="telefone" id="product_edit_sku_control" value="<?php echo $telefone ?>" />
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="product_editt_memory_control" class="uk-form-label">Email</label>
                                           <input type="text"class="md-input" required name="email" id="product_edit_sku_control" value="<?php echo $email ?>" />
                                        </div>
                                          <div class="uk-form-row">
                                            <label for="product_editt_memory_control" class="uk-form-label">N Contribuinte</label>
                                           <input type="text"class="md-input" required name="numContribuinte" id="product_edit_sku_control" value="<?php echo $numContr ?>" />
                                        </div>
                                    </div>
                                    <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                            <label for="product_edit_description_control">Observações</label><br><br>
                                            <textarea class="md-input" name="product_edit_description_control" id="product_edit_description_control" cols="30" rows="4" placeholder="Area de Descrição a empresa"></textarea>
                                        </div>
                                         <div class="uk-form-row">
                                            <input type="file" name="" id="logotipo" style="display: none;">
                                            <label for="logotipo" style="background: #2B94ED; color:white; width: 100=%; padding: 5px;">carregar logotipo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="md-fab-wrapper">
                    <input type="submit" name="guardar" id="btn_Guardar" value="Guardar">
                    <i class="material-icons"></i>
                 </div>

            </form>

        </div>
        </div>
    </div>



    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!--  product edit functions -->
    <script src="assets/js/pages/ecommerce_product_edit.min.js"></script>
</body>
</html>