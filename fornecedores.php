<?php
    require_once("iuda_Shop.php");
    $ligar_BD = new conexao();
    $liggar=$ligar_BD->conectar();
    $player= new Operacao();
    $busca="select*from fornecedores";
    $forneceddores=$player->select($busca,$liggar);
    $i=1;    
?>

<!doctype html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Lista de Fornecedores</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
<style type="text/css">
    tr:nth-child(even) {background-color: #DDD3D3}
</style>
    <script type="text/javascript">
        function confirma(id){
            window.location.href="delete_forn.php?id="+id;
        }
    </script>
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <?php
        require("header.php");
    ?>
    <!-- main header end -->
    <!-- main sidebar -->
    <?php
        if($_SESSION['previlegio']=='Administrador')
        {
            require_once('menus\menuAdmin.php');
        }
        if($_SESSION['previlegio']=='Gvnd')
        {
            require_once('menus\menuGerenteVendas.php');
        }

        if($_SESSION['previlegio']=='Gstck')
        {
            require_once('menus\menuGerenteStock.php');
        }
    ?>
    <!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Lista de Fornecedores 
                <div class="uk-width-medium-1-6">
                            <a class="md-btn md-btn-success md-btn-wave-light" href="forn_reg.php"javascript:void(0)">Novo</a>
                </div>
            </h3>

            <div class="md-card uk-margin-medium-bottom">
           
                <div class="md-card-content">
                    <div class="dt_colVis_buttons"></div>
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
                        <thead>
                        <tr>
                            <th>Número</th>
                            <th>Companhia</th>
                            <th>Correspondente</th>
                            <th>Endereço</th>
                            <th>Telefone</th>    
                            <th>Email</th>
                            <th>Contribuinte</th>
                            <th>Acções</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                while( $liga=$forneceddores->fetch_assoc()){
                                    $idF=$liga['idfornecedores'];
                                    $emp=$liga['companhia'];
                                    $fisc=$liga["Ncontr"];   
                            ?>
                        <tr>
                            <td><?php echo $i ?></td> 
                            <td><?php echo $emp ?></td>
                            <td><?php echo $liga["correspondente"] ?></td>
                            <td><?php echo $liga["endereco"] ?></td>
                            <td><?php echo $liga["telefone"] ?></td>
                            <td><?php echo $liga["email"]    ?></td>
                              <td><?php echo $liga["Ncontr"] ?></td>
                            <td>
                                <a href="#" data-uk-modal="{target:'#modal_default<?php echo $i; ?>'}"><i class="material-icons">info</i>
                                </a> 
                                <!--Modal do Fornecedor-->
                                  <div class="uk-modal" id="modal_default<?php echo $i;?>">
                                <div class="uk-modal-dialog">
                                    <a class="uk-modal-close uk-close"></a>
                                    <h3><?php echo $emp?></h3>
                                        <div class="uk-width-1-1">
                                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content<?php echo $i?>'}" id="tabs_1">
                                                <li class="uk-active">
                                                    <a href="#">Dados Gerais</a>
                                                </li>
                                                
                                                <li><a href="#">Transações</a></li>
                                            </ul>
                                            <ul id="tabs_1_content<?php echo $i?>" class="uk-switcher uk-margin">
                                                <li>
                                                    <div class="content">
                                                        <table class="table" style="width: 100%;"> 
                                                            <tr>
                                                                <td><strong>Companhia</strong></td>
                                                                <td><?php echo $emp;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Correspondente</strong></td>
                                                                <td><?php echo $liga['correspondente'] ;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Nº Contribuinte</strong></td>
                                                                <td><?php echo $liga['Ncontr'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Telefone</strong></td>
                                                                <td><?php echo $liga['telefone'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Email</strong></td>
                                                                <td><?php echo $liga['email'];?></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                   <div class="dt_colVis_buttons"></div>
                                                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%" ">
                                                       <th>#</th><th>Data</th>
                                                       <th>Valor</th>
                                                       <th>Factura</th>
                                                       <th>M. Pagamento</th>
                                                       <th>Status</th>
                                                    <?php
                                                        $b="select*from venda inner join cliente on venda.Cliente_idCliente=cliente.idCliente inner join modo_pagamento on venda.Modo_Pagamento_idModo_Pagamento=modo_pagamento.idModo_Pagamento   where Cliente_idCliente='$idF'";
                                                        $forn=$player->select($b,$liggar);
                                                        $a=0;
                                                        while( $registos=$forn->fetch_assoc()){
                                                            $a++;
                                                            ?>
                                                           <tr>
                                                            <td><?php echo $a?></td>
                                                            <td><?php echo $registos['dataVenda'];?></td>
                                                            <td><?php echo number_format($registos['valor'],2,',','.') ;?></td>
                                                            <td><?php echo $registos['Factura']?></td>
                                                            <td><?php echo $registos['Modo_Pagamento']?></td>
                                                           </tr>
                                                            <?php
                                                             }
                                                            ?>
                                                      </table>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <a class="md-btn md-btn-primary md-btn-wave-light" href="edit_forn?XX=<?php echo $idF;?>">Editar
                                            </a>
                                        </div>
                                </div>

                                </div> 
                                <!--Modal do fornecedor-->
                             
                                <a href="#" style="width: 30px; margin-left: 15px;" 
                                  onclick="UIkit.modal.confirm('Confirma a eliminação desse registo?', function(){
                                     confirma('<?php echo $liga['idfornecedores'] ?>');
                                 });">
                                     <span style="color:red; font-weight: bolder; font-size: 18px; padding: 4px; left: -9px; position: relative;">
                                     X
                                 </span>
                                </a>
                        </td>
                        </tr>
                        <?php
                        $i++;
                        } 
                        ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- google web fonts -->
    

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>
    
    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>
</body>
</html>